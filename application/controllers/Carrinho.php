<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
        $this->load->model('CarrinhoModel', 'carrinho');
    }

    public function index() {
		if(!$this->session->userdata('logado')){
			redirect('sign/in?status=page_carrinho','refresh');
		}
		$dados['titulo'] = 'Carrinho de Compras | IPCI';
		$html = $this->load->view('loja/carrinho', null, true);
		$this->show($html,$dados);
	}

	// Requisições AJAX
	public function frete(){
		if(!$this->session->userdata('logado')){
			redirect('sign/in?status=page_carrinho','refresh');
		}
		if(isset($_POST['request']) && $_POST['request'] == 'frete'){
			
			$this->load->model('frete');
			$retorno = $this->frete->calcular();
			echo json_encode($retorno);

		} else {
			echo 'Não é permitido acesso direto por script.';
			redirect('404','refresh');
		}

	}
	
	public function adicionar(){
		if(isset($_POST['request']) && $_POST['request'] == 'add_carrinho'){

			$retorno = $this->carrinho->adicionar();
			echo json_encode($retorno);

		} else {
			echo 'Não é permitido acesso direto por script.';
			redirect('404','refresh');
		}
	}

	public function deletar(){
		if(!$this->session->userdata('logado')){
			redirect('sign/in?status=page_carrinho','refresh');
		}
		if(isset($_POST['request']) && $_POST['request'] == 'del_carrinho'){

			$retorno = $this->carrinho->deletar();
			echo json_encode($retorno);

		} else {
			echo 'Não é permitido acesso direto por script.';
			redirect('404','refresh');
		}
	}

	public function atualizar_quantidade(){
		if(!$this->session->userdata('logado')){
			redirect('sign/in?status=page_carrinho','refresh');
		}
		if(isset($_POST['request']) && $_POST['request'] == 'atl_qtd'){

			$retorno = $this->carrinho->atualizar_quantidade();
			echo json_encode($retorno);

		} else {
			echo 'Não é permitido acesso direto por script.';
			redirect('404','refresh');
		}
	}

	public function get_carrinho(){
		if(!$this->session->userdata('logado')){
			redirect('sign/in?status=page_carrinho','refresh');
		}
		if(isset($_POST['request']) && $_POST['request'] == 'get_carrinho'){

			$retorno = $this->carrinho->get_carrinho();
			echo json_encode($retorno);

		} else {
			echo 'Não é permitido acesso direto por script.';
			redirect('404','refresh');
		}
	}

	public function show($html, $dados=null) {
		// Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
			foreach ($dados['departamentos'] as $departamento) {
				$ids_departamentos[] .= $departamento->id;
			}
			$dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
			$ids_categorias = array();
			foreach ($dados['categorias'] as $categoria) {
				$ids_categorias[] .= $categoria->id;
			}
			$dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
		}
		$dados['carrinho'] = $this->carrinho->get_qtd_produtos();
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
		$aux .= $this->load->view('loja/navbar', null, true);
		$aux .= $html;
		$aux .= $this->load->view('ipci/rodape', null, true);
		$js['js'] = $this->load->view('js/carrinho', null, true);
		$js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/mask', null, true);
		$aux .= $this->load->view('common/footer', $js, true);
		echo $aux;
	}

}