<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sobre extends CI_Controller {

	public function index() {
		$dados['titulo'] = 'Sobre | IPCI';

		$this->load->view('common/header', $dados);
		$this->load->view('ipci/sobre');
		$this->load->view('ipci/rodape');
		$this->load->view('common/footer');
	}
}
