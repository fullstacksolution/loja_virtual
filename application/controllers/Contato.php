<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
        $this->load->model('CarrinhoModel', 'carrinho');
        $this->load->model('ContatoModel', 'contato');
    }

    public function index() {
        $dados['titulo'] = 'Contato | IPCI';
        
        $html = $this->load->view('ipci/contato', null, true);
        $this->show($html, $dados);
    }

    public function enviar_mensagem(){
        if(isset($_POST['request']) && $_POST['request'] == 'mail'){
            
            $retorno = $this->contato->enviar_mensagem();
            echo json_encode($retorno);

        } else {
            redirect('contato','refresh');
        }
    }

	public function show($html, $dados=null) {
        // Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
            foreach ($dados['departamentos'] as $departamento) {
                $ids_departamentos[] .= $departamento->id;
            }
            $dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
            $ids_categorias = array();
            foreach ($dados['categorias'] as $categoria) {
                $ids_categorias[] .= $categoria->id;
            }
            $dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
        }
        $dados['carrinho'] = $this->carrinho->get_qtd_produtos();
        $dados['recaptcha'] = '<script src="https://www.google.com/recaptcha/api.js" async defer></script>';
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
        $aux .= $this->load->view('loja/navbar', null, true);
        $aux .= $html;
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $aux .= $this->load->view('ipci/rodape', null, true);
		$aux .= $this->load->view('common/footer', $js, true);
		echo $aux;
	}

}