<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('UsuarioModel', 'usuario');
    }

    public function index(){
        redirect('sign/in','refresh');
    }

    public function in(){
        if($this->session->userdata('logado')){
            redirect('loja','refresh');
        }
        $dados['titulo'] = 'Entrar | IPCI';
        $html = $this->load->view('usuario/login', null, true);
        $this->show($html,$dados);
    }
    
    public function out(){
        $this->usuario->desautenticar($this->session->userdata('userlogado')->id);
        $dadosSessao['userlogado'] = NULL;
        $dadosSessao['logado'] = FALSE;
        $this->session->set_userdata($dadosSessao);
        redirect('loja','refresh');
    }

    public function up(){
        $dados['titulo'] = 'Cadastro | IPCI';
        $html = $this->load->view('usuario/cadastro', null, true);
        $html .= $this->load->view('js/cep', null, true);
        $this->show($html,$dados);
    }

    public function auth(){
        $retorno = $this->usuario->autenticar();
        echo json_encode($retorno);
    }

    public function cadastro(){
        $retorno = $this->usuario->criar();
        echo json_encode($retorno);
    }

    public function show($html, $dados=null) {
		$aux = $this->load->view('common/header', $dados, true);
        $aux .= $html;
        $js['js'] = $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
        $js['js'] .= $this->load->view('js/mask', null, true);
		$aux .= $this->load->view('common/footer', $js, true);
		echo $aux;
	}
}