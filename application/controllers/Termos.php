<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Termos extends CI_Controller {

    public function index() {
        $html = $this->load->view('ipci/termos', null, true);
        $this->show($html);
    }

	public function show($html, $dados=null) {
		$aux = $this->load->view('common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('ipci/rodape', null, true);
		$aux .= $this->load->view('common/footer', null, true);
		echo $aux;
	}

}