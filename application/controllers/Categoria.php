<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct(){
        parent::__construct();
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
        $this->load->model('CarrinhoModel', 'carrinho');
    }

    public function index() {
		$dados['titulo'] = 'Categoria | IPCI';
		$html = $this->load->view('loja/barra_lateral', null, true);
		$html .= $this->load->view('loja/categoria', null, true);
		$this->show($html,$dados);
	}

	public function listar($slug_departamento = null, $slug_categoria = null, $slug_subcategoria = null, $pular=null, $prod_por_pagina=null){
		// Listar todos os produtos pela subcategoria
		if( isset($slug_departamento) && isset($slug_categoria) && isset($slug_subcategoria) ){

			$departamento = $this->departamento->get_departamento_by_name($slug_departamento);
			
			if(isset($departamento->id) && $departamento->qtd_produtos > 0){

				$categoria = $this->categoria->get_categoria_by_name_departamento($slug_categoria, $departamento->id);

				if(isset($categoria->id) && $categoria->qtd_produtos > 0){

					$subcategoria = $this->subcategoria->get_subcategoria_by_name_categoria($slug_subcategoria, $categoria->id);

					if(isset($subcategoria->id) && $subcategoria->qtd_produtos > 0){

						$this->load->library('pagination');
						
						$config['base_url'] = base_url('categoria/'.$slug_departamento.'/'.$slug_categoria.'/'.$slug_subcategoria);
						$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
						$prod_por_pagina = 12;

						if(isset($_GET['filtro'])){

							// Filtros
							if($_GET['filtro'] == 'preco_asc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'preco', 'ASC');
								
							} else if($_GET['filtro'] == 'preco_desc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'preco', 'DESC');

							} else if($_GET['filtro'] == 'avaliacao_desc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'media_avaliacao', 'DESC');

							} else if($_GET['filtro'] == 'avaliacao_5'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 5);

							} else if($_GET['filtro'] == 'avaliacao_4'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 4);

							} else if($_GET['filtro'] == 'avaliacao_3'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 3);

							} else if($_GET['filtro'] == 'avaliacao_2'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 2);

							} else if($_GET['filtro'] == 'avaliacao_1'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 1);

							}
							
						} else {
							$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, null, null);
						}
						
						$config['total_rows'] = count($listar['produtos'])+1;
						$config['per_page'] = $prod_por_pagina;

						$this->pagination->initialize($config);

						$listar['links_paginacao'] = $this->pagination->create_links();
						
						if(isset($_GET['filtro'])){
							// Filtros
							if($_GET['filtro'] == 'preco_asc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'preco', 'ASC', $pular, $prod_por_pagina);
								
							} else if($_GET['filtro'] == 'preco_desc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'preco', 'DESC', $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_desc'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'media_avaliacao', 'DESC', $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_5'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 5, $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_4'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 4, $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_3'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 3, $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_2'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 2, $pular, $prod_por_pagina);

							} else if($_GET['filtro'] == 'avaliacao_1'){
								$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, 'avaliacao', 1, $pular, $prod_por_pagina);

							}
							
						} else {
							$listar['produtos'] = $this->produto->get_produtos_by_subcategoria_loja($subcategoria->id, null, null, $pular, $prod_por_pagina);
						}

						$dados['titulo'] = $subcategoria->nome.' em '.$categoria->nome.' de '.$departamento->nome.' | IPCI';
						$listar['url_filtro'] = base_url('categoria/'.$slug_departamento.'/'.$slug_categoria.'/'.$slug_subcategoria.'?');

						$listar['departamento'] = $departamento;
						$listar['categoria'] = $categoria;
						$listar['subcategoria'] = $subcategoria;
						$listar['titulo'] = $subcategoria->nome.' em '.$categoria->nome.' de '.$departamento->nome;
						$listar['pagina'] = 'subcategoria';
						
						$listar['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias([$categoria->id]);

						$destaques['destaques'] = $this->produto->get_produtos_destaques_by_categoria($categoria->id);
						$destaques['filtro'] = 'categoria';
						
						$html = $this->load->view('loja/barra_lateral', $listar, true);
						$html .= $this->load->view('loja/lista_produtos', null, true);
						$html .= $this->load->view('loja/destaques_carrosel', $destaques, true);

						$this->show($html,$dados);

						} else {
							// Redirect para a loja caso o departamento não exista
							redirect('loja','refresh');
						}

				} else {
					// Redirect para a loja caso o departamento não exista
					redirect('loja','refresh');
				}

			} else {
				// Redirect para a loja caso o departamento não exista
				redirect('loja','refresh');
			}

		// Listar todos os produtos pela categoria
		} else if( isset($slug_departamento) && isset($slug_categoria) && !isset($slug_subcategoria) ) {

			$departamento = $this->departamento->get_departamento_by_name($slug_departamento);
			
			if(isset($departamento->id) && $departamento->qtd_produtos > 0){

				$categoria = $this->categoria->get_categoria_by_name_departamento($slug_categoria, $departamento->id);

				if(isset($categoria->id) && $categoria->qtd_produtos > 0){
				
				$dados['titulo'] = $categoria->nome.' de '.$departamento->nome.' | IPCI';
				$listar['departamento'] = $departamento;
				
				$listar['categoria'] = $categoria;
				$ids_categorias = $categoria->id;
				$listar['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias([$ids_categorias]);
				$listar['titulo'] = $categoria->nome.' de '.$departamento->nome;
				$listar['pagina'] = 'departamento';
				
				$listar['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias([$categoria->id]);

				$destaques['destaques'] = $this->produto->get_produtos_destaques_by_categoria($categoria->id);
				$destaques['filtro'] = 'categoria';
				
				$html = $this->load->view('loja/barra_lateral', $listar, true);
				$html .= $this->load->view('loja/categoria', null, true);
				$html .= $this->load->view('loja/destaques_carrosel', $destaques, true);

				$this->show($html,$dados);

				} else {
					// Redirect para a loja caso o departamento não exista
					redirect('loja','refresh');
				}

			} else {
				// Redirect para a loja caso o departamento não exista
				redirect('loja','refresh');
			}

		// Listar todos os produtos pelo departamento
		} else if( isset($slug_departamento) && !isset($slug_categoria) && !isset($slug_subcategoria) ) {

			$departamento = $this->departamento->get_departamento_by_name($slug_departamento);
			
			if(isset($departamento->id) && $departamento->qtd_produtos > 0){
				
				$dados['titulo'] = $departamento->nome.' | IPCI';
				$listar['departamento'] = $departamento;
				$listar['titulo'] = $departamento->nome;
				$listar['pagina'] = 'departamento';
				
				$listar['categorias'] = $this->categoria->get_categorias_by_departamentos([$departamento->id]);
				
				$html = $this->load->view('loja/barra_lateral', $listar, true);
				$html .= $this->load->view('loja/departamento', null, true);

				$this->show($html,$dados);

			} else {
				// Redirect para a loja caso o departamento não exista
				redirect('loja','refresh');
			}
			
		// Voltar à loja caso não tenha passado parâmetros na URL
		} else if( !isset($slug_departamento) ) {
			redirect('loja','refresh');
		}
	}
	
	public function mapa(){
		$dados['titulo'] = 'Mapa do Site | IPCI';

		// Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$listar['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($listar['departamentos'][0])){
			foreach ($listar['departamentos'] as $departamento) {
				$ids_departamentos[] .= $departamento->id;
			}
			$listar['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
			$ids_categorias = array();
			foreach ($listar['categorias'] as $categoria) {
				$ids_categorias[] .= $categoria->id;
			}
			$listar['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
		}

		$html = $this->load->view('loja/mapa', $listar, true);
		$this->show($html,$dados);

	}

	public function show($html, $dados=null) {
		// Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
			foreach ($dados['departamentos'] as $departamento) {
				$ids_departamentos[] .= $departamento->id;
			}
			$dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
			$ids_categorias = array();
			foreach ($dados['categorias'] as $categoria) {
				$ids_categorias[] .= $categoria->id;
			}
			$dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
		}
		$dados['carrinho'] = $this->carrinho->get_qtd_produtos();
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
		$aux .= $this->load->view('loja/navbar', null, true);
		$aux .= $html;
		$aux .= $this->load->view('ipci/rodape', null, true);
		$aux .= $this->load->view('common/footer', null, true);
		echo $aux;
	}
}