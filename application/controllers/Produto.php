<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
        $this->load->model('CarrinhoModel', 'carrinho');
    }

    public function index($id = null, $slug = null) {
        if($id == null) redirect('errors/erro404','refresh');
        
        // Recuperar dados do produto no banco de dados
        $produto['produto'] = $this->produto->get_produto_by_id_loja($id);

        // Verifica se o produto existe OU se está não publicado
        if($produto['produto']->id == null || $produto['produto']->status == 0) redirect('errors/erro404','refresh');

        $dados['titulo'] = $produto['produto']->nome.' | IPCI';
        // Recuperar as imagens do produto, caso houver
        $produto['imagens'] = $this->produto->get_imagens_by_produto($id);
        // Recuperar as 4 primeiras avaliações do produto, caso houver
        $produto['avaliacoes'] = $this->produto->get_avaliacoes_by_produto_limit($id);

        // Verifica se existe usuário logado
        if($this->session->userdata('logado')){
            $carrinhoLib = new CarrinhoLib();
            // Se sim, verifica se o produto já está no carrinho do usuário
            if($carrinhoLib->verificar_adicionado($id, $this->session->userdata('userlogado')->id)->existe){
                $produto['no_carrinho'] = true;
            } else {
                $produto['no_carrinho'] = false;
            }
        } else {
            $produto['no_carrinho'] = false;
        }

        // Verifica se a variável recuperou as avaliações do produto
        if($produto['avaliacoes']){
            // Recupera a soma de todas as estrelas do produto
            $soma = $this->produto->get_soma_avaliacoes_by_produto($id)->avaliacao;
            // Recupera a quantidade de avaliações do produto
            $qtd = $this->produto->get_qtd_avaliacoes_by_produto($id)->qtd_avaliacoes;
            $produto['qtd_avaliacoes'] = $qtd;
            // Realiza a média aritmética de estrelas do produto
            $produto['media_avaliacao'] = round($soma / $qtd, 1);
        } else {
            $produto['qtd_avaliacoes'] = 0;
            $produto['media_avaliacao'] = 0;
        }


        // Informações para o campo de produtos em destaque
        $id_categoria = $produto['produto']->id_categoria;
        $destaques['destaques'] = $this->produto->get_produtos_destaques_by_categoria($id_categoria);
        
        $html = $this->load->view('loja/produto', $produto, true);
        $html .= $this->load->view('loja/avaliacao_comentario', null, true);
        $html .= $this->load->view('loja/destaques_carrosel', $destaques, true);
        
        $this->show($html, $dados);
        
    }

    public function adicionar_avaliacao(){
        if(!$this->session->userdata('logado')){
            $erros['erro'] = 'Usuário não logado';
            $erros['redirect'] = base_url('sign/in?status=add_avaliacao&produto='.$_POST['id_produto']);
            
            echo json_encode($erros);

        } else if($_POST['request'] == 'avaliacao' && $this->session->userdata('logado') ){
            $sucesso['sucesso'] = 1;
            $sucesso['usuario'] = $this->session->userdata('userlogado');
            $sucesso['post'] = $_POST;
            $sucesso['html'] = '
            <div id="row_form_avaliacao" class="row">
                <div class="col-md-6 mx-auto text-center mt-3">
                    <h5>Adicionar Avaliação</h5>
                    <form id="add_avaliacao">
                        <div class="form-row">
                            <div class="col-12 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" name="titulo" id="titulo" maxlength="40" class="form-control">
                                    <label for="titulo">Título</label>
                                </div>
                            </div>
                            <div class="col-12 mx-auto">
                                <div class="md-form mt-2">
                                    <textarea class="md-textarea w-100" name="comentario" id="comentario" rows="3" maxlength="255"></textarea>
                                    <label for="comentario">Comentário</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-8 col-sm-10 col-md-7 col-lg-6 col-xl-5 mx-auto">
                                <label class="text-muted font-weight-light" for="avaliacao">Avaliação</label>
                                <input type="range" class="custom-range" min="0" max="5" value="3" id="avaliacao" name="avaliacao">
                                <ul class="text-right m-0 p-0">
                                    <li class="list-inline-item mb-0 ml-1 ml-sm-4 ml-md-3 ml-lg-2 ml-xl-3 mr-2"><i class="d-none fas fa-star text-ipci-primary"></i></li>
                                    <li class="list-inline-item mb-0 mx-1 mx-sm-4 mx-md-1 mx-lg-2 mx-xl-2"><i id="star_1" class="fas fa-star text-ipci-primary"></i></li>
                                    <li class="list-inline-item mb-0 mx-1 mx-sm-4 mx-md-1 mx-lg-2 mx-xl-2"><i id="star_2" class="fas fa-star text-ipci-primary"></i></li>
                                    <li class="list-inline-item mb-0 mx-1 mx-sm-4 mx-md-1 mx-lg-2 mx-xl-2"><i id="star_3" class="fas fa-star text-ipci-primary"></i></li>
                                    <li class="list-inline-item mb-0 mx-1 mx-sm-4 mx-md-1 mx-lg-2 mx-xl-2"><i id="star_4" class="fas fa-star text-muted"></i></li>
                                    <li class="list-inline-item mb-0 ml-0 ml-sm-4 ml-md-1 ml-lg-2 ml-xl-2"><i id="star_5" class="fas fa-star text-muted"></i></li>
                                </ul>
                            </div>
                        </div>

                        <input type="hidden" name="request" id="request" value="add_avaliacao">
                        <input type="hidden" name="id_produto" id="id_produto" value="'.$_POST['id_produto'].'">

                        <button class="btn btn-success mt-4" type="submit">Enviar</button>
                        <button id="cancelar_avaliacao" class="btn btn-white mt-4" type="button">Cancelar</button>
                    </form>
                </div>
            </div>
            ';
            echo json_encode($sucesso);
        } else if($_POST['request'] == 'add_avaliacao'){
            $this->load->model('ProdutoModel', 'produto');
            $retorno = $this->produto->adicionar_avaliacao();
            echo json_encode($retorno);
        }
    }

    public function get_avaliacoes_by_produto(){
        if($_POST['request'] == 'getaval'){
            $this->load->model('ProdutoModel', 'produto');
            $retorno = $this->produto->get_avaliacoes_by_produto($_POST['id_produto']);
            foreach ($retorno as $r){
                $r->data = converter_data($r->data);
            }
            echo json_encode($retorno);
        }
    }

    public function show($html, $dados=null) {
        // Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
            foreach ($dados['departamentos'] as $departamento) {
                $ids_departamentos[] .= $departamento->id;
            }
            $dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
            $ids_categorias = array();
            foreach ($dados['categorias'] as $categoria) {
                $ids_categorias[] .= $categoria->id;
            }
            $dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
        }
        $dados['carrinho'] = $this->carrinho->get_qtd_produtos();
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
		$aux .= $this->load->view('loja/navbar', null, true);
        $aux .= $html;
        $aux .= $this->load->view('ipci/rodape', null, true);
        $js['js'] = $this->load->view('js/produto', null, true);
        $js['js'] .= $this->load->view('js/avaliacao_comentario', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/galeria_produtos', null, true);
		$aux .= $this->load->view('common/footer', $js, true);
		echo $aux;
	}

}