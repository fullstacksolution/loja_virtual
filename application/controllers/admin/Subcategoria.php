<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategoria extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('CategoriaModel', 'categoria');
        $this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
    }
    
    public function index(){
        
    }
    
    public function criar_subcategoria(){
        $rota = $this->uri->segment(2);
        $dados['categorias'] = $this->categoria->get_categorias();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Criar '.$rota.' | Admin';
            $dados['subtitulo'] = 'Criar nova '.$rota;
            $dados['descricao'] = 'Criar uma nova '.$rota.' de produtos';
            $dados['rota'] = $rota;
            $html = $this->load->view('admin/navbar', null, true);
            $html .= $this->load->view('admin/subcategoria/criar', $dados, true);
            $this->show($html,$dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Criar '.$rota.' | Admin';
            $retorno['subtitulo'] = 'Criar nova '.$rota;
            $retorno['descricao'] = 'Criar um nova '.$rota.' de produtos';
            $retorno['rota'] = $rota;
            $retorno['categorias'] = $dados['categorias'];

            $retorno['html'] = $this->load->view('admin/subcategoria/criar', $retorno, true);
            echo json_encode($retorno);
        } else if($_POST['request'] == 'criar'){
            $retorno = $this->subcategoria->criar();
            echo json_encode($retorno);
        }
    }

    public function listar_subcategorias(){
        $subcategorias['subcategorias'] = $this->subcategoria->get_subcategorias();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Subcategorias | Admin';
            $subcategorias['subtitulo'] = 'subcategoria';
            $subcategorias['descricao'] = 'Lista de todas as subcategorias cadastradas';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/subcategoria/tabela', $subcategorias,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Subcategorias | Admin';
            $retorno['subcategorias'] = $subcategorias['subcategorias'];
            $retorno['subtitulo'] = 'subcategoria';
            $retorno['descricao'] = 'Lista de todas as subcategorias cadastradas';
            $retorno['html'] = $this->load->view('admin/subcategoria/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function editar_subcategoria($id){
        $subcategoria['subcategoria'] = $this->subcategoria->get_subcategoria_by_id($id);
        $subcategoria['categorias'] = $this->categoria->get_categorias();
        
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar subcategoria | Admin';
            $subcategoria['subtitulo'] = $subcategoria['subcategoria']->nome;
            $subcategoria['descricao'] = 'Editar subcategoria '.$subcategoria['subcategoria']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/subcategoria/editar', $subcategoria,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Editar subcategoria | Admin';
            $retorno['subtitulo'] = $subcategoria['subcategoria']->nome;
            $retorno['descricao'] = 'Editar subcategoria '.$subcategoria['subcategoria']->nome;

            $retorno['subcategoria'] = $subcategoria['subcategoria'];
            $retorno['categorias'] = $subcategoria['categorias'];

            $retorno['html'] = $this->load->view('admin/subcategoria/editar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'edit'){
		    $retorno = $this->subcategoria->editar();
            echo json_encode($retorno);
        }
    }

    public function deletar_subcategoria($id){
        $subcategoria['subcategoria'] = $this->subcategoria->get_subcategoria_by_id($id);
        $subcategoria['produtos'] = $this->produto->get_produtos_by_subcategoria($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar subcategoria | Admin';
            $subcategoria['subtitulo'] = $subcategoria['subcategoria']->nome;
            $subcategoria['descricao'] = 'Deletar subcategoria '.$subcategoria['subcategoria']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/subcategoria/deletar', $subcategoria,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Deletar subcategoria | Admin';
            $retorno['subcategoria'] = $subcategoria['subcategoria'];
            $retorno['produtos'] = $subcategoria['produtos'];
            $retorno['subtitulo'] = $subcategoria['subcategoria']->nome;
            $retorno['descricao'] = 'Deletar subcategoria '.$subcategoria['subcategoria']->nome;

            $retorno['html'] = $this->load->view('admin/subcategoria/deletar', $retorno, true);
            echo json_encode($retorno);
            
        } else if($_POST['request'] == 'delete'){
            $retorno = $this->subcategoria->deletar();
            echo json_encode($retorno);
        }
    }

    
	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}


}