<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('SubcategoriaModel', 'subcategoria');
        $this->load->model('ProdutoModel', 'produto');
    }
    
    public function index(){
        
    }
    
    public function criar_produto(){
        $rota = $this->uri->segment(2);
        $dados['subcategorias'] = $this->subcategoria->get_subcategorias();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Criar '.$rota.' | Admin';
            $dados['subtitulo'] = 'Criar novo '.$rota;
            $dados['descricao'] = 'Criar um novo '.$rota;
            $dados['rota'] = $rota;
            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/criar', $dados, true);
            $this->show($html,$dados);
            
        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Criar '.$rota.' | Admin';
            $retorno['subtitulo'] = 'Criar novo '.$rota;
            $retorno['descricao'] = 'Criar um novo '.$rota;
            $retorno['rota'] = $rota;
            $retorno['subcategorias'] = $dados['subcategorias'];

            $retorno['html'] = $this->load->view('admin/produto/criar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'criar'){
            $retorno = $this->produto->criar();
            echo json_encode($retorno);
        }
    }
    
    public function listar_produtos(){
        $produtos['produtos'] = $this->produto->get_produtos();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Produtos | Admin';
            $produtos['subtitulo'] = 'produto';
            $produtos['descricao'] = 'Lista de todos os produtos cadastrados';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/tabela', $produtos,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Produtos | Admin';
            $retorno['produtos'] = $produtos['produtos'];
            $retorno['subtitulo'] = 'produto';
            $retorno['descricao'] = 'Lista de todos os produtos cadastrados';
            $retorno['html'] = $this->load->view('admin/produto/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function editar_produto($id){
        $rota = $this->uri->segment(2);
        $produto['produto'] = $this->produto->get_produto_by_id($id);
        $produto['subcategorias'] = $this->subcategoria->get_subcategorias();
        // $produto['imagens'] = $this->produto->get_imagens_by_produto($id);
        
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar produto | Admin';
            $produto['subtitulo'] = $produto['produto']->nome;
            $produto['descricao'] = 'Editar produto '.$produto['produto']->nome;
            $produto['rota'] = $rota;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/editar', $produto,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Editar produto | Admin';
            $retorno['subtitulo'] = $produto['produto']->nome;
            $retorno['descricao'] = 'Editar produto '.$produto['produto']->nome;
            $retorno['rota'] = $rota;

            $retorno['produto'] = $produto['produto'];
            $retorno['subcategorias'] = $produto['subcategorias'];
            // $retorno['imagens'] = $produto['imagens'];

            $retorno['html'] = $this->load->view('admin/produto/editar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'edit'){
		    $retorno = $this->produto->editar();
            echo json_encode($retorno);
        }
    }

    public function adicionar_imagem(){
        if($_POST['request'] == 'addimg'){
            $retorno = $this->produto->adicionar_imagem();
            echo json_encode($retorno);
        }
    }

    public function listar_imagens(){
        if($_POST['request'] == 'listimg'){
            $retorno = $this->produto->get_imagens_by_produto($_POST['id_produto']);
            echo json_encode($retorno);
        }
    }

    public function deletar_imagem(){
        if($_POST['request'] == 'delimg'){
            $retorno = $this->produto->deletar_imagem();
            echo json_encode($retorno);
        }
    }

    public function deletar_produto($id){
        $produto['produto'] = $this->produto->get_produto_by_id($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar produto | Admin';
            $produto['subtitulo'] = $produto['produto']->nome;
            $produto['descricao'] = 'Deletar produto '.$produto['produto']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/deletar', $produto,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Deletar produto | Admin';
            $retorno['produto'] = $produto['produto'];
            $retorno['subtitulo'] = $produto['produto']->nome;
            $retorno['descricao'] = 'Deletar produto '.$produto['produto']->nome;

            $retorno['html'] = $this->load->view('admin/produto/deletar', $retorno, true);

            echo json_encode($retorno);
        } else if($_POST['request'] == 'delete'){
            $retorno = $this->produto->deletar();
            echo json_encode($retorno);
        }
    }

    public function listar_avaliacoes(){
        $avaliacoes['avaliacoes'] = $this->produto->get_avaliacoes(1);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Avaliações | Admin';
            $avaliacoes['subtitulo'] = 'avaliações';
            $avaliacoes['descricao'] = 'Lista de todas as avaliações publicadas';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/avaliacao/tabela', $avaliacoes,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Avaliações | Admin';
            $retorno['avaliacoes'] = $avaliacoes['avaliacoes'];
            $retorno['subtitulo'] = 'avaliações';
            $retorno['descricao'] = 'Lista de todas as avaliações publicadas';
            $retorno['html'] = $this->load->view('admin/produto/avaliacao/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function listar_avaliacoes_pendentes(){
        $avaliacoes['avaliacoes'] = $this->produto->get_avaliacoes(0);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Avaliações | Admin';
            $avaliacoes['subtitulo'] = 'avaliações';
            $avaliacoes['descricao'] = 'Lista de todas as avaliações pendentes';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/avaliacao/tabela', $avaliacoes,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Avaliações | Admin';
            $retorno['avaliacoes'] = $avaliacoes['avaliacoes'];
            $retorno['subtitulo'] = 'avaliações';
            $retorno['descricao'] = 'Lista de todas as avaliações pendentes';
            $retorno['html'] = $this->load->view('admin/produto/avaliacao/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function publicar_avaliacao($id){
        $retorno = $this->produto->publicar_avaliacao($id);
        echo json_encode($retorno);
    }

    public function deletar_avaliacao($id){
        $avaliacao['avaliacao'] = $this->produto->get_avaliacao_by_id($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar avaliação | Admin';
            $dados['avaliacao'] = $avaliacao['avaliacao'];

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/produto/avaliacao/deletar', $dados,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Deletar avaliação | Admin';
            $retorno['avaliacao'] = $avaliacao['avaliacao'];
        
            $retorno['html'] = $this->load->view('admin/produto/avaliacao/deletar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'delaval'){
            $retorno = $this->produto->deletar_avaliacao();
            echo json_encode($retorno);
        }
    }

	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}


}