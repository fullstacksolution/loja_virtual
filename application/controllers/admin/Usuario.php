<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('UsuarioModel', 'usuario');
        $this->load->model('EnderecoModel', 'endereco');
    }

    public function index() {
        redirect('admin','refresh', '301');
    }

    // API
    public function listar_clientes(){
        $clientes['usuarios'] = $this->usuario->get_clientes();
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Clientes | Admin';
            $clientes['subtitulo'] = 'cliente';
            $clientes['descricao'] = 'Lista de todos os clientes cadastrados';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/usuario/tabela', $clientes,true);
            $this->show($html, $dados);

        } else {
            $retorno['titulo'] = 'Clientes | Admin';
            $retorno['usuarios'] = $clientes['usuarios'];
            $retorno['subtitulo'] = 'cliente';
            $retorno['descricao'] = 'Lista de todos os clientes cadastrados';
            $retorno['html'] = $this->load->view('admin/usuario/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }
    
    public function listar_administradores(){
        $administradores['usuarios'] = $this->usuario->get_administradores();
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Administradores | Admin';
            $administradores['subtitulo'] = 'administrador';
            $administradores['descricao'] = 'Lista de todos os administradores cadastrados';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/usuario/tabela', $administradores,true);
            $this->show($html, $dados);

        } else {
            $retorno['titulo'] = 'Administradores | Admin';
            $retorno['usuarios'] = $administradores['usuarios'];
            $retorno['subtitulo'] = 'administrador';
            $retorno['descricao'] = 'Lista de todos os administradores cadastrados';
            $retorno['html'] = $this->load->view('admin/usuario/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function editar_usuario($id){
        $usuario['usuario'] = $this->usuario->get_usuario_by_id($id);
		$enderecos = $this->endereco->get_endereco_by_id($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar usuário | Admin';
            $usuario['subtitulo'] = $usuario['usuario']->nome.' '.$usuario['usuario']->sobrenome;
            $usuario['descricao'] = 'Editar informações de cadastro de '.$usuario['usuario']->nome;
            $usuario['endereco'] = $enderecos;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/usuario/editar', $usuario,true);
            $this->show($html, $dados);

        } else {
            $retorno['titulo'] = 'Editar usuário | Admin';
            $retorno['usuario'] = $usuario['usuario'];
            $retorno['subtitulo'] = $usuario['usuario']->nome.' '.$usuario['usuario']->sobrenome;
            $retorno['descricao'] = 'Editar informações de cadastro de '.$usuario['usuario']->nome;
            $retorno['endereco'] = $enderecos;
            $retorno['html'] = $this->load->view('admin/usuario/editar', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function deletar_usuario($id){
        if(!$this->usuario->get_usuario_by_id($id) || $this->usuario->get_usuario_tipo($id)->tipo >= 3) {
            redirect('admin/','refresh');
        }
        $usuario['usuario'] = $this->usuario->get_usuario_by_id($id);
		$enderecos = $this->endereco->get_endereco_by_id($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar usuário | Admin';
            $usuario['subtitulo'] = $usuario['usuario']->nome.' '.$usuario['usuario']->sobrenome;
            $usuario['descricao'] = 'Deletar cadastro de '.$usuario['usuario']->nome;
            $usuario['endereco'] = $enderecos;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/usuario/deletar', $usuario,true);
            $this->show($html, $dados);

        } else {
            $retorno['titulo'] = 'Deletar usuário | Admin';
            $retorno['usuario'] = $usuario['usuario'];
            $retorno['subtitulo'] = $usuario['usuario']->nome.' '.$usuario['usuario']->sobrenome;
            $retorno['descricao'] = 'Deletar cadastro de '.$usuario['usuario']->nome;
            $retorno['endereco'] = $enderecos;

            $retorno['html'] = $this->load->view('admin/usuario/deletar', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function cadastrar_usuario(){
        $rota = $this->uri->segment(2);
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Cadastrar '.$rota.' | Admin';
            $dados['subtitulo'] = 'Cadastrar novo '.$rota;
            $dados['descricao'] = 'Cadastrar um novo '.$rota.' no sistema';
            $dados['rota'] = $rota;
            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/usuario/cadastrar', $dados, true);
            $this->show($html,$dados);
        } else {
            $retorno['titulo'] = 'Cadastrar usuário | Admin';
            $retorno['subtitulo'] = 'Cadastrar novo usuário';
            $retorno['descricao'] = 'Cadastrar um novo usuário no sistema';
            $retorno['rota'] = $rota;

            $retorno['html'] = $this->load->view('admin/usuario/cadastrar', $retorno, true);
            echo json_encode($retorno);
        }
    }


    // Edits
    public function altemail(){
		$retorno = $this->usuario->editar_email();
		echo json_encode($retorno);
	}
    public function altcpf(){
		$retorno = $this->usuario->editar_cpf();
		echo json_encode($retorno);
    }
    public function deletar(){
        $retorno = $this->usuario->deletar();
        echo json_encode($retorno);
    }

    // Gets

    public function get_usuarios_ativos(){
        if($_POST['request'] == 'admin'){
            $retorno = $this->usuario->get_usuarios_by_status(1);
            echo json_encode($retorno);
        }
    }

    public function get_usuarios_inativos(){
        if($_POST['request'] == 'admin'){
            $retorno = $this->usuario->get_usuarios_by_status(0);
            echo json_encode($retorno);
        }
    }

	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}

}