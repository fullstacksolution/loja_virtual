<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('DepartamentoModel', 'departamento');
        $this->load->model('CategoriaModel', 'categoria');
    }
    
    public function index(){
        
    }

    public function listar_departamentos(){
        $departamentos['departamentos'] = $this->departamento->get_departamentos();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Departamentos | Admin';
            $departamentos['subtitulo'] = 'departamento';
            $departamentos['descricao'] = 'Lista de todos os departamentos cadastrados';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/departamento/tabela', $departamentos,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Departamentos | Admin';
            $retorno['departamentos'] = $departamentos['departamentos'];
            $retorno['subtitulo'] = 'departamento';
            $retorno['descricao'] = 'Lista de todos os departamentos cadastrados';
            $retorno['html'] = $this->load->view('admin/departamento/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function editar_departamento($id){
        $departamento['departamento'] = $this->departamento->get_departamento_by_id($id);
        
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar departamento | Admin';
            $departamento['subtitulo'] = $departamento['departamento']->nome;
            $departamento['descricao'] = 'Editar departamento '.$departamento['departamento']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/departamento/editar', $departamento,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Editar departamento | Admin';
            $retorno['departamento'] = $departamento['departamento'];
            $retorno['subtitulo'] = $departamento['departamento']->nome;
            $retorno['descricao'] = 'Editar departamento '.$departamento['departamento']->nome;
            $retorno['html'] = $this->load->view('admin/departamento/editar', $retorno, true);
            echo json_encode($retorno);
        } else if($_POST['request'] == 'edit'){
		    $retorno = $this->departamento->editar();
            echo json_encode($retorno);
        }
    }

    public function deletar_departamento($id){
        $departamento['departamento'] = $this->departamento->get_departamento_by_id($id);
        $departamento['categorias'] = $this->categoria->get_categorias_by_departamento($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar departamento | Admin';
            $departamento['subtitulo'] = $departamento['departamento']->nome;
            $departamento['descricao'] = 'Deletar departamento '.$departamento['departamento']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/departamento/deletar', $departamento,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Deletar departamento | Admin';
            $retorno['departamento'] = $departamento['departamento'];
            $retorno['subtitulo'] = $departamento['departamento']->nome;
            $retorno['categorias'] = $departamento['categorias'];
            $retorno['descricao'] = 'Deletar departamento '.$departamento['departamento']->nome;

            $retorno['html'] = $this->load->view('admin/departamento/deletar', $retorno, true);

            echo json_encode($retorno);
        } else if($_POST['request'] == 'delete'){
            $retorno = $this->departamento->deletar();
            echo json_encode($retorno);
        }
    }

    public function criar_departamento(){
        $rota = $this->uri->segment(2);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Criar '.$rota.' | Admin';
            $dados['subtitulo'] = 'Criar novo '.$rota;
            $dados['descricao'] = 'Criar um novo '.$rota.' de produtos e categorias';
            $dados['rota'] = $rota;
            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/departamento/criar', $dados, true);
            $this->show($html,$dados);
        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Criar '.$rota.' | Admin';
            $retorno['subtitulo'] = 'Criar novo '.$rota;
            $retorno['descricao'] = 'Criar um novo '.$rota.' de produtos e categorias';
            $retorno['rota'] = $rota;

            $retorno['html'] = $this->load->view('admin/departamento/criar', $retorno, true);
            echo json_encode($retorno);
        } else if($_POST['request'] == 'criar'){
            $retorno = $this->departamento->criar();
            echo json_encode($retorno);
        }
    }
    
	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}


}