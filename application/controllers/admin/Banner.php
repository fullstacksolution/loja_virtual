<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('BannerModel', 'banner');
    } 

    public function index() {
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Banner | Admin';
            $dados['subtitulo'] = 'banner';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/banner/lista', null ,true);
            $html .= $this->load->view('admin/banner/criar', null ,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Banner | Admin';
            $retorno['subtitulo'] = 'banner';
            $retorno['html'] = $this->load->view('admin/banner/lista', null, true);
            $retorno['html'] .= $this->load->view('admin/banner/criar', null ,true);
            echo json_encode($retorno);
        }
    }

    public function get_lista_banners(){
        if( isset($_POST['request']) && $_POST['request'] == 'admin' ){
            $dados['banners'] = $this->banner->get_lista_banners();
            echo json_encode($dados);
        }
    }

    public function adicionar(){
        if( isset($_POST['request']) && $_POST['request'] == 'admin' ){
            $retorno = $this->banner->adicionar();
            echo json_encode($retorno);
        }
    }

    public function editar($id){
        $banner['banner'] = $this->banner->get_banner_by_id($id);
        
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar banner | Admin';
            $banner['subtitulo'] = $banner['banner']->nome;
            $banner['descricao'] = 'Editar banner '.$banner['banner']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/banner/editar', $banner,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Editar banner | Admin';
            $retorno['subtitulo'] = $banner['banner']->nome;
            $retorno['descricao'] = 'Editar banner '.$banner['banner']->nome;

            $retorno['banner'] = $banner['banner'];

            $retorno['html'] = $this->load->view('admin/banner/editar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'edit'){
		    $retorno = $this->banner->editar();
            echo json_encode($retorno);
        }
    }

    public function deletar($id){
        $banner['banner'] = $this->banner->get_banner_by_id($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar banner | Admin';
            $banner['subtitulo'] = $banner['banner']->nome;
            $banner['descricao'] = 'Deletar banner '.$banner['banner']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/banner/deletar', $banner,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Deletar banner | Admin';
            $retorno['banner'] = $banner['banner'];
            $retorno['subtitulo'] = $banner['banner']->nome;
            $retorno['descricao'] = 'Deletar banner '.$banner['banner']->nome;

            $retorno['html'] = $this->load->view('admin/banner/deletar', $retorno, true);

            echo json_encode($retorno);
        } else if($_POST['request'] == 'delete'){
            $retorno = $this->banner->deletar();
            echo json_encode($retorno);
        }
    }

	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}

}