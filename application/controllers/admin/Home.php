<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
    }

    public function index() {
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Painel administrativo | Admin';
            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/painel', null ,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Painel administrativo | Admin';
            $retorno['html'] = $this->load->view('admin/painel', $retorno, true);
            echo json_encode($retorno);
        }
    }

	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}

}