<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

    public function __construct(){
        parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect('loja','refresh');
        }
        $this->load->model('DepartamentoModel', 'departamento');
        $this->load->model('CategoriaModel', 'categoria');
        $this->load->model('SubcategoriaModel', 'subcategoria');
    }
    
    public function index(){
        
    }
    
    public function criar_categoria(){
        $rota = $this->uri->segment(2);
        $dados['departamentos'] = $this->departamento->get_departamentos();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Criar '.$rota.' | Admin';
            $dados['subtitulo'] = 'Criar nova '.$rota;
            $dados['descricao'] = 'Criar uma nova '.$rota.' de produtos';
            $dados['rota'] = $rota;
            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/categoria/criar', $dados, true);
            $this->show($html,$dados);
        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Criar '.$rota.' | Admin';
            $retorno['subtitulo'] = 'Criar nova '.$rota;
            $retorno['descricao'] = 'Criar um nova '.$rota.' de produtos';
            $retorno['rota'] = $rota;
            $retorno['departamentos'] = $dados['departamentos'];

            $retorno['html'] = $this->load->view('admin/categoria/criar', $retorno, true);
            echo json_encode($retorno);
        } else if($_POST['request'] == 'criar'){
            $retorno = $this->categoria->criar();
            echo json_encode($retorno);
        }
    }
    
    public function listar_categorias(){
        $categorias['categorias'] = $this->categoria->get_categorias();

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Categorias | Admin';
            $categorias['subtitulo'] = 'categoria';
            $categorias['descricao'] = 'Lista de todas as categorias cadastradas';

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/categoria/tabela', $categorias,true);
            $this->show($html, $dados);
        } else {
            $retorno['titulo'] = 'Categorias | Admin';
            $retorno['categorias'] = $categorias['categorias'];
            $retorno['subtitulo'] = 'categoria';
            $retorno['descricao'] = 'Lista de todas as categorias cadastradas';
            $retorno['html'] = $this->load->view('admin/categoria/tabela', $retorno, true);
            echo json_encode($retorno);
        }
    }

    public function editar_categoria($id){
        $categoria['categoria'] = $this->categoria->get_categoria_by_id($id);
        $categoria['departamentos'] = $this->departamento->get_departamentos();
        
        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Editar categoria | Admin';
            $categoria['subtitulo'] = $categoria['categoria']->nome;
            $categoria['descricao'] = 'Editar categoria '.$categoria['categoria']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/categoria/editar', $categoria,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin') {
            $retorno['titulo'] = 'Editar categoria | Admin';
            $retorno['subtitulo'] = $categoria['categoria']->nome;
            $retorno['descricao'] = 'Editar categoria '.$categoria['categoria']->nome;

            $retorno['categoria'] = $categoria['categoria'];
            $retorno['departamentos'] = $categoria['departamentos'];

            $retorno['html'] = $this->load->view('admin/categoria/editar', $retorno, true);
            echo json_encode($retorno);

        } else if($_POST['request'] == 'edit'){
		    $retorno = $this->categoria->editar();
            echo json_encode($retorno);
        }
    }

    public function deletar_categoria($id){
        $categoria['categoria'] = $this->categoria->get_categoria_by_id($id);
        $categoria['subcategorias'] = $this->subcategoria->get_subcategorias_by_categoria($id);

        if(!isset($_POST['request'])) {
            $dados['titulo'] = 'Deletar categoria | Admin';
            $categoria['subtitulo'] = $categoria['categoria']->nome;
            $categoria['descricao'] = 'Deletar categoria '.$categoria['categoria']->nome;

            $html = $this->load->view('admin/navbar', null ,true);
            $html .= $this->load->view('admin/categoria/deletar', $categoria,true);
            $this->show($html, $dados);

        } else if($_POST['request'] == 'admin'){
            $retorno['titulo'] = 'Deletar categoria | Admin';
            $retorno['categoria'] = $categoria['categoria'];
            $retorno['subcategorias'] = $categoria['subcategorias'];
            $retorno['subtitulo'] = $categoria['categoria']->nome;
            $retorno['descricao'] = 'Deletar categoria '.$categoria['categoria']->nome;

            $retorno['html'] = $this->load->view('admin/categoria/deletar', $retorno, true);

            echo json_encode($retorno);
        } else if($_POST['request'] == 'delete'){
            $retorno = $this->categoria->deletar();
            echo json_encode($retorno);
        }
    }

	public function show($html, $dados=null, $js=null) {
		$aux = $this->load->view('admin/common/header', $dados, true);
        $aux .= $html;
        $aux .= $this->load->view('admin/rodape', null ,true);
        $js['js'] = $this->load->view('js/mask', null, true);
        $js['js'] .= $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
		$aux .= $this->load->view('admin/common/footer', $js, true);
		echo $aux;
	}


}