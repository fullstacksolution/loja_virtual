<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logado')){
			redirect('sign/in','refresh');
		}
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
		$this->load->model('EnderecoModel', 'endereco');
		$this->load->model('UsuarioModel', 'usuario');
        $this->load->model('CarrinhoModel', 'carrinho');
	}

    public function index(){
		$dados['titulo'] = 'Perfil | IPCI';
		$dados['endereco'] = $this->endereco->get_endereco_by_id($this->session->userdata('userlogado')->id);
		$html = $this->load->view('usuario/perfil', $dados, true);
		
		$this->show($html,$dados);
	}
	
	// Métodos de edição
	public function infouser(){
		$retorno = $this->usuario->editar();
		echo json_encode($retorno);
	}

	public function altsenha(){
		$retorno = $this->usuario->editar_senha();
		echo json_encode($retorno);
	}
	

	public function infoentrega(){
		$retorno = $this->endereco->editar();
		echo json_encode($retorno);
	}

	public function get_endereco(){
		if($this->session->userdata('userlogado')->tipo < 2){ 
			$id = $this->session->userdata('userlogado')->id;
		} else {
			$id = $this->input->post('id');
		}
		$enderecos = $this->endereco->get_endereco_by_id($id);
		echo json_encode($enderecos);			
	}
    
	public function show($html, $dados=null) {
		// Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
			foreach ($dados['departamentos'] as $departamento) {
				$ids_departamentos[] .= $departamento->id;
			}
			$dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
			$ids_categorias = array();
			foreach ($dados['categorias'] as $categoria) {
				$ids_categorias[] .= $categoria->id;
			}
			$dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
		}
		$dados['carrinho'] = $this->carrinho->get_qtd_produtos();
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
		$aux .= $this->load->view('loja/navbar', null, true);
        $aux .= $html;
		$aux .= $this->load->view('ipci/rodape', null, true);
        $js['js'] = $this->load->view('js/maskbrphone', null, true);
        $js['js'] .= $this->load->view('js/cep', null, true);
        $js['js'] .= $this->load->view('js/cpf_cnpj', null, true);
        $js['js'] .= $this->load->view('js/mask', null, true);
		$aux .= $this->load->view('common/footer', $js, true);
		echo $aux;
	}

}