<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {
	public function index(){
		
	}

    public function error404() 
    {
		$dados['titulo'] = 'Erro 404 | IPCI';
		$html = $this->load->view('errors/error404', null, true);
		$this->show($html,$dados);
    }

	public function show($html, $dados=null) {
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $html;
		$aux .= $this->load->view('common/footer', null, true);
		echo $aux;
	}

}