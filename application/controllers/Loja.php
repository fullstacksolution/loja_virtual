<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loja extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('DepartamentoModel', 'departamento');
		$this->load->model('CategoriaModel', 'categoria');
		$this->load->model('SubcategoriaModel', 'subcategoria');
		$this->load->model('ProdutoModel', 'produto');
		$this->load->model('CarrinhoModel', 'carrinho');
	}
	
    public function index() {
		$dados['titulo'] = 'Loja | IPCI';
		
		$this->load->model('BannerModel', 'banner');
		$banners['banners'] = $this->banner->get_banners();
		
		$html = $this->load->view('loja/carrosel', $banners, true);

		$destaques['destaques'] = $this->produto->get_produtos_destaques();
		$html .= $this->load->view('loja/destaques_carrosel', $destaques, true);
		
		$this->show($html,$dados);
	}

	public function busca($pular=null, $prod_por_pagina=null){
		if( isset($_GET['request']) && $_GET['request'] == 'busca' && isset($_GET['busca'])){

			$this->load->helper('text');
			// Texto do campo de busca
			$busca = $_GET['busca'];
			// Limita a quantidade de palavras para apenas 4
			$busca = word_limiter($busca, 4);
			
			// Pagination
			$this->load->library('pagination');
						
			$config['base_url'] = base_url('busca');
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
			$prod_por_pagina = 12;

			if(isset($_GET['filtro'])){
				// Filtros
				if($_GET['filtro'] == 'preco_asc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'preco', 'ASC');
					
				} else if($_GET['filtro'] == 'preco_desc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'preco', 'DESC');

				} else if($_GET['filtro'] == 'avaliacao_desc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'media_avaliacao', 'DESC');

				} else if($_GET['filtro'] == 'avaliacao_5'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 5);

				} else if($_GET['filtro'] == 'avaliacao_4'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 4);

				} else if($_GET['filtro'] == 'avaliacao_3'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 3);

				} else if($_GET['filtro'] == 'avaliacao_2'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 2);

				} else if($_GET['filtro'] == 'avaliacao_1'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 1);

				}
			} else {
				$dados['produtos'] = $this->produto->get_produtos_busca($busca, null, null);
			}

			$config['total_rows'] = count($dados['produtos'])+1;
			$config['per_page'] = $prod_por_pagina;

			$this->pagination->initialize($config);

			$dados['links_paginacao'] = $this->pagination->create_links();

			if(isset($_GET['filtro'])){
				// Filtros
				if($_GET['filtro'] == 'preco_asc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'preco', 'ASC', $pular, $prod_por_pagina);
					
				} else if($_GET['filtro'] == 'preco_desc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'preco', 'DESC', $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_desc'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'media_avaliacao', 'DESC', $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_5'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 5, $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_4'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 4, $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_3'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 3, $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_2'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 2, $pular, $prod_por_pagina);

				} else if($_GET['filtro'] == 'avaliacao_1'){
					$dados['produtos'] = $this->produto->get_produtos_busca($busca, 'avaliacao', 1, $pular, $prod_por_pagina);

				}
			} else {
				$dados['produtos'] = $this->produto->get_produtos_busca($busca, null, null, $pular, $prod_por_pagina);
			}

			$dados['pagina'] = 'busca';
			$dados['titulo'] = 'Buscar '.$busca.' | IPCI';
			$dados['url_filtro'] = base_url('busca?request=busca&busca='.$_GET['busca']);

			$html = $this->load->view('loja/barra_lateral', $dados, true);
			$html .= $this->load->view('loja/lista_produtos', null, true);

			$destaques['destaques'] = $this->produto->get_produtos_destaques();
			$html .= $this->load->view('loja/destaques_carrosel', $destaques, true);
			
			$this->show($html,$dados);
			
		} else {
			redirect('loja','refresh');
		}
	}

	public function show($html, $dados=null) {
		// Get departamentos, categorias e subcategorias na qual possam produtos publicados
		$dados['departamentos'] = $this->departamento->get_departamentos_loja();
		$ids_departamentos = array();
		if(isset($dados['departamentos'][0])){
			foreach ($dados['departamentos'] as $departamento) {
				$ids_departamentos[] .= $departamento->id;
			}
			$dados['categorias'] = $this->categoria->get_categorias_by_departamentos($ids_departamentos);
			$ids_categorias = array();
			foreach ($dados['categorias'] as $categoria) {
				$ids_categorias[] .= $categoria->id;
			}
			$dados['subcategorias'] = $this->subcategoria->get_subcategorias_by_categorias($ids_categorias);
		}
		$dados['carrinho'] = $this->carrinho->get_qtd_produtos();
		$aux = $this->load->view('common/header', $dados, true);
		$aux .= $this->load->view('loja/nav', null, true);
		$aux .= $this->load->view('loja/navbar', null, true);
		$aux .= $html;
		$aux .= $this->load->view('ipci/rodape', null, true);
		$aux .= $this->load->view('common/footer', null, true);
		echo $aux;
	}
}

