<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manutencao extends CI_Controller {

    public function index()
    {
        $dados['titulo'] = 'Em construção | IPCI';

        $this->load->view('common/header', $dados);
        $this->load->view('manutencao');
        $this->load->view('common/footer');
    }

}