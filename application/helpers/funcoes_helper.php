﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function limpar($string){
	$table = array(
        '/'=>'', '('=>'', ')'=>'',
    );
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
	$string= preg_replace('/[,.;:`´^~\'"]/', null, iconv('UTF-8','ASCII//TRANSLIT',$string));
	$string= strtolower($string);
	$string= str_replace(" ", "", $string);
	$string= str_replace("---", "-", $string);
	$string= str_replace("-", "", $string);
	return $string;
}
function url_amigavel($string){
	$table = array(
        '/'=>'', '('=>'', ')'=>'',
    );
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
	$string= preg_replace('/[,.;:`´^~\'"]/', null, iconv('UTF-8','ASCII//TRANSLIT',$string));
	$string= strtolower($string);
	$string= str_replace(" ", "-", $string);
	$string= str_replace("---", "-", $string);
	return $string;
}
function preco($string){
	// 10.000,50
	$string= str_replace(".", "", $string);
	// 10000,50
	$string= str_replace(",", ".", $string);
	// 10000.50
	// settype($string, "float");
	// 10000.5
	if(strpos($string, '.')){
		$depois = strstr($string, '.');
		$depois = str_replace(".", "", $depois);
		$antes = strstr($string, '.', true);
		$antes = str_replace(".", "", $antes);
		if(strlen($depois) == 1){
			$depois = $depois.'0';
		}
		$string = $antes.'.'.$depois;
	} 

	return $string;
}
function transforma_preco($string){
	settype($string, "string");
	if(strpos($string, '.')){
		$depois = strstr($string, '.');
		$depois = str_replace(".", "", $depois);
		$antes = strstr($string, '.', true);
		$antes = str_replace(".", "", $antes);
		if(strlen($antes) == 4){
			$antes = $antes[0].'.'.$antes[1].$antes[2].$antes[3];
		} else if (strlen($antes) == 5){
			$antes = $antes[0].$antes[1].'.'.$antes[2].$antes[3].$antes[4];
		}
		if(strlen($depois) == 1){
			$depois = $depois.'0';
		}
		$string = $antes.','.$depois;
	} else if($string != null) {
		$string = $string.'.00';
		$depois = strstr($string, '.');
		$depois = str_replace(".", "", $depois);
		$antes = strstr($string, '.', true);
		$antes = str_replace(".", "", $antes);
		if(strlen($antes) == 4){
			$antes = $antes[0].'.'.$antes[1].$antes[2].$antes[3];
		} else if (strlen($antes) == 5){
			$antes = $antes[0].$antes[1].'.'.$antes[2].$antes[3].$antes[4];
		}
		if(strlen($depois) == 1){
			$depois = $depois.'0';
		}
		$string = $antes.','.$depois;
	}
	return $string;
}
function verifica_kg(float $float = null){
	if (strpos($float,'.') == false && $float != null){
		$float = $float.'000';
	} else if($float != null){
		$depois = strstr($float, '.');
		$depois = str_replace(".", "", $depois);
		$antes = strstr($float, '.', true);
		$antes = str_replace(".", "", $antes);
		if(strlen($depois) == 1){
			$depois = $depois.'00';
		} else if(strlen($depois) == 2){
			$depois = $depois.'0';
		}
		$float = $antes.'.'.$depois;
	} else {
		$float = null;
	}
	return $float;
}
function verifica_cm(float $float = null){
	if (strpos($float,'.') == false && $float != null){
		$float = $float.'00';
	} else if($float != null) {
		$depois = strstr($float, '.');
		$depois = str_replace(".", "", $depois);
		$antes = strstr($float, '.', true);
		$antes = str_replace(".", "", $antes);
		if(strlen($depois) == 1){
			$depois = $depois.'0';
		}
		$float = $antes.'.'.$depois;
	} else {
		$float = null;
	}
	return $float;
}
function validar_cpf($cpf){
	if(strlen($cpf) != 11 ){
		return false;
	}
	$digitoA = 0;
	$digitoB = 0;
	for($i = 0, $x = 10; $i <= 8; $i++, $x--){
		$digitoA += $cpf[$i] * $x;
	}
	for($i = 0, $x = 11; $i <= 9; $i++, $x--){
		if(str_repeat($i, 11) == $cpf){
			return false;
		}
		$digitoB += $cpf[$i] * $x;
	}
	$somaA = (($digitoA%11) < 2 ) ? 0 : 11-($digitoA%11);
	$somaB = (($digitoB%11) < 2 ) ? 0 : 11-($digitoB%11);
	if($somaA != $cpf[9] || $somaB != $cpf[10]){
		return false;	
	}else{
		return true;
	}
}

function validar_cnpj($cnpj)
{
	// Valida tamanho
	if (strlen($cnpj) != 14){
		return false;
	}
	
	// Verifica se todos os digitos são iguais
	if (preg_match('/(\d)\1{13}/', $cnpj)){
		return false;	
	}
	// Valida primeiro dígito verificador
	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)){
		return false;
	}
	// Valida segundo dígito verificador
	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}

function converter_data($string){
    
    $dia_sem= date('w', strtotime($string));

    if($dia_sem == '0'){
    $semana = "Domingo";
    }elseif($dia_sem == '1'){
    $semana = "Segunda-feira";
    }elseif($dia_sem == '2'){
    $semana = "Terça-feira";
    }elseif($dia_sem == '3'){
    $semana = "Quarta-feira";
    }elseif($dia_sem == '4'){
    $semana = "Quinta-feira";
    }elseif($dia_sem == '5'){
    $semana = "Sexta-feira";
    }else{
    $semana = "Sábado";
    }

 	$dia= date('d', strtotime($string));

	$mes_num = date('m', strtotime($string));
 	if($mes_num == '01'){
    $mes= "Janeiro";
    }elseif($mes_num == '02'){
    $mes = "Fevereiro";
    }elseif($mes_num == '03'){
    $mes = "Março";
    }elseif($mes_num == '04'){
    $mes = "Abril";
    }elseif($mes_num == '05'){
    $mes = "Maio";
    }elseif($mes_num == '06'){
    $mes = "Junho";
    }elseif($mes_num == '07'){
    $mes = "Julho";
    }elseif($mes_num == '08'){
    $mes = "Agosto";
    }elseif($mes_num == '09'){
    $mes = "Setembro";
    }elseif($mes_num == '10'){
    $mes = "Outubro";
    }elseif($mes_num == '11'){
    $mes = "Novembro";
    }else{
    $mes = "Dezembro";
    }

    $ano = date('Y', strtotime($string));
    $hora = date('H:i', strtotime($string));
 
    return $semana.', '.$dia.' de '.$mes.' de '.$ano.' '.$hora;
}