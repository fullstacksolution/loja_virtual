<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/BannerLib.php';

class BannerModel extends CI_Model {

    public function adicionar(){

        if(sizeof($_FILES) == 0){
            $erros['erro'] = 'Nenhuma imagem foi selecionada para fazer o upload.';
            $erros['tipo'] = 'imagem';
            return $erros;
        }

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[60]|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('link', 'link', 'min_length[5]|max_length[255]|trim');
        $this->form_validation->set_rules('status', 'status', 'in_list[0,1]');
        
        if ($this->form_validation->run() == FALSE) {
            $erros = array(
                'erro' => 'Erros foram encontrados.',
                'tipo' => 'campos',
                'nome' => form_error('nome', null, null),
                'link' => form_error('link', null, null),
                'status' => form_error('status', null, null)
            );
            return $erros;
        } else {

            // Recuperando valores da imagem de upload
            $file_name = $_FILES['file']['name']; // Nome do arquivo
            $file_type = $_FILES['file']['type']; // Tipo de imagem
            $file_size = $_FILES['file']['size']; // Tamanho da imagem
            $file_tmp_name = $_FILES['file']['tmp_name'];

            if($file_size > 1048576){ // 2097152
                $erros['erro'] = 'A imagem deve ter no máximo o tamanho de 2mb.';
                $erros['tipo'] = 'imagem';
                return $erros;
            }

            // Verificar tipo da imagem
            if($file_type == 'image/jpeg' || $file_type == 'image/png'){

                $banners = $this->get_lista_banners();
                
                $destino = $_SERVER['DOCUMENT_ROOT'].'/uploads/banner/';
                foreach($banners as $banner){
                    if($banner->img == $file_name){
                        $erros['erro'] = 'Você já adicionou um banner com esse nome de arquivo.';
                        $erros['tipo'] = 'imagem';
                        return $erros;
                    }
                }
                
                // Faz o upload da imagem
                if(move_uploaded_file($file_tmp_name,$destino.$file_name)){
                    $sucesso['upload'] = true;
                } else {
                    $erros['erro'] = 'Ocorreu um erro no upload da imagem.';
                    return $erros;
                }                

                // Edição da imagem
                $config['source_image'] = $_SERVER['DOCUMENT_ROOT'].'/uploads/banner/'.$file_name;
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = true;
                // $config['width'] = 1280;
                $config['height'] = 478;

                // Carregando library de edição passando as configurações
                $this->load->library('image_lib', $config);

                // Aplicar a edição da imagem
                if($this->image_lib->resize()){
                    $sucesso['resize'] = true;
                } else {
                    $erros['tipo'] = 'imagem';
                    $erros['erro'] = $this->image_lib->display_errors();
                    return $erros;
                }

                $bannerLib = new BannerLib();
                $nome = ucfirst($this->input->post('nome'));
                $link = $this->input->post('link');
                $status = $this->input->post('status');

                // Inserção no banco de dados
                if($sucesso['upload'] == true && $sucesso['resize'] == true){                
                    if($bannerLib->adicionar($nome, $link, $status, $file_name)){
                        $sucesso = array(
                            'html' => $this->load->view('admin/banner/lista', null, true),
                            'erro' => null,
                            'sucesso' => 'O banner foi salvo com sucesso.',
                            'tipo' => null,
                            'nome' => form_error('nome', null, null),
                            'link' => form_error('link', null, null),
                            'status' => form_error('status', null, null)
                        );                        
                        return $sucesso;
                    }
                }

            } else {
                $erros['erro'] = 'Só são aceitas imagens no formato .jpg ou .png';
                $erros['tipo'] = 'imagem';
                return $erros;
            }
        }
    }

    public function editar(){

        $this->form_validation->set_rules('id', 'id', 'required|min_length[1]|is_numeric');
        $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('link', 'link', 'trim|min_length[3]|max_length[255]');
        $this->form_validation->set_rules('status', 'status', 'in_list[0,1]|is_numeric');
        
        if ($this->form_validation->run() == FALSE) {
            $erros = array(
                'erro' => true,
                'sucesso' => false,
                'id' => form_error('id', null, null),
                'nome' => form_error('nome', null, null),
                'link' => form_error('link', null, null),
                'status' => form_error('status', null, null)
            );
            return $erros;
        } else {

            $id = $this->input->post('id');
            $nome = $this->input->post('nome');
            $link = $this->input->post('link');
            $status = $this->input->post('status');

            $bannerLib = new BannerLib();
            
            if($bannerLib->editar($id, $nome, $link, $status)){
                $sucesso = array(
                    'dados' => $this->banner->get_banner_by_id($id),
                    'erro' => false,
                    'sucesso' => 'O banner foi editado com sucesso.',
                    'id' => form_error('id', null, null),
                    'nome' => form_error('nome', null, null),
                    'link' => form_error('link', null, null),
                    'status' => form_error('status', null, null)
                );
                return $sucesso;
            }
        }
            
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;

        $id = $this->input->post('id');
        
        $bannerLib = new BannerLib();

        $img = $this->get_banner_by_id($id)->img;

        if(unlink($_SERVER['DOCUMENT_ROOT'].'/uploads/banner/'.$img)){
            
            // Deletar
            if($bannerLib->deletar($id)) { 
                $sucesso['dados'] = array(
                    'id'=>$id
                );
                $sucesso['erro'] = false;
                $sucesso['delete'] = 1;
                return $sucesso;
                
            } else {
                $sucesso['erro'] = 'Ocorreu um erro no servidor.';
                $erros['500'] = 1;
                return $erros;
            }

        } else {
            $erros['erro'] = 'O servidor não conseguiu apagar o arquivo da imagem.';
            return $erros;
        }

    }

    public function get_banner_by_id($id){
        $bannerLib = new BannerLib();
        return $bannerLib->get_banner_by_id($id);        
    }

    public function get_lista_banners(){
        $bannerLib = new BannerLib();
        return $bannerLib->get_lista_banners();
    }

    public function get_banners(){
        $bannerLib = new BannerLib();
        return $bannerLib->get_banners();
    }

}
