<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."libraries/PHPMailer/Exception.php";
require_once APPPATH."libraries/PHPMailer/OAuth.php";
require_once APPPATH."libraries/PHPMailer/PHPMailer.php";
require_once APPPATH."libraries/PHPMailer/POP3.php";
require_once APPPATH."libraries/PHPMailer/SMTP.php";

require_once APPPATH."libraries/Mensagem.php";

class ContatoModel extends CI_Model {

    public function enviar_mensagem(){

        if($this->input->post('g-recaptcha-response') == ""){
            $erros['erro'] = true;
            $erros['g-recaptcha'] = 'Você precisa fazer a verificação reCAPTCHA.';
            $erros['retorno'] = false;
            return $erros;
        }

        $captcha = $this->input->post('g-recaptcha-response');
        $secreto = '6Ld48sEUAAAAAIZI40eG_Ykd_8g2Ho3vOZu9jS-l';
        $ip = $_SERVER['REMOTE_ADDR'];
        $var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secreto&response=$captcha&remoteip=$ip");
        $resposta = json_decode($var, true);
        
        if(!$resposta['success']){
            $erros['erro'] = true;
            $erros['g-recaptcha'] = 'A validação reCAPTCHA está inválida.';
            $erros['retorno'] = false;
            return $erros;
        }

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('sobrenome', 'sobrenome', 'min_length[3]|max_length[40]');
        $this->form_validation->set_rules('email', 'email', 'required|min_length[6]|max_length[40]|trim|valid_email');
        $this->form_validation->set_rules('telefone', 'telefone', 'min_length[14]|max_length[15]');
        $this->form_validation->set_rules('celular', 'celular', 'exact_length[15]');
        $this->form_validation->set_rules('tipo', 'tipo', 'required|in_list[Dúvida,Sugestão,Reclamação,Serviços]');
        $this->form_validation->set_rules('assunto', 'assunto', 'required|min_length[5]|max_length[127]');
        $this->form_validation->set_rules('mensagem', 'mensagem', 'required|min_length[10]|max_length[255]');
        $this->form_validation->set_rules('meio', 'meio de resposta', 'required|in_list[Email,Telefone,WhatsApp]');
        $this->form_validation->set_rules('horario', 'horário', 'exact_length[5]');
        $this->form_validation->set_rules('cc', 'verificação de cópia', 'in_list[on]');
        $this->form_validation->set_rules('g-recaptcha', 'reCAPTCHA', '');

        
        if ($this->form_validation->run() == FALSE) {
            $erros = array(
                'retorno' => false,
                'erro' => true,
                'nome' => form_error('nome', null, null),
                'sobrenome' => form_error('sobrenome', null, null),
                'email' => form_error('email', null, null),
                'telefone' => form_error('telefone', null, null),
                'celular' => form_error('celular', null, null),
                'tipo' => form_error('tipo', null, null),
                'assunto' => form_error('assunto', null, null),
                'mensagem' => form_error('mensagem', null, null),
                'meio' => form_error('meio', null, null),
                'horario' => form_error('horario', null, null),
                'cc' => form_error('cc', null, null)
            );
            return $erros;
        } else if( ($this->input->post('meio') == 'Telefone' || $this->input->post('meio') == 'WhatsApp') && $this->input->post('horario') == null ) {
            $erros = array(
                'retorno' => false,
                'erro' => true,
                'nome' => form_error('nome', null, null),
                'sobrenome' => form_error('sobrenome', null, null),
                'email' => form_error('email', null, null),
                'telefone' => form_error('telefone', null, null),
                'celular' => form_error('celular', null, null),
                'tipo' => form_error('tipo', null, null),
                'assunto' => form_error('assunto', null, null),
                'mensagem' => form_error('mensagem', null, null),
                'meio' => form_error('meio', null, null),
                'horario' => 'Você precisa especificar um horário caso tenha selecionado resposta por WhatsApp ou telefone.',
                'cc' => form_error('cc', null, null)
            );
            return $erros;
        } else {
            $nome = ucfirst($this->input->post('nome'));
            $sobrenome = ucfirst($this->input->post('sobrenome'));
            $email = $this->input->post('email');
            $telefone = $this->input->post('telefone');
            $celular = $this->input->post('celular');
            $tipo = $this->input->post('tipo');
            $assunto = ucfirst($this->input->post('assunto'));
            $mensagem = ucfirst($this->input->post('mensagem'));
            $meio = $this->input->post('meio');
            if($this->input->post('horario') != ''){
                $horario = $this->input->post('horario');
            } else {
                $horario = 'Qualquer';
            }
            $cc = $this->input->post('cc');

            $mail = new Mensagem();
            $mail->__set('nome', $nome);
            $mail->__set('sobrenome', $sobrenome);
            $mail->__set('email', $email);
            $mail->__set('telefone', $telefone);
            $mail->__set('celular', $celular);
            $mail->__set('tipo', $tipo);
            $mail->__set('assunto', $assunto);
            $mail->__set('mensagem', $mensagem);
            $mail->__set('meio', $meio);
            $mail->__set('horario', $horario);
            $mail->__set('cc', $cc);
            $enviado = $mail->enviar();

            if($enviado){
                $sucesso = array(
                    'erro' => true,
                    'nome' => form_error('nome', null, null),
                    'sobrenome' => form_error('sobrenome', null, null),
                    'email' => form_error('email', null, null),
                    'telefone' => form_error('telefone', null, null),
                    'celular' => form_error('celular', null, null),
                    'tipo' => form_error('tipo', null, null),
                    'assunto' => form_error('assunto', null, null),
                    'mensagem' => form_error('mensagem', null, null),
                    'meio' => form_error('meio', null, null),
                    'horario' => form_error('horario', null, null),
                    'cc' => form_error('cc', null, null)
                );
                $sucesso['retorno'] = $enviado;
                return $sucesso;
            } else {
                $erros['erro'] = false;
                $erros['aviso'] = 'Ocorreu um erro na biblioteca.';
                return $erros;
            }
        }
        
        
    }

}
