<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/CategoriaLib.php';

class CategoriaModel extends CI_Model {

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('nome', 'nome da categoria', 'required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('id_departamento', 'departamento', 'required|min_length[1]|max_length[2]|is_natural_no_zero');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_departamento' => form_error('id_departamento', null, null)
            );
            return $erros;
        } else {
            $nome = ucfirst($this->input->post('nome'));
            $id_departamento = $this->input->post('id_departamento');
            $id = $this->input->post('id');
            
            $categoriaLib = new CategoriaLib();
			if($categoriaLib->editar($nome, $id_departamento, $id)) {
				$dados = array(
                    'nome' => $nome,
                    'id_departamento' => $id_departamento,
                    'id' => $id
				);
				$sucesso = array(
					'nome' => form_error('nome', null, null),
					'id_departamento' => form_error('id_departamento', null, null),
					'edit' => 1,
					'dados' => $dados
				);
				return $sucesso;
					
            } else {
				$erros['500'] = 1;
				return $erros;
            }
		}
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;
        $this->load->model('SubcategoriaModel', 'subcategoria');

        $id = $this->input->post('id');
        
        if(!$this->subcategoria->get_subcategorias_by_categoria($id)){

            $categoriaLib = new CategoriaLib();
            // Deletar
            if($categoriaLib->deletar($id)) { 
                $sucesso['dados'] = array(
                    'id'=>$id
                );           
                $sucesso['delete'] = 1;
                return $sucesso;
                    
            } else {
                $erros['500'] = 1;
                return $erros;
            }
        } else {
            $erros['erro'] = 'Você não pode deletar uma categoria enquanto ela estiver associada a uma ou mais subcategorias.';
            return $erros;
        }
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'nome da categoria', 'required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('id_departamento', 'departamento', 'required|min_length[1]|max_length[2]|is_natural_no_zero');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_departamento' => form_error('id_departamento', null, null)
            );
            return $erros;
        } else {

            $nome = ucfirst($this->input->post('nome'));
            $id_departamento = $this->input->post('id_departamento');

            $categorias = $this->get_categorias_by_nome_departamento($nome, $id_departamento);

            if($categorias == 0){
                // Carregando lib de categorias
                $categoriaLib = new CategoriaLib();

                if($categoriaLib->criar($nome, $id_departamento)) {
                    
                    $sucesso['nome'] = form_error('nome', null, null);                
                    $sucesso['id_departamento'] = form_error('id_departamento', null, null);                
                    $sucesso['redirect'] = base_url('admin/categoria/criar');
                    
                    return $sucesso;
                } else {
                    $erros['500'] = 1;
                    return $erros;
                }  
            } else {
                $erros = array(
                    'nome' => 'Já existe uma categoria com esse nome no departamento selecionado',
                    'id_departamento' => form_error('id_departamento', null, null)
                );
                return $erros;
            }
        } 
    }

    public function get_categorias(){
        $this->db->select('cat.id, cat.nome, count(sub.id) as qtd_subcategorias, dep.nome as departamento, dep.id as id_departamento');
        $this->db->from('tb_categoria cat');
        $this->db->join('tb_subcategoria sub', 'sub.id_categoria = cat.id', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->group_by('cat.id');
        return $this->db->get()->result();
    }

    public function get_categoria_by_id($id){
        $this->db->select('cat.id, cat.nome, dep.id as id_departamento, dep.nome as nome_departamento');
        $this->db->from('tb_categoria cat');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('cat.id', $id);
        return $this->db->get()->row_object();
    }

    public function get_categorias_by_departamento($id){
        $this->db->where('id_departamento', $id);
        return $this->db->count_all_results('tb_categoria');
    }

    // Buscar categorias pelo nome dela e pelo id do departamento
    public function get_categorias_by_nome_departamento($categoria, $id_departamento){
        $this->db->where('nome', $categoria);
        $this->db->where('id_departamento', $id_departamento);
        return $this->db->count_all_results('tb_categoria');
    }

    public function get_categorias_by_departamentos(array $ids_departamentos){
        $this->db->select('cat.id, cat.nome, cat.id_departamento, count(sub.id) as qtd_subcategorias');
        $this->db->from('tb_categoria cat');
        $this->db->join('tb_subcategoria sub', 'sub.id_categoria = cat.id', 'inner');
        $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
        $this->db->where('prod.status', 1);
        $this->db->where_in('cat.id_departamento', $ids_departamentos);
        $this->db->group_by('cat.id');
        return $this->db->get()->result();
    }

    public function get_categoria_by_name_departamento($categoria, $departamento){

        $categorias_bd = $this->get_categorias();

        foreach($categorias_bd as $categoria_bd){

            if(url_amigavel($categoria_bd->nome) === $categoria && $categoria_bd->id_departamento == $departamento){

                $this->db->select('cat.id, cat.nome, count(sub.id) as qtd_subcategorias, count(prod.id) as qtd_produtos');
                $this->db->from('tb_categoria cat');
                $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
                $this->db->join('tb_subcategoria sub', 'sub.id_categoria = cat.id', 'inner');
                $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
                $this->db->where('prod.status', 1);
                $this->db->where('cat.id', $categoria_bd->id);
                return $this->db->get()->row_object();
            }

        }
    }

}