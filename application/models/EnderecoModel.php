<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/EnderecoLib.php';

class EnderecoModel extends CI_Model {

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('cep', 'CEP', 'required|min_length[9]|max_length[9]');
        $this->form_validation->set_rules('endereco', 'endereço', 'required|min_length[6]|max_length[40]');
        $this->form_validation->set_rules('numero', 'número', 'is_natural_no_zero|required|min_length[1]|max_length[6]');
        $this->form_validation->set_rules('complemento', 'complemento', 'min_length[1]|max_length[15]');
        $this->form_validation->set_rules('bairro', 'bairro', 'required|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('cidade', 'cidade', 'required|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('uf', 'UF', 'required|exact_length[2]');
        $this->form_validation->set_rules('tipo', 'tipo', 'required|exact_length[1]');

		if($this->form_validation->run() == FALSE){
            $erros = array(
                'cep' => form_error('cep', null, null),
                'endereco' => form_error('endereco', null, null),
                'numero' => form_error('numero', null, null),
                'complemento' => form_error('complemento', null, null),
                'bairro' => form_error('bairro', null, null),
                'cidade' => form_error('cidade', null, null),
                'uf' => form_error('uf', null, null),
                'tipo' => form_error('tipo', null, null),
            );
            return $erros;
        // Verificação de adulteração do campo tipo
		} else if($this->input->post('tipo') > 2 || $this->input->post('tipo') <= 0 ) { 
            $erros = array(
                'cep' => form_error('cep', null, null),
                'endereco' => form_error('endereco', null, null),
                'numero' => form_error('numero', null, null),
                'complemento' => form_error('complemento', null, null),
                'bairro' => form_error('bairro', null, null),
                'cidade' => form_error('cidade', null, null),
                'uf' => form_error('uf', null, null),
                'tipo' => 'Você adulterou o tipo de endereço',
            );
            return $erros;
		} else {
            $cep = limpar($this->input->post('cep')); 
            $endereco = $this->input->post('endereco'); 
            $numero = $this->input->post('numero'); 
            $complemento = $this->input->post('complemento'); 
            $bairro = $this->input->post('bairro'); 
            $cidade = $this->input->post('cidade'); 
			$uf = $this->input->post('uf'); 
            $tipo = $this->input->post('tipo');
            if($this->session->userdata('userlogado')->tipo >= 2){
                $id_usuario = $this->input->post('id');
            } else {
                $id_usuario = $this->session->userdata('userlogado')->id;
            }
            
            $enderecoLib = new EnderecoLib();
			if($enderecoLib->verifica($id_usuario, $tipo)){
				if($enderecoLib->editar($cep, $endereco, $numero, $complemento, $bairro, $cidade, $uf, $id_usuario, $tipo)) {
					$sucesso = array(
						'cep' => form_error('cep', null, null),
						'endereco' => form_error('endereco', null, null),
						'numero' => form_error('numero', null, null),
						'complemento' => form_error('complemento', null, null),
						'bairro' => form_error('bairro', null, null),
						'cidade' => form_error('cidade', null, null),
						'uf' => form_error('uf', null, null),
                        'tipo' => form_error('tipo', null, null),
                        'id' => $id_usuario,
						'edit' => 1
					);
					return $sucesso;
						
				} else {
					$erros['500'] = 1;
					return $erros;
				}
			} else {
				if($enderecoLib->criar($cep, $endereco, $numero, $complemento, $bairro, $cidade, $uf, $id_usuario, $tipo)) {
					$sucesso = array(
						'cep' => form_error('cep', null, null),
						'endereco' => form_error('endereco', null, null),
						'numero' => form_error('numero', null, null),
						'complemento' => form_error('complemento', null, null),
						'bairro' => form_error('bairro', null, null),
						'cidade' => form_error('cidade', null, null),
						'uf' => form_error('uf', null, null),
						'tipo' => form_error('tipo', null, null),
						'edit' => 1
					);
					return $sucesso;
					
                } else {
					$erros['500'] = 1;
					return $erros;
				}
			}
		}
    }

    public function get_endereco_by_id($id_usuario){
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->get('tb_endereco')->result();
    }

}