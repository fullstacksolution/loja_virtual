<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/SubcategoriaLib.php';

class SubcategoriaModel extends CI_Model {

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[30]|is_unique[tb_departamento.nome]');
        $this->form_validation->set_rules('id_categoria', 'categoria', 'required|min_length[1]|max_length[2]|is_natural_no_zero');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_categoria' => form_error('id_categoria', null, null)
            );
            return $erros;
        } else {
            $nome = ucfirst($this->input->post('nome'));
            $id_categoria = $this->input->post('id_categoria');
            $id = $this->input->post('id');
            
            $subcategoriaLib = new SubcategoriaLib();
			if($subcategoriaLib->editar($nome, $id_categoria, $id)) {
				$dados = array(
                    'nome' => $nome,
                    'id_categoria' => $id_categoria,
                    'id' => $id
				);
				$sucesso = array(
					'nome' => form_error('nome', null, null),
					'id_categoria' => form_error('id_categoria', null, null),
					'edit' => 1,
					'dados' => $dados
				);
				return $sucesso;
					
            } else {
				$erros['500'] = 1;
				return $erros;
            }
		}
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;
        $this->load->model('ProdutoModel', 'produto');
        $id = $this->input->post('id');

        if(!$this->produto->get_produtos_by_subcategoria($id)){

            $subcategoriaLib = new SubcategoriaLib();

            if($subcategoriaLib->deletar($id)) { 
                $sucesso['dados'] = array(
                    'id'=>$id
                );           
                $sucesso['delete'] = 1;
                return $sucesso;
                    
            } else {
                $erros['500'] = 1;
                return $erros;
            }
        } else {
            $erros['erro'] = 'Você não pode deletar uma subcategoria enquanto existir produtos associados a ela.';
            return $erros;
        }
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('id_categoria', 'categoria', 'required|min_length[1]|max_length[2]|is_natural_no_zero');
        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[30]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_categoria' => form_error('id_categoria', null, null)
            );
            return $erros;
        } else {
            
            $nome = ucfirst($this->input->post('nome'));
            $id_categoria = $this->input->post('id_categoria');

            $nomes_subcategorias = $this->get_subcategorias_nome_by_categoria($id_categoria);
            foreach($nomes_subcategorias as $nome_subcategoria){
                 if($nome == $nome_subcategoria->nome){
                     $erros = array(
                         'nome' => 'Esse nome já está sendo usado por uma subcategoria da categoria selecionada.',
                         'id_categoria' => form_error('id_categoria', null, null)
                     );
                     return $erros;
                 }
            }
            
            $subcategoriaLib = new SubcategoriaLib();
            if($subcategoriaLib->criar($nome, $id_categoria)) {
                
                $sucesso['nome'] = form_error('nome', null, null);                
                $sucesso['redirect'] = base_url('admin/subcategoria/criar');
                
                return $sucesso;
            } else {
                $erros['500'] = 1;
                return $erros;
            }  
        } 
    }

    public function get_subcategorias(){
        $this->db->select('sub.id, sub.nome, count(prod.id) as qtd_produtos, cat.nome as categoria, cat.id as id_categoria, dep.nome as departamento');
        $this->db->from('tb_subcategoria sub');
        $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->group_by('sub.id');
        return $this->db->get()->result();
    }

    public function get_subcategoria_by_id($id){
        $this->db->select('sub.id, sub.nome, cat.id as id_categoria, cat.nome as categoria, dep.nome as departamento');
        $this->db->from('tb_subcategoria sub');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('sub.id', $id);
        return $this->db->get('tb_categoria')->row_object();
    }

    public function get_subcategorias_by_categoria($id){
        $this->db->where('id_categoria', $id);
        return $this->db->count_all_results('tb_subcategoria');
    }

    public function get_subcategorias_nome_by_categoria($id_categoria){
        $this->db->select('sub.nome');
        $this->db->from('tb_subcategoria sub');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->where('cat.id', $id_categoria);
        return $this->db->get()->result();

    }

    public function get_subcategorias_by_categorias(array $ids_categorias){
        $this->db->select('sub.id, sub.nome, sub.id_categoria, count(prod.id) as qtd_produtos');
        $this->db->from('tb_subcategoria sub');
        $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
        $this->db->where('prod.status', 1);
        $this->db->where_in('sub.id_categoria', $ids_categorias);
        $this->db->group_by('sub.id');
        return $this->db->get()->result();
    }

    public function get_subcategoria_by_name_categoria($subcategoria, $id_categoria){

        $subcategorias_bd = $this->get_subcategorias();

        foreach($subcategorias_bd as $subcategoria_bd){

            if(url_amigavel($subcategoria_bd->nome) === $subcategoria && $subcategoria_bd->id_categoria == $id_categoria){

                $this->db->select('sub.id, sub.nome, count(prod.id) as qtd_produtos');
                $this->db->from('tb_subcategoria sub');
                $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'inner');
                $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
                $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
                $this->db->where('prod.status', 1);
                $this->db->where('sub.id', $subcategoria_bd->id);
                return $this->db->get()->row_object();
            }

        }
    }

}