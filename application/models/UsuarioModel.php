<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/UsuarioLib.php';
include_once APPPATH.'libraries/EnderecoLib.php';

class UsuarioModel extends CI_Model {

    public function criar(){
        if( sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('sobrenome', 'sobrenome', 'min_length[3]|max_length[40]');
        $this->form_validation->set_rules('email', 'email', 'required|min_length[5]|max_length[40]|valid_email|is_unique[tb_usuario.email]');
        $this->form_validation->set_rules('confirmaremail', 'de confirmação de email', 'required|min_length[5]|max_length[40]|valid_email|matches[email]');
        $this->form_validation->set_rules('telefone', 'telefone', 'min_length[14]|max_length[15]');
        $this->form_validation->set_rules('celular', 'celular', 'exact_length[15]');
        $this->form_validation->set_rules('senha', 'senha', 'required|min_length[6]|max_length[30]');
        $this->form_validation->set_rules('confirmarsenha', 'de confirmação de senha', 'required|min_length[6]|max_length[30]|matches[senha]');
        $this->form_validation->set_rules('cpf', 'CPF', 'required|min_length[14]|max_length[19]');
        $this->form_validation->set_rules('cep', 'CEP', 'min_length[9]|max_length[9]');
        $this->form_validation->set_rules('endereco', 'endereço', 'min_length[6]|max_length[40]');
        $this->form_validation->set_rules('numero', 'número', 'is_natural_no_zero|min_length[1]|max_length[6]');
        $this->form_validation->set_rules('complemento', 'complemento', 'min_length[1]|max_length[15]');
        $this->form_validation->set_rules('bairro', 'bairro', 'min_length[3]|max_length[40]');
        $this->form_validation->set_rules('cidade', 'cidade', 'min_length[3]|max_length[40]');
        $this->form_validation->set_rules('uf', 'UF', 'exact_length[2]');
        $this->form_validation->set_rules('termos', 'termos', 'required');
        $cpf = limpar($this->input->post('cpf'));
        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'sobrenome' => form_error('sobrenome', null, null),
                'email' => form_error('email', null, null),
                'confirmaremail' => form_error('confirmaremail', null, null),
                'telefone' => form_error('telefone', null, null),
                'celular' => form_error('celular', null, null),
                'senha' => form_error('senha', null, null),
                'confirmarsenha' => form_error('confirmarsenha', null, null),
                'cpf' => form_error('cpf', null, null),
                'cep' => form_error('cep', null, null),
                'endereco' => form_error('endereco', null, null),
                'numero' => form_error('numero', null, null),
                'complemento' => form_error('complemento', null, null),
                'bairro' => form_error('bairro', null, null),
                'cidade' => form_error('cidade', null, null),
                'uf' => form_error('uf', null, null),
                'termos' => form_error('termos', null, null)
            );
            return $erros;
        } else if(validar_cnpj($cpf) || validar_cpf($cpf)){
            // Carregando modelo de endereços
            
            $enderecoLib = new EnderecoLib();
            $usuarioLib = new UsuarioLib();
            if($this->session->userdata('logado')) {
                if($this->session->userdata('userlogado')->tipo >= 2){
                    $tipo = $this->input->post('tipo_usuario');
                }
            } else {
                $tipo = 1;
            }

            $nome = ucfirst($this->input->post('nome'));
            $sobrenome = ucfirst($this->input->post('sobrenome')); 
            $email = strToLower($this->input->post('email')); 
            $telefone = limpar($this->input->post('telefone')); 
            $celular = limpar($this->input->post('celular')); 
            $senha = $this->input->post('senha');
            $cpf = limpar($this->input->post('cpf')); 
            $cep = limpar($this->input->post('cep'));
            if(isset($cep) && is_numeric($cep)){
                $endereco = $this->input->post('endereco'); 
                $numero = $this->input->post('numero'); 
                $complemento = $this->input->post('complemento'); 
                $bairro = $this->input->post('bairro'); 
                $cidade = $this->input->post('cidade'); 
                $uf = $this->input->post('uf'); 
            } else {
                $endereco = null;
                $numero = null;
                $complemento = null;
                $bairro = null;
                $cidade = null;
                $uf = null;
            }
            $tipo_endereco = 1;

            if($usuarioLib->criar($nome, $sobrenome,$email, $telefone, $celular, $senha, $cpf, $tipo)) {
                // Capturando id do usuário recem criado
                $id_usuario = $usuarioLib->get_usuario_id($email);
                if(isset($endereco)){
                    if($enderecoLib->criar($cep, $endereco, $numero, $complemento, $bairro, $cidade, $uf, $id_usuario->id, $tipo_endereco)) {
                        $sucesso = array(
                            'nome' => form_error('nome', null, null),
                            'sobrenome' => form_error('sobrenome', null, null),
                            'email' => form_error('email', null, null),
                            'confirmaremail' => form_error('confirmaremail', null, null),
                            'telefone' => form_error('telefone', null, null),
                            'celular' => form_error('celular', null, null),
                            'senha' => form_error('senha', null, null),
                            'confirmarsenha' => form_error('confirmarsenha', null, null),
                            'cpf' => form_error('cpf', null, null),
                            'cep' => form_error('cep', null, null),
                            'endereco' => form_error('endereco', null, null),
                            'numero' => form_error('numero', null, null),
                            'complemento' => form_error('complemento', null, null),
                            'bairro' => form_error('bairro', null, null),
                            'cidade' => form_error('cidade', null, null),
                            'uf' => form_error('uf', null, null),
                            'termos' => form_error('termos', null, null)
                        );
                        
                        // Usuários administradores logados que criam outras contas não passarão pelo redirect
                        if(!$this->session->userdata('logado')){    
                            $sucesso['redirect'] = base_url('sign/in?cadastro=1');
                        } else if($this->session->userdata('userlogado')->tipo >= 2){
                            $sucesso['redirect'] = base_url('admin/');
                            $sucesso['tipo'] = $tipo;
                        }
                        return $sucesso;
                    }
                } else {
                    if(!$this->session->userdata('logado')){    
                        $sucesso['redirect'] = base_url('sign/in?cadastro=1');
                    } else if($this->session->userdata('userlogado')->tipo >= 2){
                        $sucesso['redirect'] = base_url('admin/');
                        $sucesso['tipo'] = $tipo;
                    }
                    return $sucesso;
                }
            } else {
                $erros['500'] = 1;
                return $erros;
            }    
        } else {
            $erros = array(
                'nome' => form_error('nome', null, null),
                'sobrenome' => form_error('sobrenome', null, null),
                'email' => form_error('email', null, null),
                'confirmaremail' => form_error('confirmaremail', null, null),
                'telefone' => form_error('telefone', null, null),
                'celular' => form_error('celular', null, null),
                'senha' => form_error('senha', null, null),
                'confirmarsenha' => form_error('confirmarsenha', null, null),
                'cpf' => 'O CPF/CNPJ digitado é inválido',
                'cep' => form_error('cep', null, null),
                'endereco' => form_error('endereco', null, null),
                'numero' => form_error('numero', null, null),
                'complemento' => form_error('complemento', null, null),
                'bairro' => form_error('bairro', null, null),
                'cidade' => form_error('cidade', null, null),
                'uf' => form_error('uf', null, null),
                'termos' => form_error('termos', null, null)
            );
            return $erros;
        }
    }

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('sobrenome', 'sobrenome', 'min_length[3]|max_length[40]');
        $this->form_validation->set_rules('telefone', 'telefone', 'min_length[14]|max_length[15]');
        $this->form_validation->set_rules('celular', 'celular', 'min_length[15]|max_length[15]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'sobrenome' => form_error('sobrenome', null, null),
                'telefone' => form_error('telefone', null, null),
                'celular' => form_error('celular', null, null)
            );
            return $erros;
        } else {
            $nome = ucfirst($this->input->post('nome'));
            $sobrenome = ucfirst($this->input->post('sobrenome')); 
            $telefone = limpar($this->input->post('telefone')); 
            $celular = limpar($this->input->post('celular'));
            if($this->session->userdata('userlogado')->tipo >= 2){
                $id = $this->input->post('id');
            } else {
                $id = $this->session->userdata('userlogado')->id;
            }
            
            $usuarioLib = new UsuarioLib();
			if($usuarioLib->editar($nome, $sobrenome, $telefone, $celular, $id)) {
				$this->resetSession();
				$dados = array(
					'nome' => $nome,
					'sobrenome' => $sobrenome,
					'telefone' => $telefone,
					'celular' => $celular,
				);
				$sucesso = array(
					'nome' => form_error('nome', null, null),
					'sobrenome' => form_error('sobrenome', null, null),
					'telefone' => form_error('telefone', null, null),
					'celular' => form_error('celular', null, null),
					'edit' => 1,
					'dados' => $dados
				);
				return $sucesso;
					
            } else {
				$erros['500'] = 1;
				return $erros;
            }
		}
    }

    public function editar_cpf(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('cpf', 'CPF', 'required|min_length[14]|max_length[19]');
        $this->form_validation->set_rules('cpfnovo', ' do novo CPF', 'required|min_length[14]|max_length[19]');
        
        $cpf = limpar($this->input->post('cpf'));
        $cpfNovo = limpar($this->input->post('cpfnovo'));

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'cpf' => form_error('cpf', null, null),
                'cpfnovo' => form_error('cpfnovo', null, null)
            );
            return $erros;

        } else if(validar_cnpj($cpfNovo) || validar_cpf($cpfNovo)){
            if($this->session->userdata('userlogado')->tipo >= 2){
                $id = $this->input->post('id');
            }
            
            $usuarioLib = new UsuarioLib();
            $cpfAntigo = $usuarioLib->get_usuario_cpf($id);
            if($cpfAntigo->cpf == $cpf) {
                if($usuarioLib->editar_cpf($cpfNovo, $id)) {
                    $dados = array(
                        'cpf' => $cpfNovo
                    );
                    $sucesso = array(
                        'cpf' => form_error('cpf', null, null),
                        'cpfnovo' => form_error('cpfnovo', null, null),
                        'edit' => 1,
                        'dados' => $dados
                    );
                    return $sucesso;
                        
                } else {
                    $erros['500'] = 1;
                    return $erros;
                }
            } else {
                $erros = array(
                    'cpf' => 'O CPF atual está incorreto.',
                    'cpfnovo' => form_error('cpfnovo', null, null)
                );
                return $erros;
            }
		} else {
            $erros = array(
                'cpf' => form_error('cpf', null, null),
                'cpfnovo' => 'O CPF/CNPJ está inválido ou não existe.'
            );
            return $erros;
        }
    }

    public function editar_email(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('email', 'email', 'required|min_length[5]|max_length[40]|valid_email');
        $this->form_validation->set_rules('emailnovo', 'do novo email', 'required|min_length[5]|max_length[40]|valid_email|is_unique[tb_usuario.email]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'email' => form_error('email', null, null),
                'emailnovo' => form_error('emailnovo', null, null)
            );
            return $erros;
        } else {
            $email = $this->input->post('email');
            $emailNovo = strtolower($this->input->post('emailnovo'));
            if($this->session->userdata('userlogado')->tipo >= 2){
                $id = $this->input->post('id');
            }
            
            $usuarioLib = new UsuarioLib();
            $emailAntigo = $usuarioLib->get_usuario_email($id);
            if($emailAntigo->email == $email) {
                if($usuarioLib->editar_email($emailNovo, $id)) {
                    $dados = array(
                        'email' => $emailNovo
                    );
                    $sucesso = array(
                        'email' => form_error('email', null, null),
                        'emailnovo' => form_error('emailnovo', null, null),
                        'edit' => 1,
                        'dados' => $dados
                    );
                    return $sucesso;
                        
                } else {
                    $erros['500'] = 1;
                    return $erros;
                }
            } else {
                $erros = array(
                    'email' => 'O email atual está incorreto.',
                    'emailnovo' => form_error('emailnovo', null, null)
                );
                return $erros;
            }
		}
    }

    public function editar_senha(){
        if( sizeof($_POST) == 0) return false;
        if($this->session->userdata('userlogado')->tipo < 2){
            $this->form_validation->set_rules('senha', 'senha', 'required|min_length[6]|max_length[30]');
        }
		$this->form_validation->set_rules('senhanova', 'nova senha', 'required|min_length[6]|max_length[30]');
        $this->form_validation->set_rules('confirmarsenha', 'de confirmação de senha', 'required|min_length[6]|max_length[30]|matches[senhanova]');
        
		if($this->form_validation->run() == FALSE){
            $erros = array(
                'senha' => form_error('senha', null, null),
                'senhanova' => form_error('senhanova', null, null),
                'confirmarsenha' => form_error('confirmarsenha', null, null)
            );
            return $erros;
        } else {
            $senha = '';
            if($this->session->userdata('userlogado')->tipo < 2){
                $senha = sha1($this->input->post('senha'));
            }
            $senhanova = $this->input->post('senhanova');
            if($this->session->userdata('userlogado')->tipo >= 2){
                $id = $this->input->post('id');
            } else {
                $id = $this->session->userdata('userlogado')->id;
            }
            
            // Verificar se a senha atual é a mesma
            $usuarioLib = new UsuarioLib();
			if($senha == $usuarioLib->get_usuario_senha($id)->senha || $this->session->userdata('userlogado')->tipo >= 2){
				// Editar a senha
				if($usuarioLib->editar_senha($senhanova, $id)) {
					$this->resetSession();
					
					$sucesso = array(
						'senha' => form_error('senha', null, null),
						'senhanova' => form_error('senhanova', null, null),
						'confirmarsenha' => form_error('confirmarsenha', null, null),
						'edit' => 1
					);
					return $sucesso;
						
				} else {
					$erros['500'] = 1;
					return $erros;
				}
			} else {
				$erros = array(
					'senha' => 'A senha atual está incorreta.',
					'senhanova' => form_error('senhanova', null, null),
					'confirmarsenha' => form_error('confirmarsenha', null, null)
				);
				return $erros;
			}
		}
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;
        $tipo = $this->get_usuario_tipo($this->input->post('id'));
        if($tipo->tipo >= 3) return false;

        if($this->session->userdata('userlogado')->tipo >= 2){
            $id = $this->input->post('id');
        } else {
            return false;
        }
        
        $usuarioLib = new UsuarioLib();
        $enderecoLib = new EnderecoLib();
        // Deletar
        if($enderecoLib->deletar($id) && $usuarioLib->deletar($id)) { 
            $sucesso['dados'] = array(
                'id'=>$id
            );           
            $sucesso['delete'] = 1;
            return $sucesso;
                
        } else {
            $erros['500'] = 1;
            return $erros;
        }
    }

    public function resetSession(){
        if($this->session->userdata('logado')){
            $email = $this->session->userdata('userlogado')->email;
            
            $dadosSessao['userlogado'] = NULL;
            $dadosSessao['logado'] = TRUE;
            $this->session->set_userdata($dadosSessao);

            $usuarioLib = new UsuarioLib();
            $userlogado = $usuarioLib->get_usuario_by_email($email);

			$userlogado->senha = '';
            $dadosSessao['userlogado'] = $userlogado;
            $dadosSessao['logado'] = TRUE;
            $this->session->set_userdata($dadosSessao);
        }
    }

    public function autenticar(){
        if( sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('email', 'email', 'required|min_length[5]|max_length[40]|valid_email');
        $this->form_validation->set_rules('senha', 'senha', 'required|min_length[3]|max_length[30]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'email' => form_error('email', null, null),
                'senha' => form_error('senha', null, null)
            );
            return $erros;
        } else {
            $email = $this->input->post('email');
            $senha = $this->input->post('senha');

            // Verifica a existência do email no BD
            $usuarioLib = new UsuarioLib();
            $userlogado = $usuarioLib->get_usuario_by_email($email);

            // Se existir um usuário com o email
            if(isset($userlogado)){
                // Verificar se a senha coincidem
                if($userlogado->senha == sha1($senha)){
                    $userlogado->senha = '';
                    $usuarioLib->autenticar($userlogado->id, 1);
                    $dadosSessao['userlogado'] = $userlogado;
                    $dadosSessao['logado'] = TRUE;
                    $this->session->set_userdata($dadosSessao);
                    $sucesso['auth'] = 'Autenticado.';
                    // Redirect comum
                    if($userlogado->tipo > 1){
                        $sucesso['redirect'] = base_url('admin');
                    } else {
                        $sucesso['redirect'] = base_url('loja');
                    }
                    // Redirect personalizado
                    if( isset($_POST['redirect']) ) {

                        if(isset($_POST['id_produto'])) $id_produto = $this->input->post('id_produto');

                        if($this->input->post('redirect') == 'add_avaliacao'){
                            $sucesso['redirect'] = base_url('produto/'.$id_produto.'#avaliacoes');
                        } else if($this->input->post('redirect') == 'add_carrinho'){
                            $sucesso['redirect'] = base_url('produto/'.$id_produto);
                        } else if($this->input->post('redirect') == 'page_carrinho'){
                            $sucesso['redirect'] = base_url('carrinho');
                        }
                    }
                    return $sucesso;
                // Se não, a senha está incorreta
                } else {
                    $dadosSessao['userlogado'] = NULL;
                    $dadosSessao['logado'] = FALSE;
                    $this->session->set_userdata($dadosSessao);
                    $erros['senha'] = 'Senha incorreta.';
                    return $erros;
                }
            // Se não, o email não está cadastrado
            } else {
                $dadosSessao['userlogado'] = NULL;
                $dadosSessao['logado'] = FALSE;
                $this->session->set_userdata($dadosSessao);
                $erros['email'] = 'Email não cadastrado no sistema.';
                $erros['senha'] = 'Email não cadastrado no sistema.';
                return $erros;
            }
        }
    }

    public function desautenticar($id){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->autenticar($id);
    }

    //GETS 

    public function get_clientes(){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->get_clientes();
    }

    public function get_administradores(){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->get_administradores();
    }

    public function get_usuario_by_id($id){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->get_usuario_by_id($id);
    }
    
    public function get_usuario_tipo($id){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->get_usuario_tipo($id);
    }

    public function get_usuarios_by_status($status){
        $usuarioLib = new UsuarioLib();
        return $usuarioLib->get_usuarios_by_status($status);
    }
}

