<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/ProdutoLib.php';
include_once APPPATH.'libraries/CarrinhoLib.php';

class ProdutoModel extends CI_Model {

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('id_subcategoria', 'subcategoria', 'required|min_length[1]|max_length[2]|is_natural_no_zero');
        $this->form_validation->set_rules('descricao', 'descricao', 'required|min_length[3]|max_length[1000]');
        $this->form_validation->set_rules('fabricante', 'fabricante', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('marca', 'marca', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('modelo', 'modelo', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('cor', 'cor', 'min_length[3]|max_length[30]');
        $this->form_validation->set_rules('preco_custo', 'preço de custo', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('preco_venda', 'preço de venda', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('preco_promocional', 'preço promocional', 'min_length[4]|max_length[9]');
        $this->form_validation->set_rules('garantia', 'garantia', 'max_length[2]');
        $this->form_validation->set_rules('ipi', 'IPI', 'max_length[3]');
        $this->form_validation->set_rules('sku', 'SKU', 'max_length[30]');
        $this->form_validation->set_rules('ean_gtin', 'EAN/GTIN', 'min_length[9]|max_length[13]');
        $this->form_validation->set_rules('mpn_partnumber', 'MPN/Part Number', 'max_length[20]');
        $this->form_validation->set_rules('ncm', 'NCM', 'max_length[10]');
        $this->form_validation->set_rules('serial', 'serial', 'max_length[30]');
        $this->form_validation->set_rules('caixa', 'caixa', 'max_length[5]');
        $this->form_validation->set_rules('peso_kg', 'peso', 'min_length[5]|max_length[6]');
        $this->form_validation->set_rules('altura_cm', 'altura', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('largura_cm', 'largura', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('profundidade_cm', 'profundidade', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('endereco_estoque', 'endereço do estoque', 'min_length[2]|max_length[10]');
        $this->form_validation->set_rules('quantidade', 'quantidade', 'required|min_length[1]|max_length[3]|is_natural');
        $this->form_validation->set_rules('unidade', 'unidade', 'required|min_length[1]|max_length[5]');
        $this->form_validation->set_rules('disponibilidade', 'disponibilidade', 'required|min_length[8]|max_length[10]');
        $this->form_validation->set_rules('condicao', 'condicao', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('codigo', 'codigo', 'required|min_length[3]|max_length[4]');
        $this->form_validation->set_rules('status', 'status', 'required|exact_length[1]');
        $this->form_validation->set_rules('promocao[]', 'promocao', '');
        $this->form_validation->set_rules('destaque[]', 'destaque', '');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_subcategoria' => form_error('id_subcategoria', null, null),
                'descricao' => form_error('descricao', null, null),
                'fabricante' => form_error('fabricante', null, null),
                'marca' => form_error('marca', null, null),
                'modelo' => form_error('modelo', null, null),
                'cor' => form_error('cor', null, null),
                'preco_custo' => form_error('preco_custo', null, null),
                'preco_venda' => form_error('preco_venda', null, null),
                'preco_promocional' => form_error('preco_promocional', null, null),
                'garantia' => form_error('garantia', null, null),
                'ipi' => form_error('ipi', null, null),
                'sku' => form_error('sku', null, null),
                'ean_gtin' => form_error('ean_gtin', null, null),
                'mpn_partnumber' => form_error('mpn_partnumber', null, null),
                'ncm' => form_error('ncm', null, null),
                'serial' => form_error('serial', null, null),
                'caixa' => form_error('caixa', null, null),
                'peso_kg' => form_error('peso_kg', null, null),
                'altura_cm' => form_error('altura_cm', null, null),
                'largura_cm' => form_error('largura_cm', null, null),
                'profundidade_cm' => form_error('profundidade_cm', null, null),
                'endereco_estoque' => form_error('endereco_estoque', null, null),
                'quantidade' => form_error('quantidade', null, null),
                'unidade' => form_error('unidade', null, null),
                'disponibilidade' => form_error('disponibilidade', null, null),
                'condicao' => form_error('condicao', null, null),
                'codigo' => form_error('codigo', null, null),
                'status' => form_error('status', null, null),
                'promocao' => form_error('promocao', null, null),
                'destaque' => form_error('destaque', null, null)
            );
            return $erros;
        } else if($this->input->post('promocao') != '' && $this->input->post('preco_promocional') == ''){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_subcategoria' => form_error('id_subcategoria', null, null),
                'descricao' => form_error('descricao', null, null),
                'fabricante' => form_error('fabricante', null, null),
                'marca' => form_error('marca', null, null),
                'modelo' => form_error('modelo', null, null),
                'cor' => form_error('cor', null, null),
                'preco_custo' => form_error('preco_custo', null, null),
                'preco_venda' => form_error('preco_venda', null, null),
                'garantia' => form_error('garantia', null, null),
                'ipi' => form_error('ipi', null, null),
                'sku' => form_error('sku', null, null),
                'ean_gtin' => form_error('ean_gtin', null, null),
                'mpn_partnumber' => form_error('mpn_partnumber', null, null),
                'ncm' => form_error('ncm', null, null),
                'serial' => form_error('serial', null, null),
                'caixa' => form_error('caixa', null, null),
                'peso_kg' => form_error('peso_kg', null, null),
                'altura_cm' => form_error('altura_cm', null, null),
                'largura_cm' => form_error('largura_cm', null, null),
                'profundidade_cm' => form_error('profundidade_cm', null, null),
                'endereco_estoque' => form_error('endereco_estoque', null, null),
                'quantidade' => form_error('quantidade', null, null),
                'unidade' => form_error('unidade', null, null),
                'disponibilidade' => form_error('disponibilidade', null, null),
                'condicao' => form_error('condicao', null, null),
                'codigo' => form_error('codigo', null, null),
                'status' => form_error('status', null, null),
                'destaque' => form_error('destaque', null, null)
            );
            $erros['promocao'] = 'Este campo só é válido se o campo de Preço Promocional não for nulo.';
            $erros['preco_promocional'] = 'O campo de promoção só é permitido se este campo for preenchido.';
            return $erros;
        } else if($this->input->post('destaque') != '' && $this->input->post('status') == 0){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_subcategoria' => form_error('id_subcategoria', null, null),
                'descricao' => form_error('descricao', null, null),
                'fabricante' => form_error('fabricante', null, null),
                'marca' => form_error('marca', null, null),
                'modelo' => form_error('modelo', null, null),
                'cor' => form_error('cor', null, null),
                'preco_custo' => form_error('preco_custo', null, null),
                'preco_venda' => form_error('preco_venda', null, null),
                'garantia' => form_error('garantia', null, null),
                'ipi' => form_error('ipi', null, null),
                'sku' => form_error('sku', null, null),
                'ean_gtin' => form_error('ean_gtin', null, null),
                'mpn_partnumber' => form_error('mpn_partnumber', null, null),
                'ncm' => form_error('ncm', null, null),
                'serial' => form_error('serial', null, null),
                'caixa' => form_error('caixa', null, null),
                'peso_kg' => form_error('peso_kg', null, null),
                'altura_cm' => form_error('altura_cm', null, null),
                'largura_cm' => form_error('largura_cm', null, null),
                'profundidade_cm' => form_error('profundidade_cm', null, null),
                'endereco_estoque' => form_error('endereco_estoque', null, null),
                'quantidade' => form_error('quantidade', null, null),
                'unidade' => form_error('unidade', null, null),
                'disponibilidade' => form_error('disponibilidade', null, null),
                'condicao' => form_error('condicao', null, null),
                'codigo' => form_error('codigo', null, null),
                'promocao' => form_error('promocao', null, null),
                'preco_promocional' => form_error('preco_promocional', null, null),
                'status' => form_error('status', null, null)
            );
            $erros['destaque'] = 'Este campo só é válido se o campo status estiver como "Publicado".';
            $erros['status'] = 'O campo destaque só é válido se o status for "Publicado".';
            return $erros;
        } else {
            $nome = ucfirst($this->input->post('nome'));
            $id_subcategoria = $this->input->post('id_subcategoria');
            $descricao = ucfirst($this->input->post('descricao'));
            $fabricante = ucfirst($this->input->post('fabricante'));
            $marca = ucfirst($this->input->post('marca'));
            $modelo = ucfirst($this->input->post('modelo'));
            $cor = ucfirst($this->input->post('cor'));
            $preco_custo = preco($this->input->post('preco_custo'));
            $preco_venda = preco($this->input->post('preco_venda'));
            if($this->input->post('preco_promocional')){
                $preco_promocional = preco($this->input->post('preco_promocional'));
            } else {
                $preco_promocional = null;
            }
            $garantia = $this->input->post('garantia');
            $ipi = $this->input->post('ipi');
            $sku = $this->input->post('sku');
            $ean_gtin = $this->input->post('ean_gtin');
            $mpn_partnumber = $this->input->post('mpn_partnumber');
            $ncm = $this->input->post('ncm');
            $serial = $this->input->post('serial');
            $caixa = $this->input->post('caixa');
            $peso_kg = $this->input->post('peso_kg');            
            $altura_cm = $this->input->post('altura_cm');            
            $largura_cm = $this->input->post('largura_cm');            
            $profundidade_cm = $this->input->post('profundidade_cm');            
            $endereco_estoque = $this->input->post('endereco_estoque');
            $quantidade = $this->input->post('quantidade');
            $unidade = ucfirst($this->input->post('unidade'));
            $disponibilidade = $this->input->post('disponibilidade');
            $condicao = $this->input->post('condicao');
            $codigo = $this->input->post('codigo');
            $status = $this->input->post('status');
            if($this->input->post('promocao')){
                $promocao = 1;
            } else {
                $promocao = 0;
            }
            if($this->input->post('destaque')){
                $destaque = 1;
            } else {
                $destaque = 0;
            }
            $id = $this->input->post('id');
            
            $produtoLib = new ProdutoLib();
			if($produtoLib->editar($nome, $id, $id_subcategoria, $descricao, $fabricante, $marca, $modelo, $cor, $preco_custo, $preco_venda, $preco_promocional, $garantia, $ipi, $sku, $ean_gtin, $mpn_partnumber, $ncm, $serial, $caixa, $peso_kg, $altura_cm, $largura_cm, $profundidade_cm, $endereco_estoque, $quantidade, $unidade, $disponibilidade, $condicao, $codigo, $status, $promocao, $destaque)) {
				$dados = array(
                    'nome' => $nome,
                    'id_subcategoria' => $id_subcategoria,
                    'descricao' => $descricao,
                    'fabricante' => $fabricante,
                    'marca' => $marca,
                    'modelo' => $modelo,
                    'cor' => $cor,
                    'preco_custo' => $preco_custo,
                    'preco_venda' => $preco_venda,
                    'preco_promocional' => $preco_promocional,
                    'garantia' => $garantia,
                    'ipi' => $ipi,
                    'sku' => $sku,
                    'ean_gtin' => $ean_gtin,
                    'mpn_partnumber' => $mpn_partnumber,
                    'ncm' => $ncm,
                    'serial' => $serial,
                    'caixa' => $caixa,
                    'peso_kg' => $peso_kg,
                    'altura_cm' => $altura_cm,
                    'largura_cm' => $largura_cm,
                    'profundidade_cm' => $profundidade_cm,
                    'endereco_estoque' => $endereco_estoque,
                    'quantidade' => $quantidade,
                    'unidade' => $unidade,
                    'disponibilidade' => $disponibilidade,
                    'condicao' => $condicao,
                    'codigo' => $codigo,
                    'status' => $status,
                    'promocao' => $promocao,	
                    'destaque' => $destaque,
                );
				$sucesso = array(
                    'nome' => form_error('nome', null, null),
                    'id_subcategoria' => form_error('id_subcategoria', null, null),
                    'descricao' => form_error('descricao', null, null),
                    'fabricante' => form_error('fabricante', null, null),
                    'marca' => form_error('marca', null, null),
                    'modelo' => form_error('modelo', null, null),
                    'cor' => form_error('cor', null, null),
                    'preco_custo' => form_error('preco_custo', null, null),
                    'preco_venda' => form_error('preco_venda', null, null),
                    'preco_promocional' => form_error('preco_promocional', null, null),
                    'garantia' => form_error('garantia', null, null),
                    'ipi' => form_error('ipi', null, null),
                    'sku' => form_error('sku', null, null),
                    'ean_gtin' => form_error('ean_gtin', null, null),
                    'mpn_partnumber' => form_error('mpn_partnumber', null, null),
                    'ncm' => form_error('ncm', null, null),
                    'serial' => form_error('serial', null, null),
                    'caixa' => form_error('caixa', null, null),
                    'peso_kg' => form_error('peso_kg', null, null),
                    'altura_cm' => form_error('altura_cm', null, null),
                    'largura_cm' => form_error('largura_cm', null, null),
                    'profundidade_cm' => form_error('profundidade_cm', null, null),
                    'endereco_estoque' => form_error('endereco_estoque', null, null),
                    'quantidade' => form_error('quantidade', null, null),
                    'unidade' => form_error('unidade', null, null),
                    'disponibilidade' => form_error('disponibilidade', null, null),
                    'condicao' => form_error('condicao', null, null),
                    'codigo' => form_error('codigo', null, null),
                    'status' => form_error('status', null, null),
                    'promocao' => form_error('promocao', null, null),
                    'destaque' => form_error('destaque', null, null),
					'edit' => 1,
					'dados' => $dados
				);
				return $sucesso;
					
            } else {
				$erros['500'] = 1;
				return $erros;
            }
		}
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;
        $id = $this->input->post('id');

        $produtoLib = new ProdutoLib();
        $carrinhoLib = new CarrinhoLib();

        if($this->deletar_imagens($id)){
            if($produtoLib->deletar_avaliacoes($id)){
                if($carrinhoLib->deletar_produtos_carrinho($id)){
                    if($produtoLib->deletar($id)) { 
                        $sucesso['dados'] = array(
                            'id'=>$id
                        );           
                        $sucesso['delete'] = 1;
                        return $sucesso;
                            
                    } else {
                        $erros['erro'] = 'Não foi possível deletar o produto.';
                        return $erros;
                    }
                } else {
                    $erros['erro'] = 'Não foi possível deletar o produto dos carrinhos dos clientes.';
                    return $erros;
                }
            } else {
                $erros['erro'] = 'Não foi possível deletar as avaliações do produto.';
                return $erros;
            }
        } else {
            $erros['erro'] = 'Não foi possível deletar as imagens do produto.';
            return $erros;
        }
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('id_subcategoria', 'subcategoria', 'required|min_length[1]|max_length[2]|is_natural_no_zero');
        $this->form_validation->set_rules('descricao', 'descricao', 'required|min_length[3]|max_length[1000]');
        $this->form_validation->set_rules('fabricante', 'fabricante', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('marca', 'marca', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('modelo', 'modelo', 'min_length[2]|max_length[30]');
        $this->form_validation->set_rules('cor', 'cor', 'min_length[3]|max_length[30]');
        $this->form_validation->set_rules('preco_custo', 'preço de custo', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('preco_venda', 'preço de venda', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('preco_promocional', 'preço promocional', 'min_length[4]|max_length[9]');
        $this->form_validation->set_rules('garantia', 'garantia', 'max_length[2]');
        $this->form_validation->set_rules('ipi', 'IPI', 'max_length[3]|is_natural');
        $this->form_validation->set_rules('sku', 'SKU', 'max_length[30]');
        $this->form_validation->set_rules('ean_gtin', 'EAN/GTIN', 'min_length[9]|max_length[13]');
        $this->form_validation->set_rules('mpn_partnumber', 'MPN/Part Number', 'max_length[20]');
        $this->form_validation->set_rules('ncm', 'NCM', 'max_length[10]');
        $this->form_validation->set_rules('serial', 'serial', 'max_length[30]');
        $this->form_validation->set_rules('caixa', 'caixa', 'max_length[5]');
        $this->form_validation->set_rules('peso_kg', 'peso', 'min_length[5]|max_length[6]');
        $this->form_validation->set_rules('altura_cm', 'altura', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('largura_cm', 'largura', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('profundidade_cm', 'profundidade', 'min_length[4]|max_length[6]');
        $this->form_validation->set_rules('endereco_estoque', 'endereço do estoque', 'min_length[2]|max_length[10]');
        $this->form_validation->set_rules('quantidade', 'quantidade', 'required|min_length[1]|max_length[3]|is_natural');
        $this->form_validation->set_rules('unidade', 'unidade', 'required|min_length[1]|max_length[5]');
        $this->form_validation->set_rules('disponibilidade', 'disponibilidade', 'required|min_length[8]|max_length[10]');
        $this->form_validation->set_rules('condicao', 'condicao', 'required|min_length[4]|max_length[9]');
        $this->form_validation->set_rules('codigo', 'codigo', 'required|min_length[3]|max_length[4]');
        $this->form_validation->set_rules('status', 'status', 'required|exact_length[1]');
        $this->form_validation->set_rules('promocao', 'promocao', '');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null),
                'id_subcategoria' => form_error('id_subcategoria', null, null),
                'descricao' => form_error('descricao', null, null),
                'fabricante' => form_error('fabricante', null, null),
                'marca' => form_error('marca', null, null),
                'modelo' => form_error('modelo', null, null),
                'cor' => form_error('cor', null, null),
                'preco_custo' => form_error('preco_custo', null, null),
                'preco_venda' => form_error('preco_venda', null, null),
                'preco_promocional' => form_error('preco_promocional', null, null),
                'garantia' => form_error('garantia', null, null),
                'ipi' => form_error('ipi', null, null),
                'sku' => form_error('sku', null, null),
                'ean_gtin' => form_error('ean_gtin', null, null),
                'mpn_partnumber' => form_error('mpn_partnumber', null, null),
                'ncm' => form_error('ncm', null, null),
                'serial' => form_error('serial', null, null),
                'caixa' => form_error('caixa', null, null),
                'peso_kg' => form_error('peso_kg', null, null),
                'altura_cm' => form_error('altura_cm', null, null),
                'largura_cm' => form_error('largura_cm', null, null),
                'profundidade_cm' => form_error('profundidade_cm', null, null),
                'endereco_estoque' => form_error('endereco_estoque', null, null),
                'quantidade' => form_error('quantidade', null, null),
                'unidade' => form_error('unidade', null, null),
                'disponibilidade' => form_error('disponibilidade', null, null),
                'condicao' => form_error('condicao', null, null),
                'codigo' => form_error('codigo', null, null),
                'status' => form_error('status', null, null),
                'promocao' => form_error('promocao', null, null)
            );
            return $erros;
        } else if($this->input->post('promocao') && $this->input->post('preco_promocional') == null){
            $erros['promocao'] = 'Este campo só é válido se o campo de Preço Promocional não for nulo.';
            $erros['preco_promocional'] = 'Este campo deve ser preenchido.';
            return $erros;
        }else {
            $nome = ucfirst($this->input->post('nome'));
            $id_subcategoria = $this->input->post('id_subcategoria');
            $descricao = ucfirst($this->input->post('descricao'));
            $fabricante = ucfirst($this->input->post('fabricante'));
            $marca = ucfirst($this->input->post('marca'));
            $modelo = ucfirst($this->input->post('modelo'));
            $cor = ucfirst($this->input->post('cor'));
            $preco_custo = preco($this->input->post('preco_custo'));
            $preco_venda = preco($this->input->post('preco_venda'));
            $preco_promocional = preco($this->input->post('preco_promocional'));
            $garantia = $this->input->post('garantia');
            $ipi = $this->input->post('ipi');
            $sku = $this->input->post('sku');
            $ean_gtin = $this->input->post('ean_gtin');
            $mpn_partnumber = $this->input->post('mpn_partnumber');
            $ncm = $this->input->post('ncm');
            $serial = $this->input->post('serial');
            $caixa = $this->input->post('caixa');
            $peso_kg = $this->input->post('peso_kg');            
            $altura_cm = $this->input->post('altura_cm');            
            $largura_cm = $this->input->post('largura_cm');            
            $profundidade_cm = $this->input->post('profundidade_cm');            
            $endereco_estoque = $this->input->post('endereco_estoque');
            $quantidade = $this->input->post('quantidade');
            $unidade = $this->input->post('unidade');
            $disponibilidade = $this->input->post('disponibilidade');
            $condicao = $this->input->post('condicao');
            $codigo = $this->input->post('codigo');
            $status = $this->input->post('status');
            if($this->input->post('promocao')){
                $promocao = 1;
            } else {
                $promocao = null;
            }
            
            $produtoLib = new ProdutoLib();
            if($produtoLib->criar($nome, $id_subcategoria, $descricao, $fabricante, $marca, $modelo, $cor, $preco_custo, $preco_venda, $preco_promocional, $garantia, $ipi, $sku, $ean_gtin, $mpn_partnumber, $ncm, $serial, $caixa, $peso_kg, $altura_cm, $largura_cm, $profundidade_cm, $endereco_estoque, $quantidade, $unidade, $disponibilidade, $condicao, $codigo, $status, $promocao)) {
                
                $sucesso['nome'] = form_error('nome', null, null);                
                $sucesso['redirect'] = base_url('admin/produto/criar');
                
                return $sucesso;
            } else {
                $erros['500'] = 1;
                return $erros;
            }  
        } 
    }

    public function adicionar_imagem(){
        if(sizeof($_FILES) == 0){
            $erros['erro'] = 'Nenhuma imagem foi selecionada para fazer o upload.';
            return $erros;
        }

        $id_produto = $this->input->post('id_produto');

        $produtoLib = new ProdutoLib();
        $qtd_imagens = $this->get_qtd_imagens_by_produto($id_produto);
        $imagens = $this->get_imagens_by_produto($id_produto);

        // Verificar se o limite de imagens foi excedido
        if($qtd_imagens->qtd_imagens >= 8){
            $erros['erro'] = 'Limite de fotos excedidos para este produto.';
            return $erros;
        }

        // Recuperando valores da imagem de upload
        $file_name = $_FILES['file']['name']; // Nome do arquivo
        $file_type = $_FILES['file']['type']; // Tipo de imagem
        $file_size = $_FILES['file']['size']; // Tamanho da imagem
        $file_tmp_name = $_FILES['file']['tmp_name'];

        if($file_size > 2097152){ // 1048576 1mb // 2097152 2mb
            $erros['erro'] = 'A imagem deve ter no máximo o tamanho de 2mb.';
            return $erros;
        }
        
        // Verificar tipo da imagem
        if($file_type == 'image/jpeg' || $file_type == 'image/png'){

            // Verifica existência de diretório de imagens do produto
            if(is_dir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){
                
                $destino = $_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto).'/';
                foreach($imagens as $imagem){
                    if($imagem->img == $file_name){
                        $erros['erro'] = 'Você já adicionou uma foto com esse nome.';
                        return $erros;
                    }
                }
                // Faz o upload da imagem
                if(move_uploaded_file($file_tmp_name,$destino.$file_name)){
                    $sucesso['upload'] = true;
                } else {
                    $erros['erro'] = 'Ocorreu um erro no upload da imagem.';
                    return $erros;
                }
            } else {

                // Cria o diretório de imagens do produto caso não exista
                mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto), 0755, true);

                // Verifica se o diretório foi criado
                if(is_dir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){

                    $destino = $_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto).'/';

                    // Faz o upload da imagem
                    if(move_uploaded_file($file_tmp_name,$destino.$file_name)){
                        $sucesso['dir'] = 'criado';
                        $sucesso['upload'] = true;
                    } else {
                        $erros['erro'] = 'Ocorreu um erro no upload da imagem.';
                        return $erros;
                    }
                } else {
                    $erros['erro'] = 'A criação do diretório de imagens do produto não foi bem sucedida.';
                    return $erros;
                }
            }

            // Edição da imagem
            $config2['source_image'] = $_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto).'/'.$file_name;
            $config2['create_thumb'] = false;
            $config2['maintain_ratio'] = true;
            $config2['width'] = 512;
            $this->load->library('image_lib', $config2);
            if($this->image_lib->resize()){
                $sucesso['resize'] = true;
            } else {
                $erros['tipo'] = 'image_lib';
                $erros['erro'] = $this->image_lib->display_errors();
                return $erros;
            }

            // Inserção no banco de dados
            if($sucesso['upload'] == true && $sucesso['resize'] == true){                
                if($produtoLib->adicionar_imagem($id_produto, $file_name)){
                    $sucesso['sucesso'] = 'A imagem foi salva com sucesso.';
                    return $sucesso;
                }
            }

        } else {
            $erros['erro'] = 'Só são aceitas imagens no formato .jpg ou .png';
            return $erros;
        }
        

    }

    public function deletar_imagem(){
        if(sizeof($_POST) == 0) return false;
        $id_produto = $this->input->post('id_produto');
        $id_imagem = $this->input->post('id_imagem');
        $img = $this->input->post('img');

        $produtoLib = new ProdutoLib();

        if(unlink($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto).'/'.$img)){
            if($produtoLib->deletar_imagem($id_imagem, $id_produto)) { 
                $sucesso['dados'] = array(
                    'id_produto' => $id_produto,
                    'id_imagem' => $id_imagem,
                    'img' => $img
                );           
                $sucesso['sucesso'] = 'Imagem deletada com sucesso.';
                return $sucesso;
                    
            } else {
                $erros['erro'] = 'Houve um erro na remoção do registro da imagem no banco de dados.';
                return $erros;
            }
        } else {
            $erros['erro'] = 'O servidor não conseguiu apagar o arquivo da imagem.';
            return $erros;
        }
    }

    public function deletar_imagens($id_produto){
        $produtoLib = new ProdutoLib();
        $imagens = $this->produto->get_imagens_by_produto($id_produto);

        if(isset($imagens[0])){
            foreach($imagens as $imagem){
                if(unlink($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto).'/'.$imagem->img)){
                    if($produtoLib->deletar_imagens($id_produto)) { 
                        $sucesso['delete_imgs'] = 1; 
                    } 
                }
            }
            if(is_dir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){
                if(rmdir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){
                    $sucesso['delete_dir'] = 1;
                }
            } else {
                $sucesso['delete_dir'] = 'Não possui';
            }
            return $sucesso;
        } else {
            if(is_dir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){
                if(rmdir($_SERVER['DOCUMENT_ROOT'].'/uploads/produtos/'.md5($id_produto))){
                    $sucesso['delete_dir'] = 1;
                }
            } else {
                $sucesso['delete_dir'] = 'Não possui';
            }
            $sucesso['delete_imgs'] = 'Não possui';
            return $sucesso;
        }
    }

    public function adicionar_avaliacao(){
        if(sizeof($_POST) == 0) {
            $erros['erro'] = 'Nenhum dado foi enviado.';
            return $erros;
        }
        if(!$this->session->userdata('logado')){
            $erros['erro'] = 'Você precisa estar logado para enviar uma avaliação.';
            return $erros;
        }


        $id_produto = $this->input->post('id_produto');
        if($this->if_produto_exists($id_produto)->existe){ 

            $this->form_validation->set_rules('titulo', 'título', 'required|min_length[3]|max_length[40]');
            $this->form_validation->set_rules('comentario', 'comentário', 'required|min_length[3]|max_length[255]');
            $this->form_validation->set_rules('avaliacao', 'avaliação', 'required|exact_length[1]');

            if($this->form_validation->run() == FALSE){
                $erros = array(
                    'erro' => 1,
                    'titulo' => form_error('titulo', null, null),
                    'comentario' => form_error('comentario', null, null),
                    'avaliacao' => form_error('avaliacao', null, null)
                );
                return $erros;
            } else {
            
                $produtoLib = new ProdutoLib();
                
                $id_usuario = $this->session->userdata('userlogado')->id;
                $titulo = ucfirst($this->input->post('titulo'));
                $comentario = ucfirst($this->input->post('comentario'));
                $avaliacao = $this->input->post('avaliacao');

                $sucesso['usuario'] = $this->session->userdata('userlogado');
                $sucesso['sucesso'] = 'Olá '.$sucesso['usuario']->nome.', sua avaliação foi enviada para análise com sucesso.';
                $sucesso['post'] = $this->input->post();
                $sucesso['html'] = '
                <div class="col-md-8 mx-auto mt-3">
                    <div class="alert card alert-success alert-dismissible fade show" role="alert">
                        <h5 class="h5 mb-0">'.$sucesso['sucesso'].'</h5>
                        <hr>
                        <p>Dados enviados:</p>
                        <ul class="list-group bg-transparent">
                            <li class="list-group-item py-0 px-0 bg-transparent border-0 ">Título: '.$titulo.'</li>
                            <li class="list-group-item py-0 px-0 bg-transparent border-0 ">Comentário: '.$comentario.'</li>
                            <li class="list-group-item py-0 px-0 bg-transparent border-0 ">Avaliação: '.$avaliacao.' estrela(s)</li>
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
                ';

                if($produtoLib->adicionar_avaliacao($id_produto, $id_usuario, $titulo, $comentario, $avaliacao)){

                    $sucesso['sucesso'] = 'Sua avaliação foi enviada para análise com sucesso.';
                    $sucesso['usuario'] = $this->session->userdata('userlogado');
                    $sucesso['post'] = $this->input->post();
                    return $sucesso;

                } else {
                    $erros['erro'] = 'Houve algum erro no salvamento de sua avaliação.';
                    return $erros;
                }
            }
        } else {
            $erros['erro'] = 'Esse produto não existe.';
            return $erros;
        }
    }

    public function deletar_avaliacao(){
        if(sizeof($_POST) == 0) return false;
        $id_avaliacao = $this->input->post('id_avaliacao');
        $id_usuario = $this->input->post('id_usuario');

        $produtoLib = new ProdutoLib();

        if($produtoLib->deletar_avaliacao($id_avaliacao)) { 
            $sucesso['dados'] = array(
                'id_avaliacao' => $id_avaliacao
            );           
            $sucesso['delete'] = 1;
            return $sucesso;
                
        } else {
            $erros['erro'] = 'Não foi possível deletar a avaliação.';
            return $erros;
        }
    }

    public function publicar_avaliacao($id){
        $produtoLib = new ProdutoLib();

        if($produtoLib->publicar_avaliacao($id)){
            $sucesso['sucesso'] = 1;
            return $sucesso;
        } else {
            $erros['erro'] = 'Não foi possível publicar a avaliação.';
            return $erros;
        }
    }

    // Imagens

    public function get_imagens_by_produto($id_produto){
        $this->db->select('id, img');
        $this->db->where('id_produto', $id_produto);
        return $this->db->get('tb_produto_img')->result();
    }

    public function get_qtd_imagens_by_produto($id_produto){
        $this->db->select('count(img.id) as qtd_imagens');
        $this->db->from('tb_produto_img img');
        $this->db->where('img.id_produto', $id_produto);
        return $this->db->get()->row_object();
    }

    // Produtos
    public function get_produtos(){
        $this->db->select('prod.id, prod.codigo, prod.nome, prod.quantidade, prod.modelo, prod.marca, prod.descricao, prod.fabricante, prod.status, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento'); 
        $this->db->from('tb_produto prod');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->group_by('prod.id');
        return $this->db->get()->result();
    }

    public function get_produtos_busca($busca, $consulta = null, $condicao = null, $pular=null, $prod_por_pagina=null){
        $termos = explode(' ', $busca);
        $this->db->select('prod.id, prod.nome, prod.status, prod.preco_venda, sum(aval.avaliacao)/count(aval.id) as media_avaliacao, prod.preco_promocional, prod.promocao, count(img.id) as qtd_imagens, img.img, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento'); 
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_avaliacao aval', 'aval.id_produto = prod.id', 'left');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->having('prod.status', 1);
        foreach($termos as $key => $termo){
            $this->db->or_like('prod.nome', $termo); 
            $this->db->or_like('prod.descricao', $termo); 
            $this->db->or_like('prod.modelo', $termo); 
            $this->db->or_like('sub.nome', $termo); 
            $this->db->or_like('cat.nome', $termo); 
            $this->db->or_like('dep.nome', $termo); 
        }

        if($consulta == 'preco'){
            $this->db->order_by('prod.preco_venda', $condicao);
            $this->db->order_by('prod.preco_promocional', $condicao);

        } else if ($consulta == 'media_avaliacao'){
            $this->db->order_by('media_avaliacao', $condicao);

        } else if($consulta == 'avaliacao'){
            $this->db->having('media_avaliacao <', $condicao+1);
            $this->db->having('media_avaliacao >=', $condicao);
        }
        
        if($pular && $prod_por_pagina){
            $this->db->limit($prod_por_pagina, $pular);
        } else {
            $this->db->limit(12);
        }

        $this->db->group_by('prod.id');
        return $this->db->get()->result();
    }

    // Retorna 1 se o produto passado por parâmetro existe
    public function if_produto_exists($id_produto){
        $this->db->select('count(prod.id) as existe');
        $this->db->from('tb_produto prod');
        $this->db->where('prod.id', $id_produto);
        return $this->db->get()->row_object();
    }

    // Informações para a página de edição no painel administrativo
    public function get_produto_by_id($id_produto){
        $this->db->select('prod.*, count(img.id) as qtd_imagens, sub.nome as subcategoria, cat.nome as categoria, cat.id as id_categoria, dep.nome as departamento'); 
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('prod.id', $id_produto);
        return $this->db->get()->row_object();
    }

    // Informações para o carrinho
    public function get_qtd_produto_by_id($id_produto){
        $this->db->select('prod.quantidade'); 
        $this->db->from('tb_produto prod');
        $this->db->where('prod.id', $id_produto);
        return $this->db->get()->row_object();
    }

    // Informações para a página do produto na loja
    public function get_produto_by_id_loja($id_produto){
        $this->db->select('prod.id, prod.nome, prod.codigo, prod.marca, prod.modelo, prod.preco_venda, prod.preco_custo, prod.preco_promocional, prod.promocao, prod.destaque, prod.fabricante, prod.status, prod.quantidade, prod.unidade, prod.caixa, prod.garantia, prod.peso_kg, prod.altura_cm, prod.largura_cm, prod.profundidade_cm, prod.descricao, count(img.id) as qtd_imagens, sub.nome as subcategoria, cat.nome as categoria, cat.id as id_categoria, dep.nome as departamento'); 
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('prod.id', $id_produto);
        return $this->db->get()->row_object();
    }

    public function get_produtos_by_subcategoria_loja($id_subcategoria, $consulta = null, $condicao = null, $pular=null, $prod_por_pagina=null){
        $this->db->select('prod.id, prod.nome, prod.status, prod.preco_venda, sum(aval.avaliacao)/count(aval.id) as media_avaliacao, prod.preco_promocional, prod.promocao, count(img.id) as qtd_imagens, img.img, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento'); 
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_avaliacao aval', 'aval.id_produto = prod.id', 'left');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('prod.status', 1);
        $this->db->where('sub.id', $id_subcategoria);

        if($consulta == 'preco'){
            $this->db->order_by('prod.preco_venda', $condicao);
            $this->db->order_by('prod.preco_promocional', $condicao);

        } else if ($consulta == 'media_avaliacao'){
            $this->db->order_by('media_avaliacao', $condicao);

        } else if($consulta == 'avaliacao'){
            $this->db->having('media_avaliacao <', $condicao+1);
            $this->db->having('media_avaliacao >=', $condicao);
        }
        $this->db->group_by('prod.id');
        
        if($pular && $prod_por_pagina){
            $this->db->limit($prod_por_pagina, $pular);
        } else {
            $this->db->limit(12);
        }
        
        return $this->db->get()->result();
    }

    // Informação para a página de deletar subcategoria no painel administrativo
    public function get_produtos_by_subcategoria($id){
        $this->db->where('id_subcategoria', $id);
        return $this->db->count_all_results('tb_produto');
    }

    // Destaques
    public function get_produtos_destaques_by_categoria($id_categoria){
        $this->db->select('prod.id, prod.nome, prod.preco_venda, prod.preco_promocional, prod.promocao, img.img, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento');
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('prod.destaque', 1);
        $this->db->where('cat.id', $id_categoria);
        $this->db->group_by('prod.id');
        $this->db->limit(12);
        return $this->db->get()->result();
    }

    public function get_produtos_destaques(){
        $this->db->select('prod.id, prod.nome, prod.preco_venda, prod.preco_promocional, prod.promocao, img.img, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento');
        $this->db->from('tb_produto prod');
        $this->db->join('tb_produto_img img', 'img.id_produto = prod.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->where('prod.destaque', 1);
        $this->db->group_by('prod.id');
        $this->db->limit(12);
        return $this->db->get()->result();
    }

    // Avaliações
    public function get_avaliacoes($status){
        $this->db->select('aval.id, aval.titulo, aval.comentario, aval.avaliacao, aval.data_envio as data, aval.status, prod.nome as produto, prod.id as id_produto, sub.nome as subcategoria, cat.nome as categoria, dep.nome as departamento, usuario.email as usuario_email'); 
        $this->db->from('tb_produto_avaliacao aval');
        $this->db->join('tb_produto prod', 'prod.id = aval.id_produto', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id = prod.id_subcategoria', 'left');
        $this->db->join('tb_categoria cat', 'cat.id = sub.id_categoria', 'left');
        $this->db->join('tb_departamento dep', 'dep.id = cat.id_departamento', 'left');
        $this->db->join('tb_usuario usuario', 'usuario.id = aval.id_usuario', 'left');
        $this->db->where('aval.status', $status);
        $this->db->group_by('aval.id');
        return $this->db->get()->result();
    }

    public function get_avaliacao_by_id($id_avaliacao){
        $this->db->select('aval.id, aval.titulo, aval.comentario, aval.avaliacao, aval.data_envio as data, aval.status, prod.nome as produto, usuario.id as id_usuario, usuario.email as email_usuario, usuario.nome as nome_usuario, usuario.sobrenome as sobrenome_usuario');
        $this->db->from('tb_produto_avaliacao aval');
        $this->db->join('tb_produto prod', 'prod.id = aval.id_produto', 'left');
        $this->db->join('tb_usuario usuario', 'usuario.id = aval.id_usuario', 'left');
        $this->db->where('aval.id', $id_avaliacao);
        return $this->db->get()->row_object();
    }

    public function get_avaliacoes_by_produto($id_produto){
        $this->db->select('aval.titulo, aval.comentario, aval.avaliacao, aval.data_envio as data, usuario.nome as nome_usuario, usuario.sobrenome as sobrenome_usuario'); 
        $this->db->from('tb_produto_avaliacao aval');
        $this->db->join('tb_produto prod', 'prod.id = aval.id_produto', 'left');
        $this->db->join('tb_usuario usuario', 'usuario.id = aval.id_usuario', 'left');
        $this->db->where('aval.status', 1);
        $this->db->where('aval.id_produto', $id_produto);
        return $this->db->get()->result();
    }

    public function get_avaliacoes_by_produto_limit($id_produto){
        $this->db->select('aval.titulo, aval.comentario, aval.avaliacao, aval.data_envio as data, usuario.nome as nome_usuario, usuario.sobrenome as sobrenome_usuario'); 
        $this->db->from('tb_produto_avaliacao aval');
        $this->db->join('tb_produto prod', 'prod.id = aval.id_produto', 'left');
        $this->db->join('tb_usuario usuario', 'usuario.id = aval.id_usuario', 'left');
        $this->db->where('aval.status', 1);
        $this->db->where('aval.id_produto', $id_produto);
        $this->db->limit(4);
        return $this->db->get()->result();
    }

    public function get_soma_avaliacoes_by_produto($id_produto){
        $this->db->select_sum('avaliacao');
        $this->db->where('id_produto', $id_produto);
        $this->db->where('status', 1);
        return $this->db->get('tb_produto_avaliacao')->row_object();
    }
    
    public function get_qtd_avaliacoes_by_produto($id_produto){
        $this->db->select('count(aval.id) as qtd_avaliacoes');
        $this->db->from('tb_produto_avaliacao aval');
        $this->db->where('aval.id_produto', $id_produto);
        $this->db->where('aval.status', 1);
        return $this->db->get()->row_object();
    }
}
