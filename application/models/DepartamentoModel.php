<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/DepartamentoLib.php';

class DepartamentoModel extends CI_Model {

    public function editar(){
        if(sizeof($_POST) == 0) return false;

        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[3]|max_length[30]|is_unique[tb_departamento.nome]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null)
            );
            return $erros;
        } else {
            $nome = $this->input->post('nome');

            if($this->session->userdata('userlogado')->tipo >= 2){
                $id = $this->input->post('id');
            }
            
            $departamentoLib = new DepartamentoLib();
			if($departamentoLib->editar($nome, $id)) {
				$dados = array(
                    'nome' => $nome,
                    'id' => $id
				);
				$sucesso = array(
					'nome' => form_error('nome', null, null),
					'edit' => 1,
					'dados' => $dados
				);
				return $sucesso;
					
            } else {
				$erros['500'] = 1;
				return $erros;
            }
		}
    }

    public function deletar(){
        if(sizeof($_POST) == 0) return false;
        $this->load->model('CategoriaModel', 'categoria');
        $id = $this->input->post('id');
        if(!$this->categoria->get_categorias_by_departamento($id)){

            $departamentoLib = new DepartamentoLib();
            // Deletar
            if($departamentoLib->deletar($id)) { 
                $sucesso['dados'] = array(
                    'id'=>$id
                );           
                $sucesso['delete'] = 1;
                return $sucesso;
                    
            } else {
                $erros['500'] = 1;
                return $erros;
            }
        } else {
            $erros['erro'] = 'Você não pode deletar um departamento enquanto ele estiver associado a uma ou mais categorias.';
            return $erros;
        }
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'nome do departamento', 'required|min_length[3]|max_length[30]|is_unique[tb_departamento.nome]');

        if($this->form_validation->run() == FALSE){
            $erros = array(
                'nome' => form_error('nome', null, null)
            );
            return $erros;
        } else {
            // Carregando modelo de endereços
            $departamentoLib = new DepartamentoLib();
            
            $nome = ucfirst($this->input->post('nome'));

            if($departamentoLib->criar($nome)) {
                
                $sucesso['nome'] = form_error('nome', null, null);                
                $sucesso['redirect'] = base_url('admin/departamento');
                
                return $sucesso;
            } else {
                $erros['500'] = 1;
                return $erros;
            }  
        } 
    }

    public function get_departamento_by_id($id){
        $this->db->select('id, nome');
        $this->db->where('id', $id);
        return $this->db->get('tb_departamento')->row_object();
    }

    public function get_departamentos(){
        $this->db->select('dep.id, dep.nome, count(cat.id) as qtd_categorias');
        $this->db->from('tb_departamento dep');
        $this->db->join('tb_categoria cat', 'cat.id_departamento = dep.id', 'left');
        $this->db->group_by('dep.id');
        return $this->db->get()->result();
    }

    public function get_departamentos_loja(){
        $this->db->select('dep.id, dep.nome, count(cat.id) as qtd_categorias');
        $this->db->from('tb_departamento dep');
        $this->db->join('tb_categoria cat', 'cat.id_departamento = dep.id', 'left');
        $this->db->join('tb_subcategoria sub', 'sub.id_categoria = cat.id', 'inner');
        $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
        $this->db->where('prod.status', 1);
        $this->db->group_by('dep.id');
        $this->db->limit(4);
        return $this->db->get()->result();
    }

    public function get_departamento_by_name($departamento){

        $departamentos_bd = $this->get_departamentos();

        foreach($departamentos_bd as $departamento_bd){

            if(url_amigavel($departamento_bd->nome) === $departamento){

                $this->db->select('dep.id, dep.nome, count(cat.id) as qtd_categorias, count(prod.id) as qtd_produtos');
                $this->db->from('tb_departamento dep');
                $this->db->join('tb_categoria cat', 'cat.id_departamento = dep.id', 'left');
                $this->db->join('tb_subcategoria sub', 'sub.id_categoria = cat.id', 'inner');
                $this->db->join('tb_produto prod', 'prod.id_subcategoria = sub.id', 'inner');
                $this->db->where('prod.status', 1);
                $this->db->where('dep.id', $departamento_bd->id);
                return $this->db->get()->row_object();
            }

        }
    }
}