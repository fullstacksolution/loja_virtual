<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/Frete/CalculaFrete.php';

class Frete extends CI_Model {

    public function calcular(){

        $cep = $this->input->post('cep');

        # setando dados
        $_args = array( 
           'nCdServico' => ServicosCorreios::SEDEX,
           'sCepOrigem'=>'07132030', 'sCepDestino' => $cep, 
           'nVlPeso' => '1', 'nVlComprimento' => 30, 
           'nVlAltura' => 15, 'nVlLargura' => 20
        );
        
        # solicitando calculo
        $calculafrete = new CalculaFrete( $_args );
        
        # exibindo os dados
        $frete['sedex'] = $calculafrete->request();

        # setando dados
        $_args = array( 
            'nCdServico' => ServicosCorreios::PAC,
            'sCepOrigem'=>'07132030', 'sCepDestino' => $cep, 
            'nVlPeso' => '1', 'nVlComprimento' => 30, 
            'nVlAltura' => 15, 'nVlLargura' => 20
         );
         
         # solicitando calculo
         $calculafrete = new CalculaFrete( $_args );

         
        # exibindo os dados
        $frete['pac'] = $calculafrete->request();

        return $frete;
    }

}
