<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/CarrinhoLib.php';

class CarrinhoModel extends CI_Model {

    public function adicionar(){

        $id_produto = $this->input->post('id_produto');

        // Verifica se existe usuário logado
        if($this->session->userdata('logado')){
            $id_usuario = $this->session->userdata('userlogado')->id;
        
        // Se não retorne a URL do redirect personalizado para login
        } else {
            $erros['redirect'] = base_url('sign/in?status=add_carrinho&produto='.$id_produto);
            return $erros;
        }
        
        $id_session = $this->session->session_id;

        // Verifica se a quantidade enviada por POST é válida
        if($this->input->post('quantidade') > 0){
            $quantidade = $this->input->post('quantidade');
            
        // Se não for, retornar
        } else {
            $erros['erro'] = 'A quantidade solicitada é inválida.';
            $erros['tipo'] = 'quantidade';
            return $erros;
        }

        // Verificar se o produto em questão existe
        if($this->produto->if_produto_exists($id_produto)->existe){

            $carrinhoLib = new CarrinhoLib();
            $verifica_adicionado = $carrinhoLib->verificar_adicionado($id_produto, $id_usuario);

            // Verifica se o produto já foi adicionado pelo usuário
            if(!$verifica_adicionado->existe){

                // Verificar a quantidade do produto no estoque
                if($this->produto->get_qtd_produto_by_id($id_produto)->quantidade >= $quantidade){
                    
                    if($carrinhoLib->adicionar($id_produto, $id_usuario, $id_session, $quantidade)){
                        $sucesso['qtd_produtos'] = $this->get_qtd_produtos()->qtd_produtos;
                        $sucesso['carrinho'] = 'Produto adicionado ao carrinho.';
                        return $sucesso;
                    } else {
                        $erros['erro'] = 'Não foi possível adicionar ao carrinho.';
                        return $erros;
                    }

                // Se não estiver disponível a quantidade solicitada
                } else {
                    $erros['erro'] = 'A quantidade solicitada está indisponível.';
                    $erros['tipo'] = 'quantidade';
                    return $erros;
                }

            } else {
                $erros['carrinho'] = 'O produto já está no seu carrinho';
                $erros['quantidade'] = $verifica_adicionado->quantidade;
                return $erros;
            }

        // Se não existir retornar
        } else {
            $erros['erro'] = 'O produto não existe.';
            return $erros;
        }
    }

    public function deletar(){

        // Verifica se existe usuário logado
        if($this->session->userdata('logado')){
            $id_usuario = $this->session->userdata('userlogado')->id;
        
        // Se não retorne a URL do redirect personalizado para login
        } else {
            $erros['redirect'] = base_url('sign/in?status=page_carrinho');
            return $erros;
        }
        
        $id_produto = $this->input->post('id_produto');

        $carrinhoLib = new CarrinhoLib();
        if($carrinhoLib->deletar($id_produto, $id_usuario)){
            $sucesso['sucesso'] = 1;
            $sucesso['delete'] = 1;
            return $sucesso;
        } else {
            $erros['erro'] = 'Não foi possível deletar o produto do carrinho.';
            return $erros;
        }

    }

    public function atualizar_quantidade(){

        // Verifica se existe usuário logado
        if($this->session->userdata('logado')){
            
            $id_usuario = $this->session->userdata('userlogado')->id;
            $id_produto = $this->input->post('id_produto');
            $qtd_atualizada = $this->input->post('qtd_atualizada');

            $carrinhoLib = new CarrinhoLib();
            $carrinho = $carrinhoLib->atualizar_quantidade($id_produto, $id_usuario, $qtd_atualizada);

            if($carrinho){
                $sucesso['sucesso'] = 1;
                $sucesso['atualizado'] = $carrinho;
                
                return $sucesso;
            } else {
                $erros['erro'] = 1;
                return $erros;
            }

        } else {
            $erros['erro'] = 1;
            $erros['user'] = 'Usuário deslogado.';
            return $erros;
        }

    }

    public function get_carrinho(){

        // Verifica se existe usuário logado
        if($this->session->userdata('logado')){
            
            $id_usuario = $this->session->userdata('userlogado')->id;

            $carrinhoLib = new CarrinhoLib();
            $carrinho = $carrinhoLib->get_carrinho($id_usuario);

            if($carrinho){
                $sucesso['sucesso'] = 1;
                $sucesso['carrinho'] = $carrinho;
                $sucesso['qtd_produtos'] = count($carrinho);
                
                return $sucesso;
            } else {
                $erros['erro'] = 1;
                $erros['qtd_produtos'] = 0;
                return $erros;
            }

        } else {
            $erros['erro'] = 1;
            $erros['user'] = 'Usuário deslogado.';
            return $erros;
        }

    }

    public function get_qtd_produtos(){

        if(!$this->session->userdata('logado')){
            return 0;
        }

        $id_usuario = $this->session->userdata('userlogado')->id;

        $carrinhoLib = new CarrinhoLib();
        return $carrinhoLib->get_qtd_produtos($id_usuario);


    }
}
