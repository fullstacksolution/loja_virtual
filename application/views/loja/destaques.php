        <!-- Container da seção de destaques -->
        <div class="container">
            <div class="row mt-4">
                <div class="col text-center">
                    <h2>Produtos em destaque</h2>
                </div>
            </div>
            <div class="row mt-4">
            <!-- Cards de produtos -->
<?php for ($i=0; $i < 4; $i++) { ?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-4">
                    <div class="card">
                        
                        <div class="view view-cascade p-3 p-md-5">
                            <img class="card-img-top img-fluid" src="assets/img/pci.png " alt="Card image cap">
                            <a href="<?= base_url('produto')?>">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    
                        <div class="view view-cascade overlay card-body card-body-cascade">
                    
                            <h5 class="card-title my-0"><strong>Arduino UNO</strong></h5>
                            <h6 class="font-weight-bold text-muted my-2">Eletrônica</h6>
                            <h4 class="h4">R$100,00</h4>
                            <h4 class="card-text">2x de R$50,00 s/ juros.</h4>
                            <a href="<?= base_url('produto')?>">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                    
                        </div>
                        <a href="<?= base_url('produto')?>" class="ml-4 mb-4 h4 text-ipci-primary">Ver mais</a>
                    </div>
                </div>
<?php } ?>
            <!-- /Cards de produtos -->
            </div>

        </div>
        <!-- /Container da seção de destaques -->

