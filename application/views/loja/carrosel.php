		<!-- Carrosel de produtos -->
		<div>
			<div id="carrosel" class="container carousel slide" data-ride="carousel">

				<ol class="carousel-indicators">
<?php if(isset($banners[0])) { foreach($banners as $item => $banner){ ?>
					<li data-target="#carrosel" data-slide-to="<?= $item ?>" <?= ($item) ? 'class="active"' : '' ?>></li>
<?php } ?>	
<?php } ?>	
				</ol>

				<!-- Inner -->
				<div class="carousel-inner" role="listbox">
<?php if(isset($banners[0])) { foreach($banners as $item => $banner){ ?>
					<div class="carousel-item carousel-item2 <?= ($item == 0) ? 'active' : '' ?>">
						<div class="view">
							<a href="<?= ($banner->link) ? $banner->link : '#' ?>">
								<img class="d-block w-100" src="<?=base_url('uploads/banner/'.$banner->img)?>"
								alt="<?=$banner->nome?>">
								<div class="mask rgba-black-slight"></div>
							</a>
						</div>
					</div>
<?php } // Fim do foreach ?>			
				</div>
				<!-- /Inner -->

<?php if(isset($banners[1])) { ?>
				<!-- Controles -->
				<div id="controle-maior">
					<a href="#carrosel" class="carousel-control-card carousel-control-prev carousel-control-prev-card rounded" data-slide="prev">
						<span class="carousel-control-prev-icon w-100 h-100" ></span>
					</a>
					<a href="#carrosel" class="carousel-control-card carousel-control-next carousel-control-next-card  rounded" data-slide="next">
						<span class="carousel-control-next-icon w-100 h-100"></span>
					</a>
				</div>
				
				<div id="controle-menor">
					<a class="carousel-control-prev" href="#carrosel" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Anterior</span>
					</a>
					<a class="carousel-control-next" href="#carrosel" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Próximo</span>
					</a>
				</div>
				<!-- /Controles -->
<?php } // Fim do if ?>		

<?php } else { ?>	
					<!-- Configuração padrão caso não aja banners -->
					<div class="carousel-item carousel-item2 active">
						<div class="view">
							<a href="<?= base_url('loja') ?>">
								<img class="d-block w-100" src="<?=base_url('uploads/banner.jpg')?>"
								alt="Bem vindo à loja">
								<div class="mask rgba-black-slight"></div>
							</a>
						</div>
					</div>	
				</div>
				<!-- /Inner -->		
<?php } ?>				

			</div>
		</div>
		<!-- /Carrosel de produtos -->

		<script>
			$(document).ready(function () {

				if ($(window).width() < 1024){
					$('#carrosel').removeClass('container')
					$('#controle-maior').addClass('d-none')
				} else {
					$('#carrosel').addClass('container')
					$('#carrosel').parent().addClass('background-gradient-ipci-dark py-5')
					$('div.carousel-item2 img').addClass('rounded-xl')
					$('div.carousel-item2 div.mask').addClass('rounded-xl')
					$('#controle-menor').addClass('d-none')
				}

				$(window).resize(function (e) { 
					if ($(window).width() < 1024){
						$('#carrosel').parent().removeClass('background-gradient-ipci-dark py-5')
						$('div.carousel-item2 img').removeClass('rounded-xl')
						$('div.carousel-item2 div.mask').removeClass('rounded-xl')
						$('#carrosel').removeClass('container')
						$('#controle-maior').addClass('d-none')
						$('#controle-menor').removeClass('d-none')
					} else {
						$('#carrosel').addClass('container')
						$('#carrosel').parent().addClass('background-gradient-ipci-dark py-5')
						$('div.carousel-item2 img').addClass('rounded-xl')
						$('div.carousel-item2 div.mask').addClass('rounded-xl')
						$('#controle-menor').addClass('d-none')
						$('#controle-maior').removeClass('d-none')
					}
				})

			});
		</script>

