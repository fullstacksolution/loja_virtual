<div class="col-xl-10 col-lg-9 mx-auto">
    <div class="row">

<?php if(isset($produtos[0])){ foreach($produtos as $produto) {  ?>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-4 d-flex">
            <!-- Card -->
            <div class="card">
                <div class="view view-cascade p-5 p-sm-4 p-md-5 p-lg-4">
                    <img class="card-img-top img-fluid" src="<?= ($produto->img) ? base_url('uploads/produtos/'.md5($produto->id)).'/'.$produto->img : base_url('uploads/semimagem.jpg') ?>" alt="<?= $produto->nome ?>">
                    <a href="<?= base_url('produto/'.$produto->id.'/'.url_amigavel($produto->nome))?>">
                        <div class="mask"></div>
                    </a>
                </div>
                <div class="view view-cascade overlay pt-0 card-body card-body-cascade">
                    <div class="row">
                        <div class="col text-center font-small">
                            <ul class="list-inline mb-3">
<?php if ($produto->media_avaliacao == 0) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($produto->media_avaliacao < 2) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($produto->media_avaliacao < 3) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($produto->media_avaliacao < 4) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($produto->media_avaliacao < 5) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($produto->media_avaliacao == 5) { ?>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
<?php } ?>
                            </ul>

                        </div>
                    </div>
                    <h6 class="card-title my-0"><strong><?= $produto->nome?></strong></h6>
                    <h6 class="font-weight-bold text-muted my-2"><?= $produto->departamento ?></h6>
<?php if($produto->promocao){ ?>
                    <p class="mb-0 text-muted">De: <s class="text-ipci-secondary">R$<?= transforma_preco($produto->preco_venda)?></s> </p>
                    <p class="text-muted mb-0">Por: <span class="h4 font-weight-normal text-dark">R$<?= transforma_preco($produto->preco_promocional) ?></span></p>
<?php if($produto->preco_promocional > 100){ ?>
					<h4 class="card-text mb-0 mt-2">2x de R$<?= transforma_preco($produto->preco_promocional / 2)?> s/ juros.</h4>
<?php } ?>
<?php } else { ?>
					<h4 class="h4 mb-0">R$<?= transforma_preco($produto->preco_venda) ?></h4>
<?php if($produto->preco_venda > 100){ ?>
					<h4 class="card-text mb-0">2x de R$<?= transforma_preco($produto->preco_venda / 2)?> s/ juros.</h4>
<?php } ?>
<?php } ?>
					<a href="<?= base_url('produto/'.$produto->id.'/'.url_amigavel($produto->nome)) ?>">
				        <div class="mask rgba-white-slight"></div>
				    </a>
			    </div>
				<a href="<?= base_url('produto/'.$produto->id.'/'.url_amigavel($produto->nome))?>" class="ml-4 mb-4 h4 text-ipci-primary mt-0">Ver mais</a>

			</div>
            <!-- Card -->
        </div>
<?php } } else { ?>
    <div class="col-12 text-center mx-auto">
        <hr>
        <h4 class="my-5">Não encontramos resultados para esta busca.</h4>
        <hr>
    </div>
<?php } ?>
    </div>
    <?= '<nav aria-label="Page navigation example">'.$links_paginacao.'</nav>' ?>
</div>
