<div class="container">
    <div class="row mt-4">
        <div class="col mx-auto text-center">
            <h1>Mapa do site</h1>
            <h3>Categorias e subcategorias do site</h3>
        </div>
    </div>


<?php if(isset($departamentos[0])){ ?>


    <div class="row mt-4">

<?php foreach($departamentos as $departamento){ if($departamento->qtd_categorias == 0) continue;?>

        <div class="col-xl-2 col-lg-3 col-md-4 col-12 mt-4 text-center text-md-left card py-3 mx-2">
            <h4><a class="h4 text-ipci-nocolor" href="<?= base_url('categoria/'.url_amigavel($departamento->nome)) ?>"><?= $departamento->nome ?></a></h4>

<?php foreach($categorias as $categoria){ if($categoria->id_departamento != $departamento->id || $categoria->qtd_subcategorias == 0) continue; ?>

            <ol class="list-group">
                <li class="list-group-item py-0 pl-md-0 border-0">
                    <h5><a href="<?=base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome))?>" class="font-weight-normal text-ipci-nocolor">
                        <?= $categoria->nome ?>
                    </a></h5>
                    <ol class="list-group ml-md-3">
<?php foreach($subcategorias as $subcategoria) { if($subcategoria->id_categoria != $categoria->id ) continue; ?>

                        <li class="list-group-item py-0 pl-md-0 border-0">
                            <h6><a href="<?=base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome).'/'.url_amigavel($subcategoria->nome))?>" class="text-ipci-nocolor ">
                                <?= $subcategoria->nome ?>
                            </a></h6>
                        </li>

<?php } // /foreach subcategoria ?>  
                    </ol>
                </li>
            </ol>
<?php } // /foreach categoria ?>


        </div>

<?php } // /foreach departamento ?>


<?php } else { // /isset departamento ?>
    <div class="row mt-4">
        <div class="col-md-6 mx-auto text-center">
            <p>A loja ainda não possui produtos publicados.</p>
        </div>
    </div>

<?php } ?>


</div>