        <div class="container">

            <div class="row mt-4">
                <div class="col text-center">
                    <h1>Carrinho</h1>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">

                    <!-- Card do carrinho -->
                    <div class="card shopping-cart">
                        <div class="card-body p-4">

                            <div class="row">
                                <div id="lista_carrinho" class="col-lg-8">
                                    
                                </div>
                                <!-- Grid column -->
            
                                <div class="col-lg-4 mt-4 mt-lg-0" id="resumo">
                                    <div class="d-none" id="xmlresponse" value=""></div>
                                    <h4 class="h4">Resumo de pedido</h4>
                                    <p class="text-muted text-sm">Frete e custos adicionais são baseados nos valores que você utilizar.</p>
                                    <table class="table">
                                        <tr>
                                            <th class="py-4">Subtotal do pedido </th>
                                            <td id="subtotal" class="py-4 text-right text-muted h5 font-weight-light"></td>
                                        </tr>
                                        <tr>
                                            <th class="py-4">Frete</th>
                                            <td id="frete" class="py-4 text-right text-muted h5 font-weight-light">Calcular</td>
                                        </tr>
                                        <!-- <tr>
                                            <th class="py-4">Taxas</th>
                                            <td class="py-4 text-right text-muted">R$00,00</td>
                                        </tr> -->
                                        <tr>
                                            <th class="py-4">Total</th>
                                            <td id="total" class="py-4 text-right h3 font-weight-bold"></td>
                                        </tr>
                                    </table>
                                    
                                    <div class="row mt-4">
                                        <div class="col">
                                            <h5>Frete</h5>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-7 input-group">
                                            <div class="md-form my-0">
                                                <input type="text" class="form-control" size="15" maxlength="9" id="cep" name="cep">
                                                <label for="cep">Calcule o frete</label>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-primary rounded-right p-1" type="button" id="btn_cep">Ok</button>
                                            </div>
                                        </div>
                                        <div class="col-md-5 align-self-center text-center">
                                            <a class="text-ipci-primary font-small" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu cep</a>
                                        </div>
                                    </div>

                                    <a href="<?= base_url('checkout') ?>" class="btn btn-primary btn-block btn-lg mt-4">Proceder ao Checkout</a>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
