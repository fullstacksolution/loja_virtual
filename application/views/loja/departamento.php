<div class="col-12 mx-auto">
    <div class="row">
<?php foreach($categorias as $categoria) { ?>
        <div class="col mb-4 mx-auto">
            <!-- Card -->
            <div class="card">
                <div class="view view-cascade overlay d-flex align-items-center p-3 text-center mx-auto">
                
                    <h3 class="card-title pt-2"><strong><?= $categoria->nome ?></strong></h3>
                    <a href="<?= base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome))?>">
                        <div class="mask rgba-white-slight"></div>
                    </a>
            
                </div>

            </div>
            <!-- Card -->
        </div>
<?php } ?>

</div>