	<?php
	$this->load->helper('url');
	if(uri_string() == 'loja' || uri_string() == '') { ?>
	<style>
		html {
			margin-top: 128px !important; /* 75px*/
		}
	</style>
	<?php
	} else { ?>
	<style>
		html {
			margin-top: 128px !important;
		}
		</style>
	<?php
	}
	?>

		<!-- Nav de Departamentos -->
		<nav id="navbar" class="navbar fixed-top fixed-top-2 navbar-dark d-block bg-dark shadow-none">
			<div class="container">
				<button class="navbar-toggler mx-0" type="button" data-toggle="collapse" data-target="#nav" aria-controls="nav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon "></span>
					<h6 class="d-inline-flex my-0">Categorias</h6>
				</button>
				<div class="dropdown-divider"></div>
				<a class="text-ipci-nocolor-w text-left mr-auto" href="<?=base_url('servicos')?>" target="_blank" rel="Serviços">Serviços</a>
			</div>
		</nav>
		<!-- /Nav de Departamentos -->

		<!-- Collapse da Nav de Departamentos -->
		<div class="collapse fixed-top fixed-top-2" id="nav" style="z-index: 3000; margin-top: 129px; margin-bottom: 0;">
			<div class="bg-dark p-4">
				<div class="container">

<?php if(isset($departamentos[0])){ ?>

					<div class="row d-none d-md-flex">
<?php foreach($departamentos as $departamento){ if($departamento->qtd_categorias == 0) continue;?>

						<div class="col-lg-3 col-md-4 col-sm-12 mr-auto text-left">
							<h5 class="text-white h4"><a href="<?= base_url('categoria/'.url_amigavel($departamento->nome)) ?>"><?= $departamento->nome ?></a></h5>

<?php $count=1; foreach($categorias as $categoria){ if($categoria->id_departamento != $departamento->id || $categoria->qtd_subcategorias == 0 || $count > 7) continue; else $count++; ?>
<?php if($count <= 7 ){ ?>
							<div class="btn-group mb-1">
								<button class="text-white bg-transparent dropdown-item dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $categoria->nome ?></button>
								<div class="dropdown-menu text-ipci-nocolor">
									<a class="dropdown-item px-3" href="<?= base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome)) ?>"><?= $categoria->nome ?></a>
									<div class="dropdown-divider"></div>
<?php $count_sub=1; foreach($subcategorias as $subcategoria) { if($subcategoria->id_categoria != $categoria->id || $count_sub > 7) continue; else $count_sub++; ?>
<?php if($count <= 7 ){ ?>	
									<a class="dropdown-item px-3" href="<?= base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome).'/'.url_amigavel($subcategoria->nome)) ?>"><?= $subcategoria->nome ?></a>
<?php } else { ?>
									<a class="px-4 py-1 text-white bg-transparent mb-1 text-ipci-nocolor-w" href="<?= base_url('categoria/'.$categoria->id.'/'.url_amigavel($categoria->nome).'-'.url_amigavel($departamento->nome)) ?>">Ver todas...</a>

	<?php } ?>
<?php } ?>
								</div>
							</div><br>
<?php } else { ?>
							<a class="px-4 py-1 text-white bg-transparent mb-1 text-ipci-nocolor-w" href="<?= base_url('categoria') ?>">Ver todas...</a>
<?php } ?>
<?php } ?>

						</div>

<?php }?>

					</div>
					<hr class="bg-ipci-primary d-none d-md-flex">
<?php }?>
					<div class="row">
						<div class="col mx-auto text-left">
							<a href="<?= base_url('mapa') ?>" class="btn btn-success">Ver todas as categorias...</a>
							<a href="<?= base_url('servicos') ?>" class="btn btn-white">Ver serviços...</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Collapse da Nav de Categorias -->

