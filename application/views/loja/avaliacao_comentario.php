        <span id="avaliacoes"></span>

        <div class="container">

            <div class="row mt-4">
                <div class="col text-center">
                    <h2>Avaliações do Produto</h2>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">

                    <!-- Card de avaliações -->
                    <div class="card">

                        <div class="card-header p-4">
                            
                            <div id="row_avaliacao" class="row">
                                <div class="col mt-1 mt-sm-3">

                                    <ul class="list-inline mb-0">
                                        <div class="row">
                                            <div class="col-12 col-md-1 text-center mr-3">
                                                <li class="list-inline-item h6 mb-0 font-weight-normal text-right">
                                                    <h4 class="h4 font-weight-bold"><?= $media_avaliacao ?></h4>
                                                </li>
                                            </div>
                                            <div class="col-12 col-md-6 text-md-left text-center">
<?php if ($media_avaliacao == 0) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 2) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 3) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 4) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 5) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao == 5) { ?>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0 h4"><i class="fas fa-star text-ipci-primary"></i></li>
<?php } ?>
                                                <li class="list-inline-item h6 mb-0 font-weight-normal text-right">
                                                    <p class="">(<?= $qtd_avaliacoes ?>)</p>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                                <div class="col-md-4 mt-1 mt-sm-2 text-center text-md-right ml-auto">
                                    <button id="btn_avaliacao" class="btn bg-ipci-primary text-white">Avaliar produto</button>
                                </div>
                            </div>

                        </div>

                        <!-- Card body -->
                        <div id="lista_avaliacoes" class="card-body p-4">

<?php if($avaliacoes == null) { // Início da condicional ?>
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <p>O produto ainda não possui avaliações.</p>
                                </div>
                            </div>
<?php } else { foreach($avaliacoes as $avaliacao){ // Início do foreach ?>
                            <div class="row">
                                <div class="col-md-8">
                                    <p class="text-muted mb-2"><?= converter_data($avaliacao->data) ?></p>
                                    <h5 class="font-weight-bold"><?= $avaliacao->titulo ?></h5>
                                    <p><?= $avaliacao->comentario ?></p>
                                    <blockquote class="blockquote-footer"><?= $avaliacao->nome_usuario ?> <?= $avaliacao->sobrenome_usuario ?></blockquote>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col text-center">
<?php if ($avaliacao->avaliacao == 0) { // Início das condicionais de estrelas ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 1) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 2) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 3) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 4) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 5) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                        </ul>
<?php } // Fim das condicionais de estrelas ?>
                                        </div>
                                    </div>
<!-- 
                                    <div class="row">
                                        <div class="col text-center">
                                            <h5>Esse comentário foi útil?</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 h3 mx-auto text-center text-sm-right">
                                            <a href="#">
                                                <i class="fas fa-thumbs-up text-ipci-primary"></i>
                                            </a>
                                            <span class="h6">(13)</span>
                                        </div>
                                        <div class="col-6 h3 mx-auto text-center text-sm-left">
                                            <a href="#">
                                                <i class="fas fa-thumbs-down text-ipci-secondary"></i>
                                            </a>
                                            <span class="h6">(0)</span>
                                        </div>
                                    </div>
                                     -->
                                </div>
                            </div>
                            
                            <hr>
<?php } // Fim do foreach ?>

<?php if($qtd_avaliacoes > 4){ ?>
                            <div class="row">
                                <div class="col-md-6 mx-auto text-center">
                                    <button id="carregar_avaliacoes" class="btn btn-success">
                                        Carregar todas...
                                    </button>
                                </div>
                            </div>
<?php } ?>

<?php } // Fim da condicional ?>
                        </div>
                        <!-- /Card body -->
                    </div>
                    <!-- Card de avaliações -->

                </div>
            </div>

        </div>
