<?php if(isset($destaques[0])){ ?>
		<div class="container">
			<div class="row mt-4">
				<div class="col mx-auto text-center">
<?php if(isset($filtro)) { ?>
<?php if($filtro == 'categoria') { ?>
					<h2>Produtos em destaque em <?= $destaques[0]->categoria ?> de <?= $destaques[0]->departamento ?></h2>
<?php } else if($filtro == 'subcategoria'){ ?>
					<h2>Produtos em destaque em <?= $destaques[0]->subcategoria ?></h2>
<?php } ?>
<?php } else { ?>
					<h2>Produtos em destaque</h2>
<?php } ?>
				</div>
			</div>

			<!-- Responsivo LG -->
			<div class="row d-none d-lg-block ">
				<div class="col-12 mx-auto" >
					<div id="carrosel-destaques" class="carousel slide" data-ride="carousel" data-interval="0">
						<!-- Carrosel Inner -->
						<div class="carousel-inner" >
<?php 
if(count($destaques) < 5){
	$c1 = 1;
} else if(count($destaques) < 9){
	$c1 = 2;
} else if(count($destaques) < 13){
	$c1 = 3;
}
$qtd = 1; for($i_destaques = 0; $i_destaques < $c1; $i_destaques++) { ?>
							<!-- Item do Carrosel -->
							<div class="carousel-item <?= ($i_destaques == 0) ? 'active' : '' ?>">
								<div class="row mt-4 px-1">           
<?php for ($cc1=0; $cc1 < ((count($destaques) > 4) ? 4 : count($destaques)); $cc1++) { if(!isset($destaques[$qtd-1]->id)) break; ?>
									<div class="col-lg-3 col-sm-6 mb-4 d-flex">
										<div class="card">
											<div class="view view-cascade p-3 p-md-5">
												<img class="card-img-top img-fluid" src="<?= ($destaques[$qtd-1]->img) ? base_url('uploads/produtos/'.md5($destaques[$qtd-1]->id)).'/'.$destaques[$qtd-1]->img : base_url('uploads/semimagem.jpg') ?>" alt="<?= $destaques[$qtd-1]->nome ?>">
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome))?>">
													<div class="mask"></div>
												</a>
											</div>
											<div class="view view-cascade overlay card-body card-body-cascade">
												<h6 class="card-title my-0"><strong><?= $destaques[$qtd-1]->nome?></strong></h6>
												<h6 class="font-weight-bold text-muted my-2"><?= $destaques[$qtd-1]->departamento ?></h6>
<?php if($destaques[$qtd-1]->promocao){ ?>
												<p class="mb-0 text-muted">De: <s class="text-ipci-secondary">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda)?></s> </p>
												<p class="text-muted mb-0">Por: <span class="h4 font-weight-normal text-dark">R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional) ?></span></p>
<?php if($destaques[$qtd-1]->preco_promocional > 100){ ?>
												<h4 class="card-text mb-0 mt-2">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional / 2)?> s/ juros.</h4>
<?php } ?>
<?php } else { ?>
												<h4 class="h4 mb-0">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda) ?></h4>
<?php if($destaques[$qtd-1]->preco_venda > 100){ ?>
												<h4 class="card-text mb-0">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_venda / 2)?> s/ juros.</h4>
<?php } ?>
<?php } ?>
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>">
													<div class="mask rgba-white-slight"></div>
												</a>
											</div>
											<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome))?>" class="ml-4 mb-4 h4 text-ipci-primary mt-0">Ver mais</a>

										</div>
									</div>
<?php $qtd++; } ?>					
								</div>
							</div>
							<!-- /Item do Carrosel -->
<?php } ?>					
						</div>
						<!--  /Carrosel Inner -->

<?php if($c1 > 1) { ?>
						<!-- Controles -->
						<a href="#carrosel-destaques" class="carousel-control-card carousel-control-prev carousel-control-prev-card rounded" data-slide="prev">
							<span class="carousel-control-prev-icon w-100 h-100" ></span>
						</a>
						<a href="#carrosel-destaques" class="carousel-control-card carousel-control-next carousel-control-next-card  rounded" data-slide="next">
							<span class="carousel-control-next-icon w-100 h-100"></span>
						</a>
						<!-- /Controles -->
<?php } ?>

					</div>
					<!-- /Carrosel -->
				</div>
			</div>

			<!-- Responsivo MD -->	
			<div class="row d-none d-md-block d-lg-none">
				<div class="col-12 mx-auto" >
					<div id="carrosel-destaques-responsivo" class="carousel slide" data-ride="carousel" data-interval="0">
						<!-- Carrosel Inner -->
						<div class="carousel-inner" >
						
<?php 
if(count($destaques) < 3){
	$c2 = 1;
} else if(count($destaques) < 5){
	$c2 = 2;
} else if(count($destaques) < 7){
	$c2 = 3;
} else if(count($destaques) < 9){
	$c2 = 4;
} else if(count($destaques) < 11){
	$c2 = 5;
} else if(count($destaques) < 13){
	$c2 = 6;
}
$qtd = 1; for($i_destaques = 0; $i_destaques < $c2; $i_destaques++) { ?>
							<!-- Item do Carrosel -->
							<div class="carousel-item <?= ($i_destaques == 0) ? 'active' : '' ?>">
								<div class="row mt-4 px-1">  
<?php for ($cc2=0; $cc2 < ((count($destaques) > 2) ? 2 : count($destaques)); $cc2++) { if(!isset($destaques[$qtd-1]->id)) break;  ?>
									<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-4 mx-auto">
										<div class="card">
											<div class="view view-cascade p-3 p-md-5">
												<img class="card-img-top img-fluid w-100 h-100" src="<?= ($destaques[$qtd-1]->img) ? base_url('uploads/produtos/'.md5($destaques[$qtd-1]->id)).'/'.$destaques[$qtd-1]->img : base_url('uploads/semimagem.jpg') ?>" alt="<?= $destaques[$qtd-1]->nome ?>">
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>">
													<div class="mask"></div>
												</a>
											</div>
											<div class="view view-cascade overlay card-body card-body-cascade">
												<h5 class="card-title my-0"><strong><?= $destaques[$qtd-1]->nome?></strong></h5>
												<h6 class="font-weight-bold text-muted my-2"><?= $destaques[$qtd-1]->departamento?></h6>
<?php if($destaques[$qtd-1]->promocao){ ?>
												<p class="mb-0 text-muted">De: <s class="text-ipci-secondary">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda)?></s> </p>
												<p class="text-muted mb-0">Por: <span class="h4 font-weight-normal text-dark">R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional) ?></span></p>
<?php if($destaques[$qtd-1]->preco_promocional > 100){ ?>
												<h4 class="card-text mb-0 mt-2">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional / 2)?> s/ juros.</h4>
<?php } ?>
<?php } else { ?>
												<h4 class="h4 mb-0">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda) ?></h4>
<?php if($destaques[$qtd-1]->preco_venda > 100){ ?>
												<h4 class="card-text mb-0">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_venda / 2)?> s/ juros.</h4>
<?php } ?>
<?php } ?>
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>">
													<div class="mask rgba-white-slight"></div>
												</a>
											</div>
											<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>" class="ml-4 mb-4 h4 text-ipci-primary">Ver mais</a>

										</div>
									</div>
<?php  $qtd++; } ?>
								</div>
							</div>
							<!-- /Item do Carrosel -->
<?php } ?>
						</div>
						<!--  /Carrosel Inner -->
						

<?php if($c2 > 1) { ?>
						<!-- Controles -->
						<a href="#carrosel-destaques-responsivo" class="carousel-control-card carousel-control-prev carousel-control-prev-card rounded" data-slide="prev">
							<span class="carousel-control-prev-icon w-100 h-100" ></span>
						</a>
						<a href="#carrosel-destaques-responsivo" class="carousel-control-card carousel-control-next carousel-control-next-card  rounded" data-slide="next">
							<span class="carousel-control-next-icon w-100 h-100"></span>
						</a>
						<!-- /Controles -->
<?php } ?>

					</div>
					<!-- /Carrosel -->
				</div>
			</div>

			<!-- Responsivo SM -->	
			<div class="row d-block d-md-none">
				<div class="col-12 mx-auto" >
					<div id="carrosel-destaques-responsivo-sm" class="carousel slide" data-ride="carousel" data-interval="0">
						<!-- Carrosel Inner -->
						<div class="carousel-inner" >
<?php 
if(count($destaques) < 2){
	$c3 = 1;
} else if(count($destaques) < 3){
	$c3 = 2;
} else if(count($destaques) < 4){
	$c3 = 3;
} else if(count($destaques) < 5){
	$c3 = 4;
} else if(count($destaques) < 6){
	$c3 = 5;
} else if(count($destaques) < 7){
	$c3 = 6;
} else if(count($destaques) < 8){
	$c3 = 7;
} else if(count($destaques) < 9){
	$c3 = 8;
} else if(count($destaques) < 10){
	$c3 = 9;
} else if(count($destaques) < 11){
	$c3 = 10;
} else if(count($destaques) < 12){
	$c3 = 11;
} else if(count($destaques) < 13){
	$c3 = 12;
}
$qtd = 1; for($i_destaques = 0; $i_destaques < $c3; $i_destaques++) { ?>
							<!-- Item do Carrosel -->
							<div class="carousel-item <?= ($i_destaques == 0) ? 'active' : '' ?>">
								<div class="row my-4 px-1">  
<?php for ($cc3=0; $cc3 < ((count($destaques) > 1) ? 1 : count($destaques)); $cc3++) { if(!isset($destaques[$qtd-1]->id)) break; ?>
									<div class="col-xl-3 col-lg-4 col-md-6 col-sm-10 mb-4 mx-auto">
										<div class="card">
											<div class="view view-cascade p-5 p-md-5 mx-auto text-center">
												<img class="card-img-top img-fluid w-75 h-75" src="<?= ($destaques[$qtd-1]->img) ? base_url('uploads/produtos/'.md5($destaques[$qtd-1]->id)).'/'.$destaques[$qtd-1]->img : base_url('uploads/semimagem.jpg') ?>" alt="<?= $destaques[$qtd-1]->nome ?>">
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>">
													<div class="mask"></div>
												</a>
											</div>
											<div class="view view-cascade overlay card-body card-body-cascade">
												<h5 class="card-title my-0"><strong><?= $destaques[$qtd-1]->nome?></strong></h5>
												<h6 class="font-weight-bold text-muted my-2"><?= $destaques[$qtd-1]->departamento?></h6>
<?php if($destaques[$qtd-1]->promocao){ ?>
												<p class="mb-0 text-muted">De: <s class="text-ipci-secondary">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda)?></s> </p>
												<p class="text-muted mb-0">Por: <span class="h4 font-weight-normal text-dark">R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional) ?></span></p>
<?php if($destaques[$qtd-1]->preco_promocional > 100){ ?>
												<h4 class="card-text mb-0 mt-2">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_promocional / 2)?> s/ juros.</h4>
<?php } ?>
<?php } else { ?>
												<h4 class="h4 mb-0">R$<?= transforma_preco($destaques[$qtd-1]->preco_venda) ?></h4>
<?php if($destaques[$qtd-1]->preco_venda > 100){ ?>
												<h4 class="card-text mb-0">2x de R$<?= transforma_preco($destaques[$qtd-1]->preco_venda / 2)?> s/ juros.</h4>
<?php } ?>
<?php } ?>
												<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>">
													<div class="mask rgba-white-slight"></div>
												</a>
											</div>
											<a href="<?= base_url('produto/'.$destaques[$qtd-1]->id.'/'.url_amigavel($destaques[$qtd-1]->nome)) ?>" class="ml-4 mb-4 h4 text-ipci-primary">Ver mais</a>

										</div>
									</div>
<?php $qtd++; } ?>
								</div>
							</div>
							<!-- /Item do Carrosel -->
<?php } ?>
						</div>
						<!--  /Carrosel Inner -->
						

<?php if($c3 > 1) { ?>
						<!-- Controles -->
						<a href="#carrosel-destaques-responsivo-sm" class="carousel-control-card carousel-control-prev carousel-control-prev-card rounded" data-slide="prev">
							<span class="carousel-control-prev-icon w-100 h-100" ></span>
						</a>
						<a href="#carrosel-destaques-responsivo-sm" class="carousel-control-card carousel-control-next carousel-control-next-card  rounded" data-slide="next">
							<span class="carousel-control-next-icon w-100 h-100"></span>
						</a>
						<!-- /Controles -->
<?php } ?>

					</div>
					<!-- /Carrosel -->
				</div>
			</div>
		</div>

<?php } else { ?>
	<script type="text/javascript">
		console.log('Não há produtos em destaque.');
	</script>
<?php } ?>