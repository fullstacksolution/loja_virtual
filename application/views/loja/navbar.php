		<!-- Navbar principal -->
		<nav class="navbar fixed-top d-block navbar-expand-lg navbar-light white shadow-none justify-content-between">
			<div class="container">

				<a class="navbar-brand" title="Página principal" href="<?= base_url('loja') ?>" >
					<h2 class="text-hide my-auto">
						IPCI
						<img class="img-fluid img-hover" src="<?= base_url('assets/img/logotipo_ipci.png')?>" alt="IPCI">
					</h2>
				</a>

				<button id="show_busca_btn" class="navbar-toggler text-ipci-nocolor white rounded waves-effect p-2 border-0 text-ipci-nocolor" type="submit" title="Buscar">
					<a class=""><i class="fas fa-search mx-0"></i></a>
				</button>	
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-dropdown" aria-controls="navbar-dropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbar-dropdown">
					<div class="form-inline mx-auto">
						<div class="md-form my-0 mx-auto row d-lg-inline d-none" id="div_show_busca">
							<input id="show_busca" class="col-md-12 form-control mr-sm-2" type="text" maxlength="20">
							<label for="show_busca" id="label_show_busca">Digite o que procura</label>
<!-- 
							<button id="btn_busca2" class="text-ipci-nocolor col-md-2 white border border-light rounded waves-effect p-2" type="submit" title="Buscar">
								<i class="fas fa-search mx-0"></i>
							</button>								 -->
						</div>
					</div>

					<!--
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link waves-effect" href="#" target="_blank">Botão 1</a>
						</li>
						<li class="nav-item">
							<a class="nav-link waves-effect" href="#" target="_blank">Botão 2</a>
						</li>
					</ul> 
					 -->

					<!-- Right -->
					<ul class="navbar-nav nav-flex-pills text-right mt-2 mt-lg-0">
<?php if($this->uri->segment(1) != 'carrinho'){ ?>

<?php if($this->session->userdata('logado')){ ?>

						<li class="nav-item mx-auto mx-md-0 px-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('carrinho')?>" class="nav-link text-ipci-nocolor border border-light rounded waves-effect" title="Carrinho de compras">
								<i class="fas fa-shopping-cart"></i>
								<span id="navbar_qtd_produtos" class="clearfix badge green mr-1"><?= $carrinho->qtd_produtos ?></span>
								<span class="clearfix text-hide">Carrinho</span>
							</a>
						</li>

<?php } else { ?>

						<li class="nav-item mx-auto mx-md-0 px-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('sign/in?status=page_carrinho')?>" class="nav-link text-ipci-nocolor border border-light rounded waves-effect" title="Carrinho de compras">
								<i class="fas fa-shopping-cart"></i>
								<span class="clearfix text-hide">Carrinho</span>
							</a>
						</li>

<?php } ?>
<?php } ?>

						<span class="mx-1 d-none d-sm-block"></span>

<?php if(!$this->session->userdata('logado')) { ?>
						<li class="nav-item mx-auto mx-md-0 px-0 mt-2 mt-lg-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('sign/in')?>" class="nav-link text-ipci-nocolor border border-light rounded waves-effect" title="Entrar ou cadastrar">
								<i class="fas fa-sign-in-alt mr-2"></i>Entrar
							</a>
						</li>
<?php } else if($this->session->userdata('userlogado')->tipo >= 2) { ?>
						<li class="nav-item mx-auto mx-md-0 px-0 mt-2 mt-lg-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('admin')?>" class="nav-link text-ipci-nocolor border border-light rounded waves-effect" title="Painel administrativo">
								<i class="fas fa-user-lock mr-2"></i>Admin
							</a>
						</li>
						<span class="mx-1 d-none d-sm-block"></span>
<?php } else { if($this->uri->segment(1) != 'perfil') { ?>
						<li class="nav-item mx-auto mx-md-0 px-0 mt-2 mt-lg-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('perfil')?>" class="nav-link text-ipci-nocolor border border-light rounded waves-effect" title="Perfil">
								<i class="fas fa-user mr-2"></i>Perfil
							</a>
						</li>
						<span class="mx-1 d-none d-sm-block"></span>
<?php } } ?>

<?php if($this->session->userdata('logado')) { ?>
						<li class="nav-item mx-auto mx-md-0 px-0 mt-2 mt-lg-0 col-12 col-lg-auto text-center">
							<a href="<?= base_url('sign/out')?>" class="nav-link text-ipci-nocolor-secondary border border-light rounded waves-effect" title="Sair">
								<i class="fas fa-sign-out-alt mr-2"></i>Sair
							</a>
						</li>
<?php } ?>
					</ul>

				</div>
			</div>
		</nav>
		<!-- /Navbar principal -->
		
		<!-- Modal -->
		<div class="modal fade top p-0" id="modal_busca" tabindex="-1" role="dialog" aria-labelledby="modal_busca_label" aria-hidden="true">
			<div class="modal-dialog modal-frame modal-top" role="document">
				<div class="modal-content bg-dark-50">
					<div class="modal-body py-5">

						<div class="row">
							<div class="col-lg-4 mx-auto">
								<div class="md-form text-white my-0 mx-auto row">
									<input id="busca" class="col-12 form-control form-control-lg text-white" type="text" maxlength="40">
									<label for="busca" class="text-white">Digite o que procura</label>							
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-4 mx-auto">
								<button id="btn_busca" class="text-ipci-nocolor col-12 bg-transparent rounded waves-effect p-2 border-0 text-ipci-nocolor-w" type="submit" title="Buscar">
									<a class=""><i class="fas fa-search fa-2x mx-0 "></i></a>
								</button>	
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->