        <!-- Conteúdo da página -->
        <div class="container">
            <div class="row mt-4">
                <div class="col text-center mx-auto">
                    <h1><?= $titulo ?></h1>
                </div>
            </div>
            <div class="row mt-4">
                
<?php if($pagina == 'subcategoria' || $pagina == 'busca') { ?>
                <!-- Barra lateral (menu) de filtros -->
                <div class="col-xl-2 col-lg-3 col-md-12 d-none d-md-block"> <!-- col-md-6 d-none d-md-block -->
                    <div class="">

                        <!-- Grid row -->
                        <div class="row">

                            <div class="col-md-6 col-lg-12 mb-5">
                                <!-- Ordenação -->
                                <h5 class="font-weight-bold dark-grey-text"><strong>Ordenar por</strong></h5>
                                <hr class="border-0 grey lighten-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 25%; height: 5px;">

                                <p><a class="text-ipci-nocolor" href="<?= $url_filtro ?>">Padrão</a></p>
                                <p><a class="text-ipci-nocolor">Popularidade</a></p>
                                <p><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_desc' ?>">Melhor avaliação</a></p>
                                <p><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=preco_asc' ?>">Preço: Menor</a></p>
                                <p><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=preco_desc' ?>">Preço: Maior</a></p>
                            </div>

                            <div class="col-md-6 col-lg-12 mb-5">
                                <h5 class="font-weight-bold dark-grey-text"><strong>Avaliação</strong></h3>
                                <hr class="border-0 grey lighten-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 25%; height: 5px;">
                                
                                <!-- 5 estrelas -->
                                <div class="row ml-1">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item text-right">
                                            <p class="ml-3"><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_5' ?>">5 estrelas</a></p>
                                        </li>
                                    </ul>
                                </div>

                                <!-- 4 estrelas -->
                                <div class="row ml-1">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item text-right">
                                            <p class="ml-3"><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_4' ?>">4 estrelas</a></p>
                                        </li>
                                    </ul>
                                </div>

                                <!-- 3 estrelas -->
                                <div class="row ml-1">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item text-right">
                                            <p class="ml-3"><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_3' ?>">3 estrelas</a></p>
                                        </li>
                                    </ul>
                                </div>

                                <!-- 2 estrelas -->
                                <div class="row ml-1">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item text-right">
                                            <p class="ml-3"><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_2' ?>">2 estrelas</a></p>
                                        </li>
                                    </ul>
                                </div>

                                <!-- 1 estrela -->
                                <div class="row ml-1">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item"><i class="fas fa-star text-ipci-primary"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item"><i class="fas fa-star grey-text"></i></li>
                                        <li class="list-inline-item text-right">
                                            <p class="ml-3"><a class="text-ipci-nocolor" href="<?= $url_filtro.'&filtro=avaliacao_1' ?>">1 estrela</a></p>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <!-- grid row -->
<?php if($pagina == 'subcategoria') { ?>
                        <div class="row">
                            
                            <!-- Filter by category-->
                            <div class="col-md-6 col-lg-12 mb-5">
                                <h5 class="font-weight-bold dark-grey-text"><strong>Subcategorias em <?=$categoria->nome?> de <?= $departamento->nome ?></strong></h5>
                                <hr class="border-0 grey lighten-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 25%; height: 5px;">
                                <ul class="list-group mb-0">
<?php foreach($subcategorias as $sub){ ?>
                                    <li class="list-group-item">                                  
                                        <a href="<?= base_url('categoria/'.url_amigavel($departamento->nome).'/'.url_amigavel($categoria->nome).'/'.url_amigavel($sub->nome) )?>"><?=$sub->nome?></a>
                                    </li>
<?php } ?>
                                </ul>
                            </div>
                            <!-- /Filter by category-->
                        </div>
                        <!-- /Grid row -->
<?php } ?>
                    </div>
                </div>
<?php } ?>
