        <div class="container">

            <div class="row mt-4">
                <div class="col text-center">
                    <h1>Produto</h1>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">

                    <!-- Card do produto -->
                    <div class="card shopping-cart">
                        <div class="card-body p-4">

                            <!-- Galeria de imagens -->
                            <div class="row">
                                <!-- Galeria para celulares -->
                                <div class="col-lg-7 d-md-none">
                                    <div id="carrosel-galeria-sm" class="carousel slide" data-ride="carousel" data-interval="0">
<?php if($produto->qtd_imagens > 0){ ?>                                        
                                        <!-- Indicadores -->
                                        <ol class="carousel-indicators" style="filter: invert(1);">

<?php for($qtd_imagens = 0; $qtd_imagens < $produto->qtd_imagens; ++$qtd_imagens) { ?>
                                            <li data-target="#carrosel-galeria-sm" data-slide-to="<?=$qtd_imagens?>" class="<?=($qtd_imagens == 0) ? 'active' : ''?> mx-1" style="width: 15px; height: 15px"></li>
<?php } ?>
                                        </ol>
<?php } ?>

                                        <!-- Carrosel Inner -->
                                        <div class="carousel-inner" >
<?php if($produto->qtd_imagens > 0){ ?> 
<?php foreach($imagens as $key => $imagem) { ?>
                                            <!-- Item do Carrosel -->
                                            <div class="carousel-item <?= ($key == 0) ? 'active' : '' ?>">
                                                <div class="row my-4 px-1">  
                                                    <div class="col mb-4 mx-auto">
                                                        <div class="view view-cascade p-1 p-sm-5">
                                                            <img class="card-img-top img-fluid w-100 h-100" src="<?= base_url('uploads/produtos/'.md5($produto->id).'/'.$imagem->img)?>" alt="<?=$produto->nome?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Item do Carrosel -->
<?php } ?>
<?php } else { ?>
                                            <div class="carousel-item active">
                                                <div class="row my-4 px-1">  
                                                    <div class="col mb-4 mx-auto text-center">
                                                        <img class="card-img-top img-fluid w-50" src="<?=base_url('uploads/semimagem.jpg')?>" alt="Sem imagem" >
                                                    </div>
                                                </div>
                                            </div>
<?php } ?>
                                        </div>
                                        <!--  /Carrosel Inner -->
                                        
<?php if($produto->qtd_imagens > 1){ ?> 
                                        <!-- Controles -->
                                        <a href="#carrosel-galeria-sm" class="carousel-control-card carousel-control-prev carousel-control-prev-card rounded" data-slide="prev">
                                            <span class="carousel-control-prev-icon w-100 h-100" ></span>
                                        </a>
                                        <a href="#carrosel-galeria-sm" class="carousel-control-card carousel-control-next carousel-control-next-card  rounded" data-slide="next">
                                            <span class="carousel-control-next-icon w-100 h-100"></span>
                                        </a>
                                        <!-- /Controles -->
<?php } ?>

                                    </div>
                                    <!-- /Carrosel -->
                                </div>
                                <!-- /Galeria para celulares -->
                                
                                <!-- Galeria para computadores -->
                                <div class="col-lg-7 d-none d-md-block">
                                    <div class="row">
                                        <div class="cover col mx-auto text-center p-3 p-sm-4 p-md-5">
<?php if($produto->qtd_imagens > 0){ ?> 
                                            <img class="img-fluid w-75" src="<?=base_url('uploads/produtos/'.md5($produto->id).'/'.$imagens[0]->img)?>" alt="<?= $produto->nome ?>">
<?php } else { ?>
                                            <img src="<?=base_url('uploads/semimagem.jpg')?>" alt="Sem imagem" class="img-fluid w-50">
<?php } ?>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-12 mx-auto text-center view">
                                            <div id="carrosel-galeria" class="carousel slide" data-ride="carousel" data-interval="0">
                                                
                                                <div class="row">

                                                    <div class="col mx-auto">
                                                        <!-- Carrosel Inner -->
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item thumbs active">
<?php foreach($imagens as $key => $imagem){ ?>
                                                                <img src="<?=base_url('uploads/produtos/'.md5($produto->id).'/'.$imagem->img)?>" alt="<?= $produto->nome ?>" class="img-fluid p-3 border rounded <?= ($key == 0) ? 'active-img' : ''?>">
<?php } ?>
                                                            </div>


                                                        </div>
                                                        <!-- /Carrosel Inner -->
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="ipci-border mb-4">

                                </div>
                                <!-- /Galeria para computadores -->

                                <!-- Descrição principal -->
                                <div class="col-lg-5 mt-4 mt-lg-0">
                                    <h2 class="h2"><?= $produto->nome ?></h2>
                                    <h5 class="text-muted">
                                        <span class="badge bg-dark"><?= $produto->departamento?></span> >
                                        <span class="badge bg-ipci-primary-dark"><?= $produto->categoria?></span> >
                                        <span class="badge bg-ipci-primary"><?= $produto->subcategoria?></span>                                        
                                    </h5>
                                    <div class="row">
                                        <div class="col font-small">
                                            <ul class="list-inline mb-0">
<?php if ($media_avaliacao == 0) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 2) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 3) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 4) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao < 5) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>
<?php } else if ($media_avaliacao == 5) { ?>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                <li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>
<?php } ?>
                                                <li class="list-inline-item text-right">
                                                    <p class="ml-3">
                                                        <a class="text-ipci-nocolor" id="redirect_avaliacao" href="#avaliacoes"><?= ($qtd_avaliacoes > 1) ? $qtd_avaliacoes.' Avaliações' : $qtd_avaliacoes.' avaliação'?></a>
                                                    </p>
                                                </li>
                                                <li class="list-inline-item text-right  ">
                                                    <div class="fb-share-button" data-href="<?= base_url($_SERVER["REQUEST_URI"]) ?>" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.ipci.com.br%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                    <p class="text-muted text-sm">Frete e custos adicionais são baseados nos valores que você utilizar.</p>
<?php if($produto->quantidade){ ?>
                                    <div class="row mt-4">
                                        <div class="col">
<?php if($produto->promocao){ ?>
                                            <h5 class="text-muted">De:</h5><h3 class="font-weight-light text-ipci-secondary"><s>R$ <?= transforma_preco($produto->preco_venda) ?></s></h3>
                                            <h5 class="text-muted">Por:</h5><h1 class="h1 font-weight-bold text-ipci-primary">R$ <?= transforma_preco($produto->preco_promocional) ?></h1>
<?php } else { ?>
                                            <h5 class="text-muted">Por:</h5><h1 class="h1 font-weight-bold text-ipci-primary">R$ <?= transforma_preco($produto->preco_venda) ?></h1>
<?php } ?>
<?php if($produto->preco_venda > 100 && !$produto->promocao){ ?>
                                            <p class="text-muted text-sm">2x de R$ <?= transforma_preco($produto->preco_venda / 2)?> sem juros</p>
<?php } else if($produto->preco_promocional > 100 && $produto->promocao){ ?>
                                            <p class="text-muted text-sm">2x de R$ <?= transforma_preco($produto->preco_promocional / 2)?> sem juros</p>
<?php } ?>
                                        </div>
                                    </div>
                                    
                                    <!-- Formulário de adicionar ao carrinho -->
                                    <form id="add_carrinho">
<?php if(!$no_carrinho) { ?>
                                        <div class="row mt-4">
                                            <div class="col text-left float-right align-self-center">
                                                <p class="text-muted text-sm mb-0">Quantidade (Disponível: <?= $produto->quantidade ?>)</p>
                                                <div class="def-number-input number-input safari_only my-0">
                                                    <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus px-3 py-1 "></button>
                                                    <input type="number" class="font-weight-bold py-1" min="1" max="<?=$produto->quantidade?>" name="quantidade" id="quantidade" value="1"  <?= ($produto->quantidade <= 10) ? 'readonly' : '' ?>>
                                                    <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus px-3 py-1"></button>
                                                </div>
                                            </div> 
                                        </div>
<?php } ?>
<!-- 
                                        <div class="row mt-4">
                                            <div class="col">
                                                <h5>Frete</h5>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col">
                                                <div class="md-form mt-0">
                                                    <input type="text" class="form-control" size="15" maxlength="9" id="cep" name="cep" >
                                                    <label for="cep">Calcule o frete</label>
                                                </div>
                                            </div>
                                            <div class="col align-self-center">
                                                <a class="text-ipci-primary font-small" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu cep</a>
                                            </div>
                                        </div> -->
                                        <div class="row mt-4">
                                            <input type="hidden" name="request" id="request" value="add_carrinho">
                                            <input type="hidden" name="id_produto" id="id_produto" value="<?=$produto->id?>">
<?php if($no_carrinho) { ?>
                                            <a href="<?= base_url('carrinho') ?>" class="btn btn-outline-primary font-weight-bold btn-block btn-lg">No carrinho <i class="fas fa-angle-double-right"></i></a>
<?php } else { ?>
                                            <button id="btn_add_carrinho" type="submit" class="btn btn-primary btn-block btn-lg">Adicionar ao carrinho</button>
<?php } ?>
                                        </div>
                                    </form>
<?php } else { ?>                                    
                                    <div class="row">
                                        <div class="col">
                                            <h3 class="font-weight-light text-ipci-secondary">Produto indisponível no momento</h3>
                                        </div>
                                    </div>
<?php } ?>
                                </div>

                            </div>
                            
                            <hr>
                            <!-- Descrição detalhada -->
                            <div class="row mt-4">
                                <div class="col mx-auto">
                                    <h3>Especificações técnicas</h3>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-12 mx-0">
                                    <div class="form-group">
                                        <label for="desc">Descrição do Produto</label> <br>
                                        <textarea data-autoresize id="desc" class="form-control bg-white" rows="3" readonly><?=$produto->descricao?></textarea>                                  
                                    </div>
                                </div>
                                <div class="col-md-6 mx-0">
                                    <ul class="list-group">
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Categoria:</strong> <br><h5 class="text-muted d-inline">
                                                <span class="badge bg-dark"><?= $produto->departamento?></span> >
                                                <span class="badge bg-ipci-primary-dark"><?= $produto->categoria?></span> >
                                                <span class="badge bg-ipci-primary"><?= $produto->subcategoria?></span>                
                                            </h5>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Código:</strong> <?= $produto->codigo ?>-<?= $produto->id ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Marca:</strong> <?= ($produto->marca) ? $produto->marca : 'Não especificada' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Modelo:</strong> <?= ($produto->modelo) ? $produto->modelo : 'Não especificado' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Unidade:</strong> <?= ($produto->unidade) ? $produto->unidade : 'Não especificada' ?>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 mx-0">
                                    <ul class="list-group">
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Caixa Master:</strong> <?= ($produto->caixa) ? $produto->caixa : 'Não especificado' ?>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Garantia:</strong> <?= ($produto->garantia) ? (($produto->garantia > 1) ? $produto->garantia.' meses' : $produto->garantia.' mês') : 'Sem garantia' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Peso (kg):</strong> <?= ($produto->peso_kg) ? $produto->peso_kg : 'Não especificado' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Altura (cm):</strong> <?= ($produto->altura_cm) ? $produto->altura_cm : 'Não especificado' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Largura (cm):</strong> <?= ($produto->largura_cm) ? $produto->largura_cm : 'Não especificado' ?>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong>Profundidade (cm):</strong> <?= ($produto->profundidade_cm) ? $produto->profundidade_cm : 'Não especificado' ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
