
		    	<div class="row">
		    		<div class="col">
						
						<form>
							<div class="form-group row">
								<label class="col-sm-9 col-form-label">Competência:</label>
								<div class="col-sm-2">
									<select class="form-control-plaintext">
										<option value="">-- Selecione </option>
										<option value="2018-08">Agosto / 2018</option>
										<option value="2018-09">Setembro / 2018</option>
										<option value="2018-10">Outubro / 2018</option>
									</select>
								</div>
							</div>
						</form>

						<hr />

		    		</div>
		    	</div>
		    	
		    	<div class="row ">
		    		<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Número de vendas
							</div>
							<div class="card-body">
								 <h5 class="card-title">?</h5>
							</div>
						</div>
					</div>

					<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Total de vendas
							</div>
							<div class="card-body">
								 <h5 class="card-title">?</h5>
							</div>
						</div>
		    		</div>

				</div>

		    	<div class="row ">
					<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Total de reclamações
							</div>
							<div class="card-body">
								 <h5 class="card-title">?</h5>
							</div>
						</div>
					</div>
					
					<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header ">
								Total de elogios
							</div>
							<div class="card-body">
								 <h5 class="card-title">?</h5>
							</div>
						</div>
		    		</div>

		    		<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Total de sugestões
							</div>
							<div class="card-body">
								 <h5 class="card-title">?</h5>
							</div>
						</div>
		    		</div>
		    	</div>

				<hr>

				<div class="row ">
					<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Usuários ativos 
							</div>
							<div class="card-body">
								 <h5 class="card-title" id="usuarios_ativos">?</h5>
							</div>
						</div>
					</div>

					<div class="col-md-4 my-2">
		    			<div class="card">
							<div class="card-header">
								Usuários inativos
							</div>
							<div class="card-body">
								 <h5 class="card-title" id="usuarios_inativos">?</h5>
							</div>
						</div>
		    		</div>
		    	</div>

				<script type="text/javascript">
					$(document).ready(function () {
						// Obtém a data/hora atual
						var data = new Date();

						// Guarda cada pedaço em uma variável
						var hora    = data.getHours();          // 0-23
						var min     = data.getMinutes();        // 0-59

						var str_hora = hora + 'h' + min + 'min'

						$.ajax({
							type: "post",
							url: "<?= base_url('admin/usuario/ativos') ?>",
							data: "request=admin",
							dataType: "json",
							success: data => {
								$('#usuarios_ativos').html(data.usuarios_ativos)
								$('#usuarios_ativos').parent().parent().find('.card-header').append('<span class="font-small text-muted">(Atualizado às ' + str_hora + ')</span>')
								
							},
							error: erro => {
								console.log('Erro ao encontrar usuários ativos.');
								console.log(erro);								
							}
						});
						$.ajax({
							type: "post",
							url: "<?= base_url('admin/usuario/inativos') ?>",
							data: "request=admin",
							dataType: "json",
							success: data => {
								$('#usuarios_inativos').html(data.usuarios_inativos)
								$('#usuarios_inativos').parent().parent().find('.card-header').append('<span class="font-small text-muted">(Atualizado às ' + str_hora + ')</span>')
							},
							error: erro => {
								console.log('Erro ao encontrar usuários inativos.');
								console.log(erro);								
							}
						});
					});
				</script>