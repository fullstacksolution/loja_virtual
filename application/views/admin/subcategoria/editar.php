<div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-6 mt-4 text-center mx-auto">

                <!-- Card das informações do usuário -->
                <div class="card">
                    <div class="card-body p-4">
            
                        <form id="infosubcategoria">
                        
                            <div class="row my-3">

                                <div class="col text-center">
                                    <h4>Informações da Subcategoria</h4>
                                </div>

                            </div>

                            <div class="form-row">
                                <!-- Nome  -->
                                <div class="col-md-12 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" id="nome" name="nome" maxlength="30" value="<?= $subcategoria->nome ?>" class="form-control">
                                        <label class="active"  for="nome">Nome da Subcategoria</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <!-- Categoria -->
                                <div class="col mx-auto">
                                    <label for="id_categoria">Categoria</label>
                                    <input type="hidden" name="categoria" id="categoria" value="">
                                    <select class="form-control mb-3" name="id_categoria" id="id_categoria" >
                                        <option value="0" disabled>-- Selecione um categoria --</option>
<?php foreach($categorias as $categoria) { ?>
                                        <option value="<?= $categoria->id ?>" <?= ($subcategoria->id_categoria == $categoria->id) ? 'selected' : ''?>><?= $categoria->nome ?> (de <?=$categoria->departamento?>)</option>
<?php } ?>
                                    </select>
                                </div>

                            </div>

                            <input type="hidden" name="id" id="id1" value="<?= $subcategoria->id ?>">
                            <input type="hidden" name="request" id="request" value="edit">

                            <button class="btn btn-primary" type="submit">Salvar</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

        <script type="text/javascript">
        $(document).ready(function () {

            $('select').selectize({
                sortField: 'text'
            });

            $('#infosubcategoria').submit(e => {
                e.preventDefault()

                let dados = $('#infosubcategoria').serialize();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/subcategoria/editar/'.$subcategoria->id)?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        $.each(data, (index, value) => {
                            if(!value){
                                $(`#${index}`).addClass('is-valid')
                                $('div.selectize-input').addClass('form-control is-valid')                                
                                $('div.selectize-input').removeClass('is-invalid')
                                $(`#${index}`).removeClass(`#${index} is-invalid`)
                                $(`#erro_${index}`).remove()
                            } else if(value.length > 3) {
                                $(`#erro_${index}`).remove()
                                $(`#${index}`).addClass('is-invalid')
                                $('div.selectize-input').addClass('form-control is-invalid') 
                                $('div.selectize-input').removeClass('is-valid')
                                $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            }
                        })
                        if(data.edit == 1){
                            $('h1').html(`${data["dados"].nome}`)
                            $('h4#descricao').html(`Editar subcategoria ${data["dados"].nome}`)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>