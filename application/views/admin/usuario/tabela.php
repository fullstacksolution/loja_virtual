                <div class="row my-5">

                    <div class="col-md-12 mx-auto text-center">

                        <span class="display-4 d-none d-md-block"><?=ucfirst($subtitulo)?><?=($subtitulo == 'administrador') ? 'es':'s'?></span>
                        <h2 class="d-md-none d-block"><?=ucfirst($subtitulo)?><?=($subtitulo == 'administrador') ? 'es':'s'?></h2>
                        <blockquote class="blockquote text-muted"><?=$descricao?></blockquote>

                    </div>

                </div>

                <div class="row mb-5">

                    <div class="col-auto mx-auto px-auto border shadow bg-white rounded text-center py-3">
                        
                        <table id="<?=$subtitulo?>" class="table table-striped table-hover table-sm table-responsive">
                            <thead>
                                <tr>
                                    <th class="th-sm" scope="col">ID</th>
                                    <th class="th-sm" scope="col">Nome</th>
                                    <th class="th-sm" scope="col">E-mail</th>
                                    <th class="th-sm" scope="col">CPF/CNPJ</th>
                                    <th class="th-sm" scope="col">Telefone</th>
                                    <th class="th-sm" scope="col">Celular</th>
                                    <th class="th-sm" scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach($usuarios as $usuario) { ?>
                                <tr>
                                    <th scope="row"><?= $usuario->id?></th>
                                    <td><?= $usuario->nome?> <?= $usuario->sobrenome?></td>
                                    <td><?= $usuario->email?></td>
                                    <td><?= $usuario->cpf?></td>
                                    <td><?= $usuario->telefone?></td>
                                    <td><?= $usuario->celular?></td>
                                    <td>
                                        <a class="a-hover p-2" title="Editar <?=$subtitulo?>" id="editar<?=$subtitulo?><?=$usuario->id?>" href="<?= base_url('admin/'.$subtitulo.'/editar/'.$usuario->id) ?>">
                                            <i class="fas fa-pen text-warning mx-2"></i>
                                        </a>
<?php if($usuario->tipo <= 2) { ?>                               
                                        <a class="a-hover p-2" title="Deletar <?=$subtitulo?>" id="deletar<?=$subtitulo?><?=$usuario->id?>" href="<?= base_url('admin/'.$subtitulo.'/deletar/'.$usuario->id)?>">
                                            <i class="fas fa-user-slash text-danger mx-2"></i>
                                        </a>
<?php } ?>
                                    </td>
                                </tr>
<?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
                <script type="text/javascript">
                $(document).ready(function () {
                    $('table#<?=$subtitulo?>').DataTable( {
                        "columnDefs": [
                                { "orderable": false, "targets": 3},
                                { "orderable": false, "targets": 4},
                                { "orderable": false, "targets": 5}
                            ],
                        "language": {
                            "lengthMenu": "_MENU_ registros por página",
                            "zeroRecords": "Nenhum Registro Encontrado",
                            "info": "Total de _MAX_ <?=$subtitulo?>(s), página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro Disponível",
                            "infoFiltered": "",
                            "search": "Busca:",
                            "pagingType": "full_numbers",
                            "paginate": {
                                "next": "Próximo",
                                "previous": "Anterior"
                            }
                        }
                    })
                    $('div.row.mb-5 div.py-3').removeClass('border bg-white rounded shadow').addClass('card')
                })
                    var usuarios = <?= json_encode($usuarios) ?>;
                    
                    $.each(usuarios, (index, usuario) => {

                        $('a#deletar<?=$subtitulo?>'+usuario.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/deletar/') ?>${usuario.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/deletar/'+usuario.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/deletar/"+usuario.id);
                                    }
                                    
                                    $('#telefone').maskbrphone();
                                    $("#celular").mask("(99) 99999-9999").focusout(function (event) {  
                                        var target, phone, element;  
                                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                                        phone = target.value.replace(/\D/g, '');
                                        element = $(target);  
                                        element.unmask();  
                                        if(phone.length) {  
                                            element.mask("(99) 99999-9999");  
                                        }
                                    });
                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                        $('a#editar<?=$subtitulo?>'+usuario.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/editar/') ?>${usuario.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/editar/'+usuario.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/editar/"+usuario.id);
                                    }
                                    
                                    $('#telefone').maskbrphone();
                                    $("#celular").mask("(99) 99999-9999").focusout(function (event) {  
                                        var target, phone, element;  
                                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                                        phone = target.value.replace(/\D/g, '');
                                        element = $(target);  
                                        element.unmask();  
                                        if(phone.length) {  
                                            element.mask("(99) 99999-9999");  
                                        }
                                    });
                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                    })
                    
                </script>