
    <div class="row mt-4">
        <div class="col text-center">
            <h1><?= $subtitulo ?></h1>
            <h4 id="descricao"><?= $descricao ?></h4>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-8 col-md-12 mx-auto">
            <div class="card">
                <div class="card-body text-center px-lg-5 px-md-5 px-3">

                    <!-- Form -->
                    <form id="cadastro">
                        
                        <div class="row my-3">
                            <div class="col">
                                <h3>Ficha cadastral</h3>
                            </div>
                        </div>

                        <div class="row my-3">

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                            <div class="col-sm-4 col-md-4 text-center">
                            <span class="text-muted">Informações do <?= ucfirst($rota)?></span>
                            </div>

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                        </div>
                        <!-- Nome -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="nome" name="nome" maxlength="40" class="form-control">
                                    <label for="nome">Nome / Razão Social</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="sobrenome" name="sobrenome" maxlength="40" class="form-control">
                                    <label for="sobrenome">Sobrenome</label>
                                </div>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="email" id="email" name="email" maxlength="40" class="form-control">
                                    <label for="email">E-mail</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="email" id="confirmaremail" name="confirmaremail" maxlength="40" class="form-control">
                                    <label for="confirmaremail">Confirmar e-mail</label>
                                </div>
                            </div>
                        </div>

                        <!-- CPF/Tel/Cel -->
                        <div class="form-row">
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="cpf" name="cpf" maxlength="14" class="form-control">
                                    <label for="cpf">CPF / CNPJ</label>
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="telefone" name="telefone" maxlength="15" class="form-control">
                                    <label for="telefone">Telefone</label>
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="celular" name="celular" maxlength="15" class="form-control">
                                    <label for="celular">Celular</label>
                                </div>
                            </div>
                        </div>

                        <!-- Senha/Confirmação -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="password" id="senha" maxlength="30" name="senha" class="form-control">
                                    <label for="senha">Senha</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="password" id="confirmarsenha" maxlength="30" name="confirmarsenha" class="form-control">
                                    <label for="confirmarsenha">Confirmar senha</label>
                                </div>
                            </div>
                        </div>

                        <div class="row my-3">

                            <div class="col-sm-4 col-md-4">
                                <hr>
                            </div>

                            <div class="col-sm-4 col-md-4 text-center">
                                <span class="text-muted">Endereço principal  <span class="font-small">(Opcional)</span></span>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <hr>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mx-auto mb-4">
                                <div class="md-form mt-2 mb-0">
                                    <input type="text" class="form-control" size="15" maxlength="9" id="cep" name="cep" >
                                    <label for="cep">CEP</label>
                                </div>
                                <a class="text-ipci-primary font-small" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu cep</a>
                            </div>
                        </div>

                        <div class="form-row">

                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="endereco" name="endereco" maxlength="40" placeholder=" " readonly>
                                    <label class="active"  for="endereco">Endereço</label>
                                </div>
                            </div>

                            <div class="col-md-3 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="number" name="numero" id="numero" min="1" max="100000" maxlength="6" class="form-control">
                                    <label for="numero">Número</label>
                                </div>
                            </div>
                            
                            <div class="col-md-3 mx-auto mb-4">
                                <div class="md-form mt-2 mb-0">
                                    <input type="text" class="form-control" size="15" maxlength="15" id="complemento" name="complemento" >
                                    <label class="active"  for="complemento">Complemento</label>
                                </div>
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="bairro" name="bairro" maxlength="40"  placeholder=" " readonly>
                                    <label class="active"  for="bairro">Bairro</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="cidade" name="cidade" maxlength="40"  placeholder=" " readonly>
                                    <label class="active" for="cidade">Cidade</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="uf" name="uf" placeholder=" " readonly>
                                    <label class="active"  for="uf">UF</label>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="termos" id="termos" value="on">
<?php if($rota == 'cliente'){ ?>
    <input type="hidden" name="tipo_usuario" id="tipo_usuario" value="1">
<?php } else if($rota == 'administrador') { ?>
    <input type="hidden" name="tipo_usuario" id="tipo_usuario" value="2">
<?php } ?>

                        <!-- Enviar -->
                        <button class="btn btn-primary btn-rounded btn-block my-4 waves-effect" type="submit">Cadastrar</button>

                    </form>
                    <!-- Form -->

                </div>

            </div>
            
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {

        $('#cadastro').submit(e => {
            e.preventDefault()

            let dados = $('#cadastro').serialize();

            $.ajax({
                type: "POST",
                url: "<?= base_url('sign/cadastro')?>",
                data: dados,
                dataType: "json",
                success: data => {
                    $.each(data, (index, value) => {
                        if(!value){
                            $(`#${index}`).addClass('is-valid')
                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                            $(`#erro_${index}`).remove()
                        } else {
                            $(`#erro_${index}`).remove()
                            $(`#${index}`).addClass('is-invalid')
                            $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                        }
                    })
                    if(data.redirect){
                        $('div#pagina').html('<div class="alert my-5 card alert-success alert-dismissible fade show" role="alert">Usuário adicionado com sucesso. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')

                        if(data.tipo == 1){
                            tipo = 'cliente'
                        } else {
                            tipo = 'administrador'
                        }

                        setTimeout(function(){
                            $.ajax({
                                type: "post",
                                url: base_url+'/admin/'+tipo,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') == '/admin/'+tipo+'/cadastrar') {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, '/admin/'+tipo);
                                    }
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        },2000)
                    }
                },
                error: erro => {
                    console.log('Erros foram encontrados: \n')
                    console.log(erro)
                }
            })
        })
    })
    </script>