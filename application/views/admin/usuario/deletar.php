        <div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-8 mt-4 text-center mx-auto">

                <!-- Card do email -->
                <div class="card mt-4">
                    <div class="card-body p-4">
                        <form id="deletar">
                                
                            <div class="row ">
                                <div class="col-8 mx-auto text-center">
                                    <h4>Deletar usuário</h4>
                                    <p>Esse procedimento não terá volta, tem certeza que deseja excluir a ficha cadastral completa de <strong><?= $usuario->nome ?> <?= $usuario->sobrenome ?></strong>?</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4 mr-0 ml-auto pr-0">
                                    <ul class="list-group-flush mx-0 px-0">
                                        <li class="list-group-item font-weight-bold">Nome</li>
                                        <li class="list-group-item font-weight-bold">Email</li>
                                        <li class="list-group-item font-weight-bold">CPF/CNPJ</li>
                                        <li class="list-group-item font-weight-bold">Tipo</li>
                                        <li class="list-group-item font-weight-bold">Cadastrado desde</li>
                                    </ul>
                                </div>
                                <div class="col-6 ml-0 mr-auto pl-0">
                                    <ul class="list-group-flush mx-0 px-0">
                                        <li class="list-group-item"><?= $usuario->nome ?> <?= $usuario->sobrenome ?></li>
                                        <li class="list-group-item"><?= $usuario->email ?></li>
                                        <li class="list-group-item"><?php if(strlen($usuario->cpf) == 11){  echo substr($usuario->cpf, -11, 3).'.'.substr($usuario->cpf, -8, 3).'.'.substr($usuario->cpf, -5, 3).'-'.substr($usuario->cpf, -2, 3); } else { echo $usuario->cpf; }?></li>
<?php ($usuario->tipo == 1) ? $tipo_usuario = 'cliente' : $tipo_usuario = 'administrador'; ?>                                        
                                        <li class="list-group-item"><?= ucfirst($tipo_usuario) ?></li>
                                        <li class="list-group-item"><?= converter_data($usuario->data_registro) ?></li>
                                    </ul>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="<?= $usuario->id ?>">

                            <button class="btn btn-danger" type="submit">Deletar</button>

                        </form>
                    </div>
                </div>
                
            </div>

        </div>

        <script type="text/javascript">
        $(document).ready(function () {
            
            $('#deletar').submit(e => {
                e.preventDefault()
                if(<?=$usuario->tipo?> == 1){
                    var tipo_usuario = 'cliente'
                } else if(<?=$usuario->tipo?> >= 2){
                    var tipo_usuario = 'administrador'
                }
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('perfil/deletar') ?>",
                    data: 'id='+$('input#id').val(),
                    dataType: "json",
                    success: data => {
                        console.log(data);                        
                        if(data.delete == 1){
                            $('div#pagina').html('<div class="alert my-5 card alert-danger alert-dismissible fade show" role="alert">O usuário foi removido. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                            setTimeout(function(){
                                $.ajax({
                                    type: "post",
                                    url: base_url+'/admin/'+tipo_usuario,
                                    data: "request=admin",
                                    dataType: "json",
                                    success: data => {
                                        if($(location).attr('pathname') == '/admin/'+tipo_usuario+'/deletar/<?=$usuario->id?>') {
                                            $(document).attr('title', data.titulo)
                                            $('#pagina').html(data.html);
                                            window.history.pushState('string', data.titulo, '/admin/'+tipo_usuario);
                                        }
                                    },
                                    error: erro => {
                                        console.log('Erros foram encontrados:');
                                        console.log(erro);
                                    }
                                });
                            },2000)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>