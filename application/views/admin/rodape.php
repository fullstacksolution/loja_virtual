			</div>
			<!-- /container id pagina -->
			
			<div class="container">
				<!-- Rodapé -->
				<footer class="page-footer">
					
					<div class="text-center text-dark py-3">
						<a class="text-dark" href="https://www.ipci.com.br">IPCI</a> © <?= date('Y') ?> | Todos os direitos reservados.
						<br>
						Site desenvolvido por <a class="text-dark" href="https://www.fullstacksolutions.tk" title="Fullstack Solutions"target="_blank" rel="Fullstack Solutions">Fullstack Solutions</a>
					</div>
					
				</footer>
				<!-- /Rodapé -->
			</div>
			
		</div>
		<!-- /Main -->
			