		<!-- menu -->
		<div class="nav-side-menu text-dark border-right">
            <div class="col mx-auto text-center my-2">
                <a id="painel" class="navbar-brand" title="Página principal" >
                    <h2 class="text-hide my-auto">
                        IPCI
                        <img class="img-fluid img-hover" src="<?= base_url('assets/img/logotipo_ipci_b.png')?>" alt="IPCI">
                    </h2>
                </a>
            </div>
            
		    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
        
            <div class="menu-list my-auto">
        
                <ul id="menu-content" class="menu-content collapse out">
                    <li>
                        <a id="painel" class="text-ipci-admin"><i class="fa fa-tachometer-alt sidebar-icon mr-3"></i>Painel</a>
                    </li>

                    <!-- Departamentos -->
                    <li  data-toggle="collapse" data-target="#department" class="collapsed">
                        <a class="text-ipci-admin"><i class="fas fa-clipboard-list sidebar-icon mr-3"></i>Departamentos<span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="department">
                        <li><a id="listardepartamentos" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Listar departamentos</a></li>
                        <li><a id="criardepartamento" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Criar departamento</a></li>  
                    </ul>
                    
                    <!-- Categorias e Subcategorias -->
                    <li  data-toggle="collapse" data-target="#cats" class="collapsed">
                        <a class="text-ipci-admin"><i class="fas fa-bookmark sidebar-icon mr-3"></i>Categorias<span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="cats">
                        <li><a id="listarcategorias" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Listar categorias</a></li>
                        <li><a id="criarcategoria" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Criar categoria</a></li>
                        
                        <!-- Subcategorias -->
                        <li data-toggle="collapse" data-target="#subcats" class="collapsed"><a class="text-ipci-admin"><i class="far fa-bookmark mr-3"></i>Subcategorias<span class="arrow"><i class="fa fa-angle-down"></i></span></a></li>
                        <ul class="sub-menu collapse" id="subcats">
                            <li><a id="listarsubcategorias" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Listar subcategorias</a></li>
                            <li><a id="criarsubcategoria" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Criar subcategoria</a></li>
                        </ul>
                    </ul>

                    <!-- Produtos -->
                    <li  data-toggle="collapse" data-target="#products" class="collapsed">
                        <a class="text-ipci-admin"><i class="fas fa-truck-loading sidebar-icon mr-3"></i>Produtos<span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="products">
                        <li><a id="listarprodutos" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Listar produtos</a></li>
                        <li><a id="criarproduto" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Criar produto</a></li>
                        
                        <!-- Avaliações -->
                        <li data-toggle="collapse" data-target="#avaliacoes" class="collapsed"><a  class="text-ipci-admin"><i class="fas fa-star mr-3"></i>Avaliações<span class="arrow"><i class="fa fa-angle-down"></i></span></a></li>
                        <ul class="sub-menu collapse" id="avaliacoes">
                            <li><a id="listaravaliacoes" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Listar avaliações</a></li>
                            <li><a id="listaravaliacoespendentes" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Listar pendentes</a></li>
                        </ul>

                    </ul>
                    
                    <!-- Usuários -->
                    <li  data-toggle="collapse" data-target="#users" class="collapsed">
                        <a class="text-ipci-admin"><i class="fas fa-user sidebar-icon mr-3"></i>Usuários<span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="users">
                        <!-- Clientes -->
                        <li data-toggle="collapse" data-target="#customers" class="collapsed"><a  class="text-ipci-admin"><i class="fas fa-users mr-3"></i>Clientes<span class="arrow"><i class="fa fa-angle-down"></i></span></a></li>
                        <ul class="sub-menu collapse" id="customers">
                            <li><a id="listarclientes" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Listar clientes</a></li>
                            <li><a id="cadastrarcliente" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Cadastrar cliente</a></li>
                        </ul>
                        <!-- Administradores -->
                        <li data-toggle="collapse" data-target="#admins" class="collapsed"><a class="text-ipci-admin"><i class="fa fa-user-secret mr-3"></i>Administradores<span class="arrow"><i class="fa fa-angle-down"></i></span></a></li>
                        <ul class="sub-menu collapse" id="admins">
                            <li><a id="listaradministradores" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Listar administradores</a></li>
                            <li><a id="cadastraradministrador" class="text-ipci-admin"><i class="fa fa-angle-right mr-2"></i>Cadastrar administrator</a></li>
                        </ul>
                    </ul>

                    <!-- Vendas e pedidos -->
                    <li  data-toggle="collapse" data-target="#orders" class="collapsed">
                        <a class="text-ipci-admin"><i class="fas fa-store sidebar-icon mr-3"></i>Loja<span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="orders">
                        <li><a id="banner" class="text-ipci-admin"></i><i class="far fa-image mr-3"></i>Banner</a></li>
                        <li><a id="listarvendas" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Listar vendas</a></li>
                        <li><a id="listarpedidos" class="text-ipci-admin"><i class="fa fa-angle-right mr-3"></i>Listar pedidos</a></li>  
                    </ul>
                    
                    <li>
                        <a href="<?= base_url('loja') ?>" class="text-ipci-primary"><i class="fas fa-shopping-cart sidebar-icon mr-3"></i>Voltar à loja</a>
                    </li>
                    
                    <li>
                        <a href="<?= base_url('sign/out') ?>" class="text-ipci-secondary"><i class="fa fa-sign-out-alt sidebar-icon mr-3"></i>Sair</a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- /Navside -->

		<!-- Main -->
		<div class="main">
		    
		    <div class="container" id="pagina">
