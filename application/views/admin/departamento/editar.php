<div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-6 mt-4 text-center mx-auto">

                <!-- Card das informações do usuário -->
                <div class="card">
                    <div class="card-body p-4">
            
                        <form id="infodepartamento">
                        
                            <div class="row my-3">

                                <div class="col text-center">
                                    <h4>Informações do Departamento</h4>
                                </div>

                            </div>

                            <div class="form-row">

                                <!-- Nome / Razão Social -->
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" id="nome" name="nome" maxlength="30" value="<?= $departamento->nome ?>" class="form-control">
                                        <label class="active"  for="nome">Nome do departamento</label>
                                    </div>
                                </div>

                            </div>

                            <input type="hidden" name="id" id="id1" value="<?= $departamento->id ?>">
                            <input type="hidden" name="request" id="request" value="edit">

                            <button class="btn btn-primary" type="submit">Salvar</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

        <script type="text/javascript">
        $(document).ready(function () {

            $('#infodepartamento').submit(e => {
                e.preventDefault()

                let dados = $('#infodepartamento').serialize();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/departamento/editar/'.$departamento->id)?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        $.each(data, (index, value) => {
                            if(!value){
                                $(`#${index}`).addClass('is-valid')
                                $(`#${index}`).removeClass(`#${index} is-invalid`)
                                $(`#erro_${index}`).remove()
                            } else {
                                $(`#erro_${index}`).remove()
                                $(`#${index}`).addClass('is-invalid')
                                $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            }
                        })
                        if(data.edit == 1){
                            $('h1').html(`${data["dados"].nome}`)
                            $('h4#descricao').html(`Editar departamento ${data["dados"].nome}`)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>