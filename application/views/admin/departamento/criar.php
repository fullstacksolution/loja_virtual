
    <div class="row mt-4">
        <div class="col text-center">
            <h1><?= $subtitulo ?></h1>
            <h4 id="descricao"><?= $descricao ?></h4>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-8 col-md-12 mx-auto">
            <div class="card">
                <div class="card-body text-center px-lg-5 px-md-5 px-3">

                    <!-- Form -->
                    <form id="criar">
                        
                        <div class="row my-3">
                            <div class="col">
                                <h3>Criar novo departamento</h3>
                            </div>
                        </div>

                        <div class="row my-3">

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                            <div class="col-sm-4 col-md-4 text-center">
                            <span class="text-muted">Informações do <?= ucfirst($rota)?></span>
                            </div>

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                        </div>
                        <!-- Nome -->
                        <div class="form-row">
                            <div class="col-md-12 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="nome" name="nome" maxlength="30" class="form-control">
                                    <label for="nome">Nome do Departamento</label>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="request" id="request" value="criar">

                        <!-- Enviar -->
                        <button class="btn btn-primary btn-rounded btn-block my-4 waves-effect" type="submit">Criar</button>

                    </form>
                    <!-- Form -->

                </div>

            </div>
            
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {

        $('#criar').submit(e => {
            e.preventDefault()

            let dados = $('#criar').serialize();

            $.ajax({
                type: "POST",
                url: "<?= base_url('admin/departamento/criar')?>",
                data: dados,
                dataType: "json",
                success: data => {
                    $.each(data, (index, value) => {
                        if(!value){
                            $(`#${index}`).addClass('is-valid')
                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                            $(`#erro_${index}`).remove()
                        } else {
                            $(`#erro_${index}`).remove()
                            $(`#${index}`).addClass('is-invalid')
                            $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                        }
                    })
                    if(data.redirect){
                        $('div#pagina').html('<div class="alert my-5 card alert-success alert-dismissible fade show" role="alert">Departamento adicionado com sucesso. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')

                        setTimeout(function(){
                            $.ajax({
                                type: "post",
                                url: data.redirect,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    $(document).attr('title', data.titulo)
                                    $('#pagina').html(data.html);
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        },2500)
                    }
                },
                error: erro => {
                    console.log('Erros foram encontrados: \n')
                    console.log(erro)
                }
            })
        })
    })
    </script>