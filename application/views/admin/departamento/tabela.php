                <div class="row my-5">

                    <div class="col-md-12 mx-auto text-center">

                        <span class="display-4 d-none d-md-block"><?=ucfirst($subtitulo)?>s</span>
                        <h2 class="d-md-none d-block"><?=ucfirst($subtitulo)?>s</h2>
                        <blockquote class="blockquote text-muted"><?=$descricao?></blockquote>

                    </div>

                </div>

                <div class="row mb-5">

                    <div class="col-auto mx-auto px-auto border shadow bg-white rounded text-center py-3">
                        
                        <table id="<?=$subtitulo?>" class="table table-striped table-hover table-sm table-responsive">
                            <thead>
                                <tr>
                                    <th class="th-sm" scope="col">ID</th>
                                    <th class="th-sm" scope="col">Nome</th>
                                    <th class="th-sm" scope="col">Qtd. Categorias</th>
                                    <th class="th-sm" scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach($departamentos as $departamento) { ?>
                                <tr>
                                    <th scope="row"><?= $departamento->id?></th>
                                    <td><?= $departamento->nome?> </td>
                                    <td><?= $departamento->qtd_categorias ?></td>
                                    <td>
                                        <a class="a-hover p-2" title="Editar <?=$subtitulo?>" id="editar<?=$subtitulo?><?=$departamento->id?>" href="<?= base_url('admin/'.$subtitulo.'/editar/'.$departamento->id) ?>">
                                            <i class="fas fa-pen text-warning mx-2"></i>
                                        </a>                          
                                        <a class="a-hover p-2" title="Deletar <?=$subtitulo?>" id="deletar<?=$subtitulo?><?=$departamento->id?>" href="<?= base_url('admin/'.$subtitulo.'/deletar/'.$departamento->id)?>">
                                            <i class="fas fa-times-circle text-danger mx-2"></i>
                                        </a>
                                    </td>
                                </tr>
<?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
                <script type="text/javascript">
                $(document).ready(function () {
                    $('table#<?=$subtitulo?>').DataTable( {
                        "columnDefs": [
                                { "orderable": false, "targets": 2}
                            ],
                        "language": {
                            "lengthMenu": "_MENU_ registros por página",
                            "zeroRecords": "Nenhum Registro Encontrado",
                            "info": "Total de _MAX_ <?=$subtitulo?>(s), página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro Disponível",
                            "infoFiltered": "",
                            "search": "Busca:",
                            "pagingType": "full_numbers",
                            "paginate": {
                                "next": "Próximo",
                                "previous": "Anterior"
                            }
                        }
                    })
                    $('table').css('width', 'auto')
                    $('div.row.mb-5 div.py-3').removeClass('border bg-white rounded shadow').addClass('card')
                })
                    var departamentos = <?= json_encode($departamentos) ?>;
                    
                    $.each(departamentos, (index, departamento) => {

                        $('a#deletar<?=$subtitulo?>'+departamento.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/deletar/') ?>${departamento.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/deletar/'+departamento.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/deletar/"+departamento.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                        $('a#editar<?=$subtitulo?>'+departamento.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/editar/') ?>${departamento.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/editar/'+departamento.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/editar/"+departamento.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                    })
                    
                </script>