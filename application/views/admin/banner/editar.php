        <div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-10 mt-4 text-center mx-auto">

                <!-- Card das informações do usuário -->
                <div class="card">
                    <div class="card-body p-4">
            
                        <form id="infobanner">
                        
                            <div class="row my-3">

                                <div class="col text-center">
                                    <h4>Informações do Banner</h4>
                                </div>

                            </div>

                            <div class="form-row">
                                <!-- Imagem -->
                                <div class="col-md-10 mx-auto text-center">
                                    <img src="<?= base_url('uploads/banner/'.$banner->img) ?>" alt="<?= $banner->nome ?>" class="w-100">
                                </div>
                            </div>

                            <div class="form-row">
                                <!-- Nome  -->
                                <div class="col-md-6 mx-auto mt-4">
                                    <div class="md-form mt-2">
                                        <input type="text" id="nome" name="nome" maxlength="30" value="<?= $banner->nome ?>" class="form-control">
                                        <label class="active"  for="nome">Nome do banner</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <!-- Link -->
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" id="link" name="link" maxlength="30" value="<?= $banner->link ?>" class="form-control">
                                        <label class="active"  for="link">Link</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="custom-control custom-switch text-muted font-weight-light">
                                        <input type="checkbox" class="custom-control-input" id="status" name="status" value="<?= $banner->status ?>" <?= ($banner->status) ? 'checked' : '' ?>>
                                        <label class="custom-control-label" for="status">Publicar na loja?</label>
                                    </div>
                                </div>
                            </div>
                            
                            <input type="hidden" name="id" id="id" value="<?= $banner->id ?>">
                            <input type="hidden" name="request" id="request" value="edit">

                            <button class="btn btn-primary mt-4" type="submit">Salvar</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

        <script type="text/javascript">
        $(document).ready(function () {

            $("#status").on('change', function() {
                if ($(this).is(':checked')) {
                    $(this).attr('value', 1);
                } else {
                    $(this).attr('value', 0);
                }
            });

            $('#infobanner').submit(e => {
                e.preventDefault()

                let dados = $('#infobanner').serialize();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/banner/editar/'.$banner->id)?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        $.each(data, (index, value) => {
                            if(!value){
                                $(`#${index}`).addClass('is-valid')
                                $(`#${index}`).removeClass(`#${index} is-invalid`)
                                $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                                $(`#erro_${index}`).remove()
                            } else {
                                $(`#erro_${index}`).remove()
                                $(`#${index}`).addClass('is-invalid')
                                $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                                $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            }
                        })
                        if(data.sucesso){
                            $('h1').html(`${data["dados"].nome}`)
                            $('h4#descricao').html(`Editar banner ${data["dados"].nome}`)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>