<div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-8 mt-4 text-center mx-auto">

                <!-- Card do email -->
                <div class="card mt-4">
                    <div class="card-body p-4">
                        <form id="deletar">
                                
                            <div class="row ">
                                <div class="col-10 mx-auto text-center">
                                    <h4>Deletar banner</h4>
                                    <p>Esse procedimento não terá volta, tem certeza que deseja excluir o banner <strong><?= $banner->nome ?></strong></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8 mx-auto text-center">
                                    <img src="<?= base_url('uploads/banner/'.$banner->img) ?>" alt="<?= $banner->nome ?>" class="w-100">
                                </div>
                            </div>

                            <div id="coluna" class="row">
                                <div class="col-4 mr-0 ml-auto pr-0">
                                    <ul class="list-group-flush mx-0 px-0">
                                        <li class="list-group-item font-weight-bold">Nome</li>
                                        <li class="list-group-item font-weight-bold">Link</li>
                                        <li class="list-group-item font-weight-bold">Status</li>
                                    </ul>
                                </div>
                                <div class="col-6 ml-0 mr-auto pl-0">
                                    <ul class="list-group-flush mx-0 px-0">
                                        <li class="list-group-item"><?= $banner->nome ?></li>
                                        <li class="list-group-item"><a href="<?= $banner->link ?>" class="text-ipci-nocolor"><?= $banner->link ?></a></li>
                                        <li class="list-group-item"><?= ($banner->status) ? 'Publicado' : 'Não publicado' ?></li>
                                    </ul>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="<?= $banner->id ?>">
                            <input type="hidden" name="request" id="request" value="delete">

                            <button class="btn btn-danger" type="submit">Deletar</button>

                        </form>
                    </div>
                </div>
                
            </div>

        </div>

        <script type="text/javascript">
        $(document).ready(function () {
            
            $('#deletar').submit(e => {
                e.preventDefault()
                
                let dados = $('#deletar').serialize();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/banner/deletar/'.$banner->id) ?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        if(data.delete == 1){
                            $('div#pagina').html('<div class="alert my-5 card alert-danger alert-dismissible fade show" role="alert">O banner foi removido. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                            setTimeout(function(){
                                $.ajax({
                                    type: "post",
                                    url: '<?= base_url('admin/banner') ?>',
                                    data: "request=admin",
                                    dataType: "json",
                                    success: data => {
                                        if($(location).attr('pathname') == '/admin/banner/deletar/<?=$banner->id?>') {
                                            $(document).attr('title', data.titulo)
                                            $('#pagina').html(data.html);
                                            window.history.pushState('string', data.titulo, "/admin/banner/");
                                        }
                                    },
                                    error: erro => {
                                        console.log('Erros foram encontrados:');
                                        console.log(erro);
                                    }
                                });
                            },2000)
                        }
                        if(data.erro){
                            $('div.invalid-feedback').remove()
                            $('li#item-banner, li#group-banner').addClass('text-danger')
                            $('div.row#coluna').append($('<div class="invalid-feedback d-block">'+data.erro+'</div>'))
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>