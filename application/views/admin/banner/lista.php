                <div class="row my-5" id="cabecalho">

                    <div class="col-md-12 mx-auto text-center">

                        <span class="display-4 d-none d-md-block">Banner</span>
                        <h2 class="d-md-none d-block">Banner</h2>
                        <blockquote class="blockquote text-muted">Banner(s) de destaque da loja</blockquote>

                    </div>

                </div>

                <div class="row mb-5" id="col_tabela">

                    <div class="col-xl-9 col-lg-10 col-md-12 mx-auto px-auto card bg-white rounded text-center py-3">
                        
                        <table id="banner" class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th class="th-sm" scope="col">ID</th>
                                    <th class="th-sm" scope="col">Imagem</th>
                                    <th class="th-sm" scope="col">Nome do banner</th>
                                    <th class="th-sm" scope="col">Link</th>
                                    <th class="th-sm" scope="col">Status</th>
                                    <th class="th-sm" scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>
                

                