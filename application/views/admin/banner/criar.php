                <div class="row mt-4" id="row_criar">
                    <div class="col-xl-9 col-lg-10 col-md-12 mx-auto px-auto card bg-white rounded text-center py-3">

                        <div class="card-body p-4">
                
                            <form id="adicionar_banner">
                            
                                <div class="row my-3">
                                    <div class="col text-center">
                                        <h4>Adicionar Banner</h4>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="nome" id="nome">
                                            <label for="nome">Nome</label>
                                        </div>
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="link" id="link">
                                            <label for="link">Link de redirecionamento</label>
                                        </div>
                                        <div class="custom-control custom-switch text-muted font-weight-light">
                                            <input type="checkbox" class="custom-control-input" id="status" name="status" value="0">
                                            <label class="custom-control-label" for="status">Publicar na loja?</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mx-auto">
                                        <div class="border">
                                            <img class="img-fluid w-100" id="preview" src="<?= base_url('uploads/sembanner.jpg')?>" alt="Banner">
                                        </div>
                                        <div class="custom-file">
                                            <input class="custom-file-input" type="file" name="imagem" id="imagem" accept="image/jpeg, image/png" >
                                            <label class="custom-file-label text-left" for="imagem" data-browse="Banner">Escolha uma imagem</label>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="request" id="request" value="admin">

                                <button class="btn btn-primary mt-4" type="submit">Adicionar banner</button>
                                <button class="btn btn-light mt-4" type="reset">Limpar</button>
                            
                            </form>
                        
                        </div>
                    
                    </div>
                </div>
                <?= $this->load->view('admin/js/banner', null, true)?>
