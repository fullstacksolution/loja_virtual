        <div class="row mt-4">
            <div class="col text-center">
                <h1>Deletar avaliação</h1>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-10 mt-4 text-center mx-auto">

                <!-- Card da avaliação -->
                <div class="card mt-4">
                    <div class="card-body p-4">
                        <form id="deletar">
                                
                            <div class="row ">
                                <div class="col-10 mx-auto text-center">
                                    <h4>Deletar avaliação</h4>
                                    <p>Esse procedimento não terá volta. Deseja mesmo deletar a avaliação deste cliente?</p>
                                </div>
                            </div>

                            <div id="coluna" class="row">
                                <div class="col mx-auto">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">ID do Usuário</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->id_usuario?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">ID da Avaliação</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->id?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Nome</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->nome_usuario?> <?= $avaliacao->sobrenome_usuario?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Email</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->email_usuario?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Título da Avaliação</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->titulo ?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Comentário da Avaliação</th>
                                                <th class="h6 font-weight-normal"><?= $avaliacao->comentario ?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Avaliação</th>
                                                <th class="h6 font-weight-normal">
<?php if ($avaliacao->avaliacao == 0) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                    </ul>
<?php } else if ($avaliacao->avaliacao == 1) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                    </ul>
<?php } else if ($avaliacao->avaliacao == 2) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                    </ul>
<?php } else if ($avaliacao->avaliacao == 3) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                    </ul>
<?php } else if ($avaliacao->avaliacao == 4) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                                    </ul>
<?php } else if ($avaliacao->avaliacao == 5) { ?>
                                                    <ul class="list-group-horizontal mx-0 px-0">
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                        <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                                    </ul>
<?php } ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Status da Avaliação</th>
                                                <th class="h6 font-weight-normal"><?= ($avaliacao->status) ? 'Publicada' : 'Não publicada' ?> </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="id_avaliacao" id="id_avaliacao" value="<?= $avaliacao->id ?>">
                            <input type="hidden" name="id_usuario" id="id_usuario" value="<?= $avaliacao->id_usuario ?>">
                            <input type="hidden" name="request" id="request" value="delaval">

                            <button class="btn btn-danger" type="submit">Deletar</button>

                        </form>
                    </div>
                </div>
                
            </div>

        </div>

        <script type="text/javascript">
        $(document).ready(function () {
            
            $('#deletar').submit(e => {
                e.preventDefault()
                
                let dados = $('#deletar').serialize();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/produto/avaliacao/deletar/'.$avaliacao->id) ?>",
                    data: dados,
                    dataType: "json",
                    success: data => {           
                        console.log(data);
                                   
                        if(data.delete == 1){
                            $('div#pagina').html('<div class="alert my-5 card alert-danger alert-dismissible fade show" role="alert">A avaliação foi removida. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                            setTimeout(function(){
                                window.history.back();
                            },2000)
                        }
                        if(data.erro){
                            $('div.invalid-feedback').remove()
                            $('li#item-avaliacao, li#group-avaliacao').addClass('text-danger')
                            $('div.row#coluna').append($('<div class="invalid-feedback d-block">'+data.erro+'</div>'))
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>