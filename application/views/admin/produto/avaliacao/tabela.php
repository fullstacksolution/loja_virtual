                <div class="row my-5">

                    <div class="col-md-12 mx-auto text-center">

                        <span class="display-4 d-none d-md-block"><?=ucfirst($subtitulo)?></span>
                        <h2 class="d-md-none d-block"><?=ucfirst($subtitulo)?></h2>
                        <blockquote class="blockquote text-muted"><?=$descricao?></blockquote>

                    </div>

                </div>

                <div class="row mb-5">

                    <div class="col-auto mx-auto px-auto border shadow bg-white rounded text-center py-3">
                    
                        <table id="<?=$subtitulo?>" class="table table-striped table-hover table-sm table-responsive">
                            <thead>
                                <tr>
                                    <th class="th-sm" scope="col">ID</th>
                                    <th class="th-sm" scope="col">Produto</th>
                                    <th class="th-sm" scope="col">Título</th>
                                    <th class="th-sm" scope="col">Comentário</th>
                                    <th class="th-sm" scope="col">Usuário</th>
                                    <th class="th-sm d-none">Outros</th>
                                    <th class="th-sm" scope="col">Data</th>
                                    <th class="th-sm" scope="col">Avaliação</th>
                                    <th class="th-sm" scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach($avaliacoes as $avaliacao) { ?>
                                <tr>
                                    <th><?= $avaliacao->id?></th>
                                    <th><?= $avaliacao->produto ?> </th>
                                    <th><?= $avaliacao->titulo ?></th>
                                    <th><?= $avaliacao->comentario?> </th>
                                    <th><?= $avaliacao->usuario_email?> </th>
                                    <th class="d-none"><?= $avaliacao->subcategoria ?> em <?= $avaliacao->categoria ?> de <?= $avaliacao->departamento?></th>
                                    <th><?= $avaliacao->data?> </th>
                                    <th>
<?php if ($avaliacao->avaliacao == 0) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 1) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 2) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 3) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 4) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star grey-text"></i></li>
                                        </ul>
<?php } else if ($avaliacao->avaliacao == 5) { ?>
                                        <ul class="list-group-horizontal mx-0 px-0">
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                            <li class="list-inline-item mb-0 mx-0"><i class="fas fa-star text-ipci-primary"></i></li>
                                        </ul>
<?php } ?>

                                    </th>
                                    <td>  
<?php if(!$avaliacao->status){?>
                                        <a class="a-hover p-2" title="Publicar avaliação" id="publicaravaliacao<?=$avaliacao->id?>" href="<?= base_url('admin/produto/avaliacao/publicar/'.$avaliacao->id)?>">
                                            <i class="fas fa-check-circle text-ipci-primary mx-2"></i>
                                        </a>
<?php } ?>
                                        <a class="a-hover p-2" title="Deletar avaliação" id="deletaravaliacao<?=$avaliacao->id?>" href="<?= base_url('admin/produto/avaliacao/deletar/'.$avaliacao->id)?>">
                                            <i class="fas fa-times-circle text-danger mx-2"></i>
                                        </a>
                                    </td>
                                </tr>
<?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
                <script type="text/javascript">
                $(document).ready(function () {
                    var groupColumn = 5;
                    $('table#<?=$subtitulo?>').DataTable( {
                        "columnDefs": [
                                { "orderable": false, "targets": 2},
                                { "orderable": false, "targets": 3},
                                { "orderable": false, "targets": 4},
                                { "visible": false, "targets": groupColumn },
                                { "searchable": true, "targets": groupColumn }
                                
                            ],                            
                        "language": {
                            "lengthMenu": "_MENU_ registros por página",
                            "zeroRecords": "Nenhum Registro Encontrado",
                            "info": "Total de _MAX_ <?=$subtitulo?>, página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro Disponível",
                            "infoFiltered": "",
                            "search": "Busca:",
                            "pagingType": "full_numbers",
                            "paginate": {
                                "next": "Próximo",
                                "previous": "Anterior"
                            }
                        },   
                        "order": [[ groupColumn, 'asc' ]], 
                        "displayLength": 25,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;
                
                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="grey lighten-2"><td colspan="8">'+group+'</td></tr>'
                                    );
                
                                    last = group;
                                }
                            } );
                        }
                    })
                    $('table').css('width', 'auto')
                    $('div.row.mb-5 div.py-3').removeClass('border bg-white rounded shadow').addClass('card')
                })
                    var avaliacoes = <?= json_encode($avaliacoes) ?>;
                    
                    $.each(avaliacoes, (index, avaliacao) => {

                        $('a#publicaravaliacao'+avaliacao.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/produto/avaliacao/publicar/') ?>${avaliacao.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if(data.sucesso == 1){
                                        e.preventDefault()
                                        $.ajax({
                                            type: "post",
                                            url: "<?= base_url('admin/produto/avaliacao/listar_pendentes') ?>",
                                            data: "request=admin",
                                            dataType: "json",
                                            success: data => {
                                                if($(location).attr('pathname') != '/admin/produto/avaliacao/listar_pendentes') {
                                                    $(document).attr('title', data.titulo)
                                                    $('#pagina').html(data.html);
                                                    window.history.pushState('string', data.titulo, "/admin/produto/avaliacao/listar_pendentes");
                                                } else { 
                                                    $(document).attr('title', data.titulo)
                                                    $('#pagina').html(data.html);
                                                }
                                            },
                                            error: erro => {
                                                console.log('Erros foram encontrados:');
                                                console.log(erro);
                                                
                                            }
                                        });
                                    }
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })

                        $('a#deletaravaliacao'+avaliacao.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/produto/avaliacao/deletar/') ?>${avaliacao.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/produto/avaliacao/deletar/'+avaliacao.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/produto/avaliacao/deletar/"+avaliacao.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                    })
                    
                </script>