        <div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-10 mt-4 text-center mx-auto">

                <!-- Card do email -->
                <div class="card mt-4">
                    <div class="card-body p-4">
                        <form id="deletar">
                                
                            <div class="row ">
                                <div class="col-10 mx-auto text-center">
                                    <h4>Deletar produto</h4>
                                    <p>Esse procedimento não terá volta. O produto em questão será apagado de todos os pedidos de clientes na qual ele está inserido.</p> 
                                    <p>Tem certeza que deseja excluir o produto <strong><?= $produto->nome ?></strong> associada à subcategoria <strong><?= $produto->subcategoria ?></strong> em <strong><?= $produto->categoria ?></strong> de <strong><?= $produto->departamento?></strong> <?= ($produto->qtd_imagens> 0) ? 'e todas suas imagens' : '' ?>?</p>
                                </div>
                            </div>

                            <div id="coluna" class="row">
                                <div class="col mx-auto">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">ID</th>
                                                <th class="h6 font-weight-normal"><?= $produto->id?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Produto</th>
                                                <th class="h6 font-weight-normal"><?= $produto->nome?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Categoria</th>
                                                <th class="h6 font-weight-normal"><?= $produto->categoria?> de <?=$produto->departamento?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Subcategoria</th>
                                                <th class="h6 font-weight-normal"><?= $produto->subcategoria?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Fabricante</th>
                                                <th class="h6 font-weight-normal"><?= $produto->fabricante?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Marca</th>
                                                <th class="h6 font-weight-normal"><?= $produto->marca?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Modelo</th>
                                                <th class="h6 font-weight-normal"><?= $produto->modelo?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Codigo</th>
                                                <th class="h6 font-weight-normal"><?= $produto->codigo?></th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Qtd.</th>
                                                <th class="h6 font-weight-normal"><?= $produto->quantidade?> </th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Imagens</th>
                                                <th class="h6 font-weight-normal"><?= $produto->qtd_imagens?> imagens</th>
                                            </tr>
                                            <tr>
                                                <th class="h6 text-decoration-none font-weight-bold">Status</th>
                                                <th class="h6 font-weight-normal"><?= ($produto->status) ? 'Publicado' : 'Não publicado' ?> </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="<?= $produto->id ?>">
                            <input type="hidden" name="request" id="request" value="delete">

                            <button class="btn btn-danger" type="submit">Deletar</button>

                        </form>
                    </div>
                </div>
                
            </div>

        </div>

        <script type="text/javascript">
        $(document).ready(function () {
            
            $('#deletar').submit(e => {
                e.preventDefault()
                
                let dados = $('#deletar').serialize();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/produto/deletar/'.$produto->id) ?>",
                    data: dados,
                    dataType: "json",
                    success: data => {                      
                        if(data.delete == 1){
                            $('div#pagina').html('<div class="alert my-5 card alert-danger alert-dismissible fade show" role="alert">O produto foi removido. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                            setTimeout(function(){
                                $.ajax({
                                    type: "post",
                                    url: '<?= base_url('admin/produto') ?>',
                                    data: "request=admin",
                                    dataType: "json",
                                    success: data => {
                                        if($(location).attr('pathname') == '/admin/produto/deletar/<?=$produto->id?>') {
                                            $(document).attr('title', data.titulo)
                                            $('#pagina').html(data.html);
                                            window.history.pushState('string', data.titulo, "/admin/produto/");
                                        }
                                    },
                                    error: erro => {
                                        console.log('Erros foram encontrados:');
                                        console.log(erro);
                                    }
                                });
                            },2000)
                        }
                        if(data.erro){
                            $('div.invalid-feedback').remove()
                            $('li#item-produto, li#group-produto').addClass('text-danger')
                            $('div.row#coluna').append($('<div class="invalid-feedback d-block">'+data.erro+'</div>'))
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>