        <div class="row mt-4">
            <div class="col text-center">
                <h1><?= $subtitulo ?></h1>
                <h4 id="descricao"><?= $descricao ?></h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col mx-auto text-center">
                <a id="redirect_info" href="#informacoes" class="btn btn-dark">Informações do produto</a>
                <a id="redirect_img" href="#imagens" class="btn btn-dark">Imagens do produto</a>
            </div>
        </div>

        <span id="informacoes"></span>
        <div class="row mt-4">
            <div class="col-xl-9 col-lg-10 col-md-12 mt-2 text-center mx-auto">

                <!-- Card das informações do produto -->
                <div class="card">
                    <div class="card-body p-4">
            
                        <form id="infoproduto">
                        
                            <div class="row my-3">

                                <div class="col text-center">
                                    <h4>Informações do Produto</h4>
                                </div>

                            </div>

                            <div class="form-row">
                                <!-- Nome  -->
                                <div class="col-md-12 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" id="nome" name="nome" maxlength="60" value="<?= $produto->nome ?>" class="form-control">
                                        <label class="active" for="nome">Nome do Produto</label>
                                    </div>
                                </div>
                            </div>
                                
                            <!-- Subcategoria -->
                            <div class="form-row">
                                <div class="col mx-auto">
                                    <label class="text-muted font-weight-light" for="id_subcategoria">Subcategoria*</label>
                                    <select class="form-control" name="id_subcategoria" id="id_subcategoria">
                                        <option value="" disabled>-- Selecione --</option>
<?php foreach($subcategorias as $subcategoria) { ?>
                                        <option value="<?= $subcategoria->id ?>" <?= ($produto->id_subcategoria == $subcategoria->id) ? 'selected' : '' ?> ><?= $subcategoria->nome ?> (em <?=$subcategoria->categoria?> de <?=$subcategoria->departamento?>)</option>
<?php } ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Descrição -->
                            <div class="form-row">
                                <div class="col mx-auto">
                                    <div class="md-form mt-2">
                                        <textarea class="md-textarea w-100" name="descricao" id="descricao" rows="3"><?= $produto->descricao ?> </textarea>
                                        <label class="active" for="descricao">Descrição do <?=ucfirst($rota)?>*</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Fabricante e Marca -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="fabricante" id="fabricante" maxlength="30" value="<?= $produto->fabricante ?>" class="form-control">
                                        <label  class="active" for="fabricante">Fabricante do <?=ucfirst($rota)?></label>
                                    </div>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="marca" id="marca" maxlength="30" value="<?= $produto->marca ?>" class="form-control">
                                        <label class="active" for="marca">Marca do <?=ucfirst($rota)?></label>
                                    </div>
                                </div>
                            </div>

                            <!-- Modelo e Cor -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="modelo" id="modelo" maxlength="30" value="<?= $produto->modelo ?>" class="form-control">
                                        <label class="active" for="modelo">Modelo do <?=ucfirst($rota)?></label>
                                    </div>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="cor" id="cor" maxlength="30" value="<?= $produto->cor ?>" class="form-control">
                                        <label  class="active" for="cor">Cor do <?=ucfirst($rota)?></label>
                                    </div>
                                </div>
                            </div>

                            <!-- Preços -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="preco_custo" id="preco_custo" maxlength="9" value="<?= transforma_preco($produto->preco_custo) ?>"  class="form-control">
                                        <label  class="active" for="preco_custo">Preço de custo (R$)*</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="preco_venda" id="preco_venda" maxlength="9" value="<?= transforma_preco($produto->preco_venda) ?>" class="form-control">
                                        <label  class="active" for="preco_venda">Preço de venda (R$)*</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Garantia e Preço promocional-->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="preco_promocional" id="preco_promocional" maxlength="9" value="<?= transforma_preco($produto->preco_promocional) ?>" class="form-control">
                                        <label  class="active" for="preco_promocional">Preço promocional (R$)</label>
                                    </div>
                                </div>
                                <div class="col-md-4 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="garantia" id="garantia" maxlength="2" value="<?= $produto->garantia ?>" class="form-control">
                                        <label  class="active" for="garantia">Garantia (mês)</label>
                                    </div>
                                </div>
                                <div class="col-md-2 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="ipi" id="ipi" maxlength="3" value="<?= $produto->ipi ?>" class="form-control">
                                        <label  class="active" for="ipi">IPI</label>
                                    </div>
                                </div>
                            </div>

                            <!-- SKU e EAN/GTIN-->
                            <div class="form-row">
                                <div class="col-md-7 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="sku" id="sku" maxlength="30" value="<?= $produto->sku ?>" class="form-control">
                                        <label  class="active" for="sku">SKU</label>
                                    </div>
                                </div>
                                <div class="col-md-5 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="ean_gtin" id="ean_gtin" maxlength="13" value="<?= $produto->ean_gtin ?>" class="form-control">
                                        <label  class="active" for="ean_gtin">EAN/GTIN</label>
                                    </div>
                                </div>
                            </div>

                            <!-- MPN/Part Number e NCM-->
                            <div class="form-row">
                                <div class="col-md-7 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="mpn_partnumber" id="mpn_partnumber" maxlength="20" value="<?= $produto->mpn_partnumber ?>" class="form-control">
                                        <label  class="active" for="mpn_partnumber">MPN/Part Number</label>
                                    </div>
                                </div>
                                <div class="col-md-5 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="ncm" id="ncm" maxlength="13" value="<?= $produto->ncm ?>" class="form-control">
                                        <label  class="active" for="ncm">NCM</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Serial e caixa -->
                            <div class="form-row">
                                <div class="col-lg-6 col-md-8 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="serial" id="serial" maxlength="30" value="<?= $produto->serial ?>" class="form-control">
                                        <label  class="active" for="serial">Serial</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="caixa" id="caixa" maxlength="5" value="<?= $produto->caixa ?>" class="form-control">
                                        <label  class="active" for="caixa">Caixa</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Peso e NCM-->
                            <div class="form-row">
                                <div class="col-lg-3 col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="peso_kg" id="peso_kg" maxlength="6" value="<?= verifica_kg($produto->peso_kg) ?>" class="form-control">
                                        <label  class="active" for="peso_kg">Peso (kg)</label>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="altura_cm" id="altura_cm" maxlength="6" value="<?= verifica_cm($produto->altura_cm) ?>" class="form-control">
                                        <label  class="active" for="altura_cm">Altura (cm)</label>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="largura_cm" id="largura_cm" maxlength="6" value="<?= verifica_cm($produto->largura_cm) ?>" class="form-control">
                                        <label  class="active" for="largura_cm">Largura (cm)</label>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="profundidade_cm" id="profundidade_cm" maxlength="6" value="<?= verifica_cm($produto->profundidade_cm) ?>" class="form-control">
                                        <label  class="active" for="profundidade_cm">Profundidade (cm)</label>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Endereço Estoque e Quantidade -->
                            <div class="form-row">
                                <div class="col-lg-6 col-md-8 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="text" name="endereco_estoque" id="endereco_estoque" maxlength="10" value="<?= $produto->endereco_estoque ?>" class="form-control">
                                        <label  class="active" for="endereco_estoque">Endereço estoque</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 mx-auto">
                                    <div class="md-form mt-2">
                                        <input type="number" name="quantidade" id="quantidade" min="0" max="999" maxlength="3" value="<?= $produto->quantidade ?>" class="form-control">
                                        <label  class="active" for="quantidade">Quantidade*</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Unidade e Disponibilidade -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <label class="text-muted font-weight-light" for="unidade">Unidade</label>
                                    <select class="form-control" name="unidade" id="unidade">
                                        <option value="" disabled <?= ($produto->unidade == '') ? 'selected' : '' ?>>-- Selecione --</option>
                                        <option value="Peça" <?= ($produto->unidade == 'Peça') ? 'selected' : '' ?>>Peça</option>
                                        <option value="Par" <?= ($produto->unidade == 'Par') ? 'selected' : '' ?>>Par</option>
                                        <option value="Dúzia" <?= ($produto->unidade == 'Dúzia') ? 'selected' : '' ?>>Dúzia</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Variação e Condição -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <label class="text-muted font-weight-light mt-2" for="disponibilidade">Disponibilidade*</label>
                                    <select class="form-control" name="disponibilidade" id="disponibilidade">
                                        <option value="" disabled <?= ($produto->disponibilidade == '') ? 'selected' : '' ?>>-- Selecione --</option>
                                        <option value="imediata" <?= ($produto->disponibilidade == 'imediata') ? 'selected' : '' ?>>Imediata</option>
                                        <option value="programada" <?= ($produto->disponibilidade == 'programada') ? 'selected' : '' ?>>Programada</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <label class="text-muted font-weight-light mt-2" for="condicao">Condição*</label>
                                    <select class="form-control" name="condicao" id="condicao">
                                        <option value="" disabled <?= ($produto->condicao == '') ? 'selected' : '' ?>>-- Selecione --</option>
                                        <option value="novo" <?= ($produto->condicao == 'novo') ? 'selected' : '' ?>>Novo</option>
                                        <option value="usado" <?= ($produto->condicao == 'usado') ? 'selected' : '' ?>>Usado</option>
                                        <option value="variação" <?= ($produto->condicao == 'variação') ? 'selected' : '' ?>>Com variação</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Código e Status -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <label class="text-muted font-weight-light mt-2" for="codigo">Codigo*</label>
                                    <select class="form-control" name="codigo" id="codigo">
                                        <option value="" disabled <?= ($produto->codigo == '') ? 'selected' : '' ?>>-- Selecione --</option>
                                        <option value="RTC" <?= ($produto->codigo == 'RTC') ? 'selected' : '' ?>>RTC</option>
                                        <option value="IPCI" <?= ($produto->codigo == 'IPCI') ? 'selected' : '' ?>>IPCI</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <label class="text-muted font-weight-light mt-2" for="status">Status*</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="0" <?= (!$produto->status) ? 'selected' : '' ?>>Não publicado</option>
                                        <option value="1" <?= ($produto->status) ? 'selected' : '' ?>>Publicado</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Promoção e Destaque -->
                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="form-group custom-control custom-checkbox mt-3">
                                        <input type="checkbox" class="custom-control-input" id="promocao" name="promocao[]" value="1" <?= ($produto->promocao) ? 'checked' : '' ?>>
                                        <label class="custom-control-label text-muted font-weight-light" for="promocao">Está em promoção?</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="form-group custom-control custom-checkbox mt-3">
                                        <input type="checkbox" class="custom-control-input" id="destaque" name="destaque[]" value="1" <?= ($produto->destaque) ? 'checked' : '' ?>>
                                        <label class="custom-control-label text-muted font-weight-light" for="destaque">Destacar produto na loja</label>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="<?= $produto->id ?>">
                            <input type="hidden" name="request" id="request" value="edit">

                            <button class="btn btn-primary" type="submit">Salvar</button>

                        </form>
                    </div>

                </div>
                <!-- /Card das informações do produto -->
            </div>
        </div>

        <span id="imagens" ></span>

        <div class="row mt-4">
            <div class="col-xl-9 col-lg-10 col-md-12 mt-2 text-center mx-auto">

                <!-- Card de imagens do produto -->
                <div class="card">
                    <div class="card-body p-4">
                    
                        <div class="row my-3">
                            <div class="col text-center">
                                <h4>Imagens do Produto</h4>
                                <form id="listarimagens">
                                    <input type="hidden" name="request" id="request3" value="listimg">
                                    <input type="hidden" name="id_produto" id="id3" value="<?=$produto->id?>">
                                </form>

                                <!-- Galeria -->
                                <div id="galeria">
                                    
                                </div>
                                <!-- /Galeria -->

                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /Card de imagens do produto -->
            </div>
        </div>

        <!-- Adicionar imagem -->
        <div class="row mt-4">
            <div class="col-xl-9 col-lg-10 col-md-12 mt-2 text-center mx-auto">
                <!-- Card de inserção de imagem -->
                <div class="card">
                    <div class="card-body p-4">
            
                        <form id="imgproduto">

                            <div class="row my-3">
                                <div class="col text-center">
                                    <h4>Adicionar Imagem</h4>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mx-auto">
                                    <div class="form-group">
                                        <input class="form-control-file" type="file" name="imagem" id="imagem" accept="image/jpeg, image/png">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="id_produto" id="id2" value="<?= $produto->id ?>">
                            <input type="hidden" name="request" id="request2" value="addimg">

                            <button class="btn btn-primary" type="submit">Salvar imagem</button>

                        </form>

                    </div>
                </div>
                <!-- /Card de inserção de imagem -->
            </div>
        </div>

        <script type="text/javascript">
        

        $(document).ready(function () {

            var id_produto = <?= $produto->id ?>;
            var id_produto_md5 = '<?= md5($produto->id) ?>';

            
            

            function carregar_lista(){

                $('div#galeria').html('')
                let dados = $('#listarimagens').serialize();
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/produto/imagem/listar')?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        if(data[0]){
                            $('div#galeria').append('<div id="thumbImagens">')
                            $('div#thumbImagens').append('<ul class="list-inline">')

                            $.each(data, (index, value) =>{
                                $('div#thumbImagens ul').append('<li class="list-inline-item mt-4" id="'+value.id+'">')
                                $('div#thumbImagens ul li#'+value.id).append('<img class="img-fluid img-thumbnail d-flex" src="<?=base_url('uploads/produtos/')?>'+id_produto_md5+'/'+value.img+'" >'+value.img)
                                $('div#thumbImagens ul li#'+value.id).append(' <a href="&id_imagem='+value.id+'&id_produto='+id_produto+'&img='+value.img+'" class="ml-auto text-ipci-secondary"><i class="fas fa-times-circle "></i></a>')
                                $('div#thumbImagens ul').append('</li>')
                            })
                            
                            $('div#galeria').append('</ul>')
                            $('div#galeria').append('</div>')

                            
                            let ativa = 1;
                            if($(window).attr('screen').width > 767){
                                $('img.img-fluid').click(function(){
                                    var fator_lupa = 3;
                                    if(ativa == 1 && !$(this).hasClass('active-img')) {
                                        $(this).addClass('active-img')
                                        this.style.width = (this.clientWidth * fator_lupa) + "px";
                                        ativa = 0;
                                    } else if(ativa == 0 && $(this).hasClass('active-img')) { 
                                        $(this).removeClass('active-img')
                                        this.style.width = (this.clientWidth / fator_lupa) + "px";
                                        ativa = 1;
                                    }
                                })
                            }             
            
                            // Deletar imagem
                            $('div#thumbImagens ul a').on('click', (e)=>{
                                e.preventDefault()
                                let dados_img = $(e.target).parent().attr('href')
                                $.ajax({
                                    type: 'post',
                                    url: "<?= base_url('admin/produto/imagem/deletar')?>",
                                    data: 'request=delimg'+dados_img,
                                    dataType: "json",
                                    success: data => {
                                        carregar_lista()
                                        $('div#alert3').remove();
                                        $('form#listarimagens').parent().find('h4').before($('<div id="alert3" class="alert card alert-success fade show"><strong>'+data.sucesso+'</strong></div>'))
                                        setTimeout(()=>{
                                            $("div#alert3").alert('close',()=>{
                                                $('div#alert3').remove(); 
                                            })
                                        },4000)
                                    },
                                    error: erro => {
                                        console.log('Erros foram encontrados: \n')
                                        console.log(erro)
                                    }
                                })
                            })
                        } else{
                            $('div#galeria').html('<p>O produto ainda não possui imagens.</p>')
                        }
                        
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                })
            }

            $(()=>{
                
                var qtd_imagens = <?= $produto->qtd_imagens ?>;

                if(qtd_imagens > 0){
                    carregar_lista()
                } else {
                    $('div#galeria').html('<p>O produto ainda não possui imagens.</p>')
                }

            })

            // Transições
            $('a#redirect_info, a#redirect_img').click(function() {
                $('html, body').animate({
                    scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 1000);
                return false;
            });

            $('#imgproduto').submit((e)=>{
                e.preventDefault()
                var formdata = new FormData();
                formdata.append('file', $('#imagem').prop('files')[0])
                formdata.append('request', $('#request2').val())
                formdata.append('id_produto', $('#id2').val())
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('admin/produto/imagem/adicionar')?>',
                    data: formdata, 
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        if(data.sucesso){
                            $('div#alert2').remove();
                            $('form#imgproduto button:submit').before($('<div id="alert2" class="alert card alert-success fade show"><strong>'+data.sucesso+'</strong></div>'))
                            setTimeout(()=>{
                                $("div#alert2").alert('close',()=>{
                                    $('div#alert2').remove(); 
                                })
                            },8000)
                            carregar_lista()
                        }
                        if(data.erro){
                            $('div#alert2').remove();
                            $('form#imgproduto button:submit').before($('<div id="alert2" class="alert card alert-danger fade show"><strong>'+data.erro+'</strong></div>'))
                            setTimeout(()=>{
                                $("div#alert2").alert('close',()=>{
                                    $('div#alert2').remove(); 
                                })
                            },8000)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

            $('#preco_custo, #preco_venda, #preco_promocional').mask('#.##0,00', {reverse: true}).focusout(function (e){ 
                if($(this).val().length == 2 || $(this).val().length == 1 ) {
                    var preco = $(this).val()
                    preco = $(this).val(preco+',00')
                }
            })
            $('#garantia').mask('00', {reverse: true})
            $('#caixa').mask('00000', {reverse: true})
            $('#ipi').mask('AAA', {reverse: true})
            $('#ncm').mask('AAAA.AA.AA', {reverse: true})
            $('#peso_kg').mask('#.000', {reverse: true}).focusout(function (e){ 
                if($(this).val().length <= 2 && $(this).val().length > 0) {
                    var kg = $(this).val()
                    kg = $(this).val(kg+'.000')
                } else if($(this).val().length == 3) {
                    var kg = $(this).val()
                    kg = $(this).val(kg[0]+kg[1]+'.'+kg[2]+'00')
                }
            })
            $('#altura_cm, #largura_cm, #profundidade_cm').mask('000.00', {reverse: true}).focusout(function (e){ 
                if($(this).val().length <= 2 && $(this).val().length > 0) {
                    var cm = $(this).val()
                    cm = $(this).val(cm+'.00')
                } else if($(this).val().length == 3) {
                    var cm = $(this).val()
                    cm = $(this).val(cm[0]+cm[1]+'.'+cm[2]+'.00')
                }
            })
            $('#quantidade').mask('000', {reverse: true})
            $(document).on('keydown', '#quantidade, #caixa', function(e) {
                if ([69].includes(e.keyCode)) {
                    e.preventDefault();
                }
            })
            
            $('select').selectize({
                sortField: 'text'
            });

            $('#infoproduto').submit(e => {
                e.preventDefault()

                let dados = $('#infoproduto').serialize();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('admin/produto/editar/'.$produto->id)?>",
                    data: dados,
                    dataType: "json",
                    success: data => {
                        $.each(data, (index, value) => {
                            if(!value){
                                $(`#${index}`).addClass('is-valid')
                                try{
                                    $(`select#${index}`).parent().find('div.selectize-input').addClass('form-control is-valid')                                
                                    $(`select#${index}`).parent().find('div.selectize-input').removeClass('is-invalid')
                                }catch(e){}                                    
                                $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                                $(`#${index}`).removeClass(`is-invalid`)
                                $(`#erro_${index}`).remove()
                            } else if(value.length > 0) {
                                $(`#erro_${index}`).remove()
                                $(`#${index}`).addClass('is-invalid')
                                $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                                try{
                                    $(`select#${index}`).parent().find('div.selectize-input').addClass('form-control is-invalid') 
                                    $(`select#${index}`).parent().find('div.selectize-input').removeClass('is-valid')
                                }catch(e){}
                                $(`#${index}`).removeClass(`is-valid`)
                                $(`select#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                $(`div.md-form`).find(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            }
                        })
                        try{
                            $('#infoproduto').find('.is-invalid').first().focus()
                            $('div#alert').remove();
                            $('button:submit').before($('<div id="alert" class="alert card alert-danger fade show"><strong>Erros foram encontrados.</strong></div>'))
                            setTimeout(()=>{
                                $("div#alert").alert('close',()=>{
                                    $('div#alert').remove(); 
                                })
                            },4000)
                        } catch (e){}
                        if(data.edit == 1){
                            $('h1').html(`${data["dados"].nome}`)
                            $('h4#descricao').html(`Editar produto ${data["dados"].nome}`)
                            $('div#alert').remove();
                            $('form#infoproduto button:submit').before($('<div id="alert" class="alert card alert-success fade show"><strong>Edição realizada com sucesso!</strong></div>'))
                            setTimeout(()=>{
                                $("div#alert").alert('close',()=>{
                                    $('div#alert').remove(); 
                                })
                            },2000)
                        }
                    },
                    error: erro => {
                        console.log('Erros foram encontrados: \n')
                        console.log(erro)
                    }
                });

            })

        })
        </script>