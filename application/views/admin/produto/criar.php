
                <div class="row mt-4">
                    <div class="col text-center">
                        <h1><?= $subtitulo ?></h1>
                        <h4 id="descricao"><?= $descricao ?></h4>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-xl-9 col-lg-10 col-md-12 mx-auto">
                        <div class="card">
                            <div class="card-body text-center px-lg-5 px-md-5 px-3">

                                <!-- Form -->
                                <form id="criar">
                                    
                                    <div class="row my-3">
                                        <div class="col">
                                            <h3><?=$subtitulo?></h3>
                                        </div>
                                    </div>

                                    <div class="row my-3">

                                        <div class="col-sm-4 col-md-4">
                                        <hr>
                                        </div>

                                        <div class="col-sm-4 col-md-4 text-center">
                                        <span class="text-muted">Informações do <?= ucfirst($rota)?></span>
                                        </div>

                                        <div class="col-sm-4 col-md-4">
                                        <hr>
                                        </div>

                                    </div>
                                    <!-- Nome -->
                                    <div class="form-row">
                                        <div class="col mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" id="nome" name="nome" maxlength="60" class="form-control">
                                                <label for="nome">Nome do <?=ucfirst($rota)?>*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Subcategoria -->
                                    <div class="form-row">
                                        <div class="col mx-auto">
                                            <label class="text-muted font-weight-light" for="id_subcategoria">Subcategoria*</label>
                                            <select class="form-control" name="id_subcategoria" id="id_subcategoria">
                                                <option value="" disabled selected>-- Selecione --</option>
<?php foreach($subcategorias as $subcategoria) { ?>
                                                <option value="<?= $subcategoria->id ?>"><?= $subcategoria->nome ?> (em <?=$subcategoria->categoria?> de <?=$subcategoria->departamento?>)</option>
<?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Descrição -->
                                    <div class="form-row">
                                        <div class="col mx-auto">
                                            <div class="md-form mt-2">
                                                <textarea class="md-textarea w-100" name="descricao" id="descricao" rows="3"></textarea>
                                                <label for="descricao">Descrição do <?=ucfirst($rota)?>*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Fabricante e Marca -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="fabricante" id="fabricante" maxlength="30" class="form-control">
                                                <label for="fabricante">Fabricante do <?=ucfirst($rota)?></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="marca" id="marca" maxlength="30" class="form-control">
                                                <label for="marca">Marca do <?=ucfirst($rota)?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modelo e Cor -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="modelo" id="modelo" maxlength="30" class="form-control">
                                                <label for="modelo">Modelo do <?=ucfirst($rota)?></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="cor" id="cor" maxlength="30" class="form-control">
                                                <label for="cor">Cor do <?=ucfirst($rota)?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Preços -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="preco_custo" id="preco_custo" maxlength="9"  class="form-control">
                                                <label for="preco_custo">Preço de custo (R$)*</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="preco_venda" id="preco_venda" maxlength="9" class="form-control">
                                                <label for="preco_venda">Preço de venda (R$)*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Garantia e Preço promocional-->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="preco_promocional" id="preco_promocional" maxlength="9" class="form-control">
                                                <label for="preco_promocional">Preço promocional (R$)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="garantia" id="garantia" maxlength="2" class="form-control">
                                                <label for="garantia">Garantia (mês)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="ipi" id="ipi" maxlength="3" class="form-control">
                                                <label for="ipi">IPI</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- SKU e EAN/GTIN-->
                                    <div class="form-row">
                                        <div class="col-md-7 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="sku" id="sku" maxlength="30" class="form-control">
                                                <label for="sku">SKU</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="ean_gtin" id="ean_gtin" maxlength="13" class="form-control">
                                                <label for="ean_gtin">EAN/GTIN</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- MPN/Part Number e NCM-->
                                    <div class="form-row">
                                        <div class="col-md-7 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="mpn_partnumber" id="mpn_partnumber" maxlength="20" class="form-control">
                                                <label for="mpn_partnumber">MPN/Part Number</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="ncm" id="ncm" maxlength="13" class="form-control">
                                                <label for="ncm">NCM</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Serial e caixa -->
                                    <div class="form-row">
                                        <div class="col-lg-6 col-md-8 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="serial" id="serial" maxlength="30" class="form-control">
                                                <label for="serial">Serial</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-4 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="caixa" id="caixa" maxlength="5" class="form-control">
                                                <label for="caixa">Caixa</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Peso e NCM-->
                                    <div class="form-row">
                                        <div class="col-lg-3 col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="peso_kg" id="peso_kg" maxlength="6" class="form-control">
                                                <label for="peso_kg">Peso (kg)</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="altura_cm" id="altura_cm" maxlength="6" class="form-control">
                                                <label for="altura_cm">Altura (cm)</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="largura_cm" id="largura_cm" maxlength="6" class="form-control">
                                                <label for="largura_cm">Largura (cm)</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="profundidade_cm" id="profundidade_cm" maxlength="6" class="form-control">
                                                <label for="profundidade_cm">Profundidade (cm)</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Endereço Estoque e Quantidade -->
                                    <div class="form-row">
                                        <div class="col-lg-6 col-md-8 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" name="endereco_estoque" id="endereco_estoque" maxlength="10" class="form-control">
                                                <label for="endereco_estoque">Endereço estoque</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-4 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="number" name="quantidade" id="quantidade" min="0" max="999" maxlength="3" class="form-control">
                                                <label for="quantidade">Quantidade*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Unidade -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <label class="text-muted font-weight-light" for="unidade">Unidade</label>
                                            <select class="form-control" name="unidade" id="unidade">
                                                <option value="" disabled selected>-- Selecione --</option>
                                                <option value="Peça">Peça</option>
                                                <option value="Par">Par</option>
                                                <option value="Dúzia">Dúzia</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Variação e Condição -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <label class="text-muted font-weight-light mt-2" for="disponibilidade">Disponibilidade*</label>
                                            <select class="form-control" name="disponibilidade" id="disponibilidade">
                                                <option value="" disabled selected>-- Selecione --</option>
                                                <option value="imediata">Imediata</option>
                                                <option value="programada">Programada</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <label class="text-muted font-weight-light mt-2" for="condicao">Condição*</label>
                                            <select class="form-control" name="condicao" id="condicao">
                                                <option value="" disabled selected>-- Selecione --</option>
                                                <option value="novo">Novo</option>
                                                <option value="usado">Usado</option>
                                                <option value="variação">Com variação</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Código e Status -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <label class="text-muted font-weight-light mt-2" for="codigo">Codigo*</label>
                                            <select class="form-control" name="codigo" id="codigo">
                                                <option value="" disabled selected>-- Selecione --</option>
                                                <option value="RTC">RTC</option>
                                                <option value="IPCI">IPCI</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <label class="text-muted font-weight-light mt-2" for="status">Status*</label>
                                            <select class="form-control" name="status" id="status">
                                                <option value="0" selected>Não publicado</option>
                                                <option value="1">Publicado</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Promoção e Destaque -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="form-group custom-control custom-checkbox mt-3">
                                                <input type="checkbox" class="custom-control-input" id="promocao" name="promocao[]">
                                                <label class="custom-control-label text-muted font-weight-light" for="promocao">Está em promoção?</label>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="request" id="request" value="criar">

                                    <!-- Enviar -->
                                    <button class="btn btn-primary btn-rounded btn-block my-4 waves-effect" type="submit">Cadastrar</button>

                                </form>
                                <!-- Form -->

                            </div>

                        </div>
                        
                    </div>
                </div>
                <script type="text/javascript">
                $(document).ready(function () {

                    $('#preco_custo, #preco_venda, #preco_promocional').mask('#.##0,00', {reverse: true}).focusout(function (e){ 
                        if($(this).val().length == 2 || $(this).val().length == 1 ) {
                            var preco = $(this).val()
                            preco = $(this).val(preco+',00')
                        }
                    })
                    $('#garantia').mask('00', {reverse: true})
                    $('#caixa').mask('00000', {reverse: true})
                    $('#ipi').mask('AAA', {reverse: true})
                    $('#ncm').mask('AAAA.AA.AA', {reverse: true})
                    $('#peso_kg').mask('#.000', {reverse: true}).focusout(function (e){ 
                        if($(this).val().length <= 2 && $(this).val().length > 0) {
                            var kg = $(this).val()
                            kg = $(this).val(kg+'.000')
                        } else if($(this).val().length == 3) {
                            var kg = $(this).val()
                            kg = $(this).val(kg[0]+kg[1]+'.'+kg[2]+'00')
                        }
                    })
                    $('#altura_cm, #largura_cm, #profundidade_cm').mask('000.00', {reverse: true}).focusout(function (e){ 
                        if($(this).val().length <= 2 && $(this).val().length > 0) {
                            var cm = $(this).val()
                            cm = $(this).val(cm+'.00')
                        } else if($(this).val().length == 3) {
                            var cm = $(this).val()
                            cm = $(this).val(cm[0]+cm[1]+'.'+cm[2]+'.00')
                        }
                    })
                    $('#quantidade').mask('000', {reverse: true})
                    $(document).on('keydown', '#quantidade', function(e) {
                        if ([69].includes(e.keyCode)) {
                            e.preventDefault();
                        }
                    })
                    
                    $('select').selectize({
                        sortField: 'text'
                    })

                    $('#criar').submit(e => {
                        e.preventDefault()

                        let dados = $('#criar').serialize();
                        
                        $.ajax({
                            type: "POST",
                            url: base_url+'/admin/produto/criar',
                            data: dados,
                            dataType: "json",
                            success: data => {
                                $.each(data, (index, value) => {
                                    if(!value){
                                        $(`#${index}`).addClass('is-valid')
                                        try{
                                            $(`select#${index}`).parent().find('div.selectize-input').addClass('form-control is-valid')                                
                                            $(`select#${index}`).parent().find('div.selectize-input').removeClass('is-invalid')
                                        }catch(e){}
                                        $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                                        $(`#${index}`).removeClass(`#${index} is-invalid`)
                                        $(`#erro_${index}`).remove()
                                    } else if(value.length > 3) {
                                        $(`#erro_${index}`).remove()
                                        $(`#${index}`).addClass('is-invalid')
                                        $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                                        try {
                                            $(`select#${index}`).parent().find('div.selectize-input').addClass('form-control is-invalid') 
                                            $(`select#${index}`).parent().find('div.selectize-input').removeClass('is-valid')
                                        }catch(e){}
                                        $(`select#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                        $(`div.md-form`).find(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                    }
                                })
                                try{
                                    $('#criar').find('.is-invalid').first().focus()
                                    $('div#alert').remove();
                                    $('button:submit').before($('<div id="alert" class="alert card alert-danger fade show"><strong>Erros foram encontrados.</strong></div>'))
                                    setTimeout(()=>{
                                        $("div#alert").alert('close',()=>{
                                            $('div#alert').remove(); 
                                        })
                                    },4000)
                                } catch (e){}
                                if(data.redirect){
                                    $('div#pagina').html('<div class="alert my-5 card alert-success alert-dismissible fade show" role="alert">Produto criado com sucesso. Você será redirecionado à lista em 2 segundos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')

                                    setTimeout(function(){
                                        $.ajax({
                                            type: "post",
                                            url: data.redirect,
                                            data: "request=admin",
                                            dataType: "json",
                                            success: data => {
                                                if($(location).attr('pathname') == '/admin/produto/criar') {
                                                    $(document).attr('title', data.titulo)
                                                    $('#pagina').html(data.html);
                                                    $('#pagina').find('input').first().focus()
                                                }
                                            },
                                            error: erro => {
                                                console.log('Erros foram encontrados:');
                                                console.log(erro);
                                            }
                                        });
                                    },2000)
                                }
                            },
                            error: erro => {
                                console.log('Erros foram encontrados: \n')
                                console.log(erro)
                            }
                        })
                    })
                })
                </script>