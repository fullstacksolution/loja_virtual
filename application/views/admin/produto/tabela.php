                <div class="row my-5">

                    <div class="col-md-12 mx-auto text-center">

                        <span class="display-4 d-none d-md-block"><?=ucfirst($subtitulo)?>s</span>
                        <h2 class="d-md-none d-block"><?=ucfirst($subtitulo)?>s</h2>
                        <blockquote class="blockquote text-muted"><?=$descricao?></blockquote>

                    </div>

                </div>

                <div class="row mb-5">

                    <div class="col-auto mx-auto px-auto border shadow bg-white rounded text-center py-3">
                     
                        <table id="<?=$subtitulo?>" class="table table-striped table-hover table-sm table-responsive">
                            <thead>
                                <tr>
                                    <th class="th-sm" scope="col">ID</th>
                                    <th class="th-sm" scope="col">Produto</th>
                                    <th class="th-sm" scope="col">Categoria</th>
                                    <th class="th-sm" scope="col">Subcategoria</th>
                                    <th class="th-sm d-none">Outros</th>
                                    <th class="th-sm" scope="col">Qtd.</th>
                                    <th class="th-sm" scope="col">Status</th>
                                    <th class="th-sm" scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach($produtos as $produto) { ?>
                                <tr>
                                    <th><?= $produto->id?></th>
                                    <th><?= $produto->nome?> </th>
                                    <th><?= $produto->categoria?> de <?=$produto->departamento?> </th>
                                    <th><?= $produto->subcategoria?> </th>
                                    <th class="d-none"><?= $produto->fabricante?> <?= $produto->marca?> <?= $produto->modelo?> <?= $produto->codigo?>  <?= $produto->descricao?></th>
                                    <th><?= $produto->quantidade?> </th>
                                    <th><?= ($produto->status) ? 'Publicado' : 'Não publicado' ?> </th>
                                    <td>
                                        <a class="a-hover p-2" title="Editar <?=$subtitulo?>" id="editar<?=$subtitulo?><?=$produto->id?>" href="<?= base_url('admin/'.$subtitulo.'/editar/'.$produto->id) ?>">
                                            <i class="fas fa-pen text-warning mx-2"></i>
                                        </a>                          
                                        <a class="a-hover p-2" title="Deletar <?=$subtitulo?>" id="deletar<?=$subtitulo?><?=$produto->id?>" href="<?= base_url('admin/'.$subtitulo.'/deletar/'.$produto->id)?>">
                                            <i class="fas fa-times-circle text-danger mx-2"></i>
                                        </a>
                                    </td>
                                </tr>
<?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
                <script type="text/javascript">
                $(document).ready(function () {
                    var groupColumn = 2;
                    $('table#<?=$subtitulo?>').DataTable( {
                        "columnDefs": [
                                { "orderable": false, "targets": 1},
                                { "visible": false, "targets": groupColumn },
                                { "searchable": true, "targets": 2 }
                            ],                            
                        "language": {
                            "lengthMenu": "_MENU_ registros por página",
                            "zeroRecords": "Nenhum Registro Encontrado",
                            "info": "Total de _MAX_ <?=$subtitulo?>(s), página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro Disponível",
                            "infoFiltered": "",
                            "search": "Busca:",
                            "pagingType": "full_numbers",
                            "paginate": {
                                "next": "Próximo",
                                "previous": "Anterior"
                            }
                        },   
                        "order": [[ groupColumn, 'asc' ]], 
                        "displayLength": 25,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;
                
                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="grey lighten-2"><td colspan="6">'+group+'</td></tr>'
                                    );
                
                                    last = group;
                                }
                            } );
                        }
                    })
                    $('table').css('width', 'auto')
                    $('div.row.mb-5 div.py-3').removeClass('border bg-white rounded shadow').addClass('card')
                })
                    var produtos = <?= json_encode($produtos) ?>;
                    
                    $.each(produtos, (index, produto) => {

                        $('a#deletar<?=$subtitulo?>'+produto.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/deletar/') ?>${produto.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/deletar/'+produto.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/deletar/"+produto.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                        $('a#editar<?=$subtitulo?>'+produto.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `<?= base_url('admin/'.$subtitulo.'/editar/') ?>${produto.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/<?=$subtitulo?>/editar/'+produto.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/<?=$subtitulo?>/editar/"+produto.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                    })
                    
                </script>