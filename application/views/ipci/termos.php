<div class="container">
    <div class="row mt-5">
        <div class="col-lg-10 col-md-12 mx-auto">
            <div class="card">
                <div class="card-body px-lg-5 px-md-5 px-3">

                        
                    <div class="py-1">
                        <a class="text-ipci-primary h6" href="<?= base_url('loja') ?>" title="Voltar à loja">Voltar à loja</a>
                    </div>

                    <!-- Form -->
                    <form class="text-center" method="post">

                        <div class="row">
                            <div class="col-8 mx-auto">
                                <a class="p-4" title="Página principal" href="<?= base_url('loja') ?>" >
                                    <h2 class="text-hide my-auto">
                                        IPCI
                                        <img class="img-fluid img-hover" src="<?= base_url('assets/img/logotipo_ipci.png')?>" alt="IPCI">
                                    </h2>
                                </a>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <h3>Políticas e os Termos de uso</h3>
                            </div>
                        </div>
                        <div class="page-title">
    </div>
        <div class="std"><p class="ng-binding ng-scope">&nbsp;</p>
            <p class="text-justify">Este documento é parte integrante dos Termos de Uso e Condições do site www.ipci.com.br, caso haja conflito entre as disposições de ambos os documentos, deve prevalecer o disposto no mais específico.</p>
            <p class="text-justify">Política de Compra - O procedimento de compra no website WWW.IPCI.COM.BR será regulado por meio deste documento. Por isso, é indispensável seu conhecimento antes de um visitante se cadastrar e concordar com os regulamentos antes de finalizar seu pedido para aproveitar da melhor forma possível a facilidade oferecida através deste website. </p>
            <p class="text-justify"> Definições - Para fins deste Regulamento, aplicam-se as seguintes definições:  </p>
            <p class="text-justify ">1-Autenticação: Ato de o visitante inserir seu nome de usuário e senha para ter acesso às áreas e funcionalidades restritas do website;</p>
            <p class="text-justify">2-Clientes: Pessoas ou Empresas que efetuaram aquisição de produtos ou serviços oferecidos pelo site www.ipci.com.br;  </p>
            <p class="text-justify">3-Conta de Acesso: Credencial digital formada pela combinação de login e senha, única e intransferível para cada usuário no site, que dará permissão ao usuário ingressar na área Minha Conta e realizar compras;</p>
            <p class="text-justify">4-IPCI Soluções e Projetos: Designa o endereço eletrônico www.ipci.com.br e seus subdomínios; </p>
            <p class="text-justify">5-Session ID: Identificação da sessão do visitante, iniciada na sua autenticação com sucesso;</p>
            <p class="text-justify">6-WebSite: Página de internet; </p>
            <p class="text-justify">7-Usuário: Todo visitante autenticado no Website; </p>
            <p class="text-justify">8-Visitante: Qualquer pessoa que navegar pelo Website. </p>
            <h5 class="text-justify h5">Funcionamento da loja virtual</h5>
            <p class="text-justify">O site www.ipci.com.br oferece a seus visitantes a facilidade de adquirirem produtos e soluções através de sua loja virtual, bastando preencherem o cadastro do site que estarão aptos a realizar seus pedidos. 
            Para concluir um pedido, o visitante deve estar devidamente autenticado conforme solicitado pelo sistema com o uso de sua conta de acesso.</p>
            <p class="text-justify">Ao selecionar os produtos ou soluções que deseja adquirir, o usuário deve adicionar os mesmos ao seu carrinho virtual, e tão logo esteja satisfeito, procederá com a finalização de seu pedido, indicando o local de entrega e efetuando o pagamento correspondente. 
Depois de finalizado o pedido e indicada sua forma de pagamento, este será processado para aprovação ou cancelamento. 
Caso aprovado, haverá a entrega pelo método escolhido. </p>
            <p class="text-justify">O pedido pode ser cancelado caso alguma das hipóteses abaixo ocorra:</p>
            <p class="text-justify">
                <ul>
                    <li class="text-justify mr-3">Não pagamento do boleto(dentro do prazo bancario em media 3 dias uteis) ou insuficiência de saldo ou crédito no cartão;</li>
                    <li class="text-justify mr-3">Inconsistência nos dados fornecidos pelo cliente; -Erro na formação do pedido em razão de defeitos no dispositivo do cliente;</li>
                    <li class="text-justify mr-3">Motivos que tornem o cumprimento do pedido impossível, pela quebra de confiança ou uso de má-fé.</li>
                    <li class="text-justify mr-3">Caso haja divergência de preços entre os anúncios exibidos nas páginas dos produtos e o constante no carrinho de compras, deverá prevalecer este último.</li>
                 </ul></p>
                 <h5 class="text-justify h5">Aquisição de produtos </h5>
                 <p class="text-justify">Os produtos podem ser adquiridos na loja virtual www.ipci.com.br através de um dos meios de pagamento oferecidos pelo site no momento do fechamento do carrinho virtual. 
Assim que o usuário concluir o pedido, este passará pelos estágios a seguir, sendo possível a verificação de seu andamento no site: </p>
                 <p class="text-justify">
                <ol type="a">
                    <li class="text-justify mr-3">Aguardando Pagamento: Comprova o registro do pedido nos sistemas www.ipci.com.br, mas o pagamento ainda consta como Pendente (pagamento não realizado ou ainda não aprovado pela gestora de pagamento); </li>
                    <li class="text-justify mr-3">Processando Pagamento: O site www.ipci.com.br realizará a checagem dos dados cadastrais do Cliente e analisará o pagamento efetuado. </li>
                    <li class="text-justify mr-3">Pedido Aprovado: Significa que o pedido foi aprovado e será enviado ao cliente atravez do meio escolhido; </li>
                    <li class="text-justify mr-3">Pedido em separação/preparação: Nessa etapa o pedido já se encontra em separação ou confecção do pedido feito; </li>
                    <li class="text-justify mr-3">Pedido Concluído: Por fim, o site www.ipci.com.br informará por e-mail ao cliente o(s) código(s) de rastreamento(s) do seu pedido, comprovando assim o envio do mesmo. </li>
                    <li class="text-justify mr-3">Pedido Cancelado: Impossibilidade de processamento do pedido por alguma razão específica, podendo variar entre:
                        <ol type="I">
                        <li class="text-justify mr-3">Não liberação em virtude de inconsistência de algum dado ou informação cadastral; </li>
                        <li class="text-justify mr-3">Não aprovação do débito junto a Administradora do cartão de crédito; </li>
                        <li class="text-justify mr-3">Ausência de pagamento do boleto; </li>
                        <li class="text-justify mr-3">Não concretização do débito em conta, caso a opção seja de débito automático; </li>
                        <li class="text-justify mr-3">Divergencia de estoque bem como algum motivo que impossibilitem o processamento do pedido. </li>
                        </ol>
                    </li>
                </ol></p>
                <p class="text-justify">Caso o pedido finalizado não puder ser entregue em razão de indisponibilidade de estoque, o site www.ipci.com.br fará contato com o comprador através do metodo escolhido pelo cliente para buscar a melhor solução. Na hipótese de possibilidade de reposição de estoque, o comprador poderá optar por desistir da compra, mediante ressarcimento do valor pago, se for o caso, ou por aguardar a entrega do produto, em novo prazo, a ser informado pelo site www.ipci.com.br. </p>
                <p class="text-justify">Na hipótese de impossibilidade de reposição de estoque, a compra será cancelada, ficando o site www.ipci.com.br, obrigado a informar imediatamente o comprador acerca do cancelamento, bem como a efetuar o ressarcimento do valor pago, se for o caso. </p>
                <h5 class="text-justify h5">Privacidade</h5>
                <p class="text-justify">O site www.ipci.com.br, não pratica venda de dados nem compartilha dados dos seus clientes sem a devida comunicação e autorização expressa dos mesmos. 
                Atendimento ao usuário/Troca e Devoluções Caso tenha qualquer dúvida em relação ao presente documento, favor entrar em contato: </p>
                <p class="text-justify">
                <ul>
                    <li class="text-justify mr-3">Via Site acessando o endereço eletronico www.ipci.com.br, clicando em “acompanhe seu pedido”, preenchendo os campos solicitados no site e clicando em “enviar mensagem”.</li>
                    <li class="text-justify mr-3">Via e-mail, enviando e-mail para atendimento@ipci.com.br. </li>
                    <li class="text-justify mr-3">Via Telefone, enviando mensagem de texto, pelo aplicativo, para o número (11) 94500-0300.</li>
                 </ul></p>
                <p class="text-justify">Lembrando que os contatos feitos terão como parametro os horários abaixo e terá como padrão o prazo máximo de 2 hora para resposta do mesmo, de Segunda a sexta feira das 09:00 às 17:00, feito fora destes dias e horários de atendimento serão respondidos no próximo dia útil. </p>
            
                     </div>
                   </div>
                </div>
             </div>
                            
        </div>
    </div>
</div>

 

 

         


 

 



