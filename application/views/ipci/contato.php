        <div class="container">

            <div class="row mt-4">
                <div class="col-md-6 mx-auto text-center">
                    <h2>Contato</h2>
                </div>
            </div>

            <?php ($this->session->userdata('logado')) ? $user = $this->session->userdata('userlogado') : $user = 0 ?>

            <div class="row mt-4">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="card">
                        <div class="card-body px-lg-5 px-md-5 px-3">

                            <form id="contato">
                            
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="nome" id="nome" <?= ($user) ? 'value="'.$user->nome.'"' : '' ?>>
                                            <label for="nome">Nome*</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="sobrenome" id="sobrenome" <?= ($user) ? 'value="'.$user->sobrenome.'"' : '' ?>>
                                            <label for="sobrenome">Sobrenome</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="md-form">
                                            <input class="form-control" type="email" name="email" id="email" <?= ($user) ? 'value="'.$user->email.'"' : '' ?>>
                                            <label for="email">Email*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="telefone" id="telefone" <?= ($user) ? 'value="'.$user->telefone.'"' : '' ?>>
                                            <label for="telefone">Telefone</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="celular" id="celular" <?= ($user) ? 'value="'.$user->celular.'"' : '' ?>>
                                            <label for="celular">Celular</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col mx-auto text-center">
                                        <label for="tipo" class="text-muted">Tipo de mensagem*</label>
                                        <select class="form-control" name="tipo" id="tipo">
                                            <option value="" disabled selected>-- Selecione um tipo --</option>
                                            <option value="Dúvida">Dúvida</option>
                                            <option value="Sugestão">Sugestão</option>
                                            <option value="Reclamação">Reclamação</option>
                                            <option value="Serviços">Serviços</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col">
                                        <div class="md-form">
                                            <input class="form-control" type="text" name="assunto" id="assunto">
                                            <label for="assunto">Assunto*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col mx-auto text-center">
                                        <div class="md-form">
                                            <textarea class="md-textarea w-100 form-control" name="mensagem" id="mensagem" rows="3"></textarea>
                                            <label for="mensagem">Mensagem*</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-6 mx-auto text-center mt-4">
                                        <label for="meio" class="text-muted">Qual meio gostaria de ser respondido?*</label>
                                        <select class="form-control" name="meio" id="meio">
                                            <option value="" disabled selected>-- Selecione um meio --</option>
                                            <option value="Email">Email</option>
                                            <option value="Telefone">Telefone</option>
                                            <option value="WhatsApp">WhatsApp</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 mx-auto text-center mt-4">
                                        <label class="text-muted" for="horario">Melhor horário para entrar em contato?</label>
                                        <input class="form-control horario" min="09:00" max="17:00" type="time" name="horario" id="horario">
                                        <p class="font-small text-muted">O horário de atendimento é de <br> Segunda a Sexta-feira das 09:00 às 17:00.</p>
                                    </div>
                                </div>

                                
                                <div class="form-row mt-4">
                                    <div class="col mx-auto text-center">
                                        <div class="form-group custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cc" name="cc">
                                            <label class="custom-control-label text-muted font-weight-light" for="cc">Enviar uma cópia para seu email?</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-xl-6 col-lg-7 col-sm-8 col mx-auto text-center">
                                        <div id="g-recaptcha" class="g-recaptcha" data-sitekey="6Ld48sEUAAAAAIdyJ_6Hh1emFERWxPK4jMIy4wh9"></div>
                                    </div>
                                </div>

                                <button class="btn btn-primary btn-block btn-lg" type="submit">Enviar mensagem</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript" src="<?=base_url('assets/js/contato.js')?>"></script>
