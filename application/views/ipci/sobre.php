<!-- Barra de navegação -->
<nav class="navbar navbar-expand-lg navbar-dark ipci-primary-color-dark shadow-none">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="wow fadeIn navbar-nav mx-auto">
            <li class="nav-item mx-auto">
                <a class="nav-link" href="<?= base_url('loja')?>">Loja Virtual</a>
            </li>
            <li class="nav-item mx-auto">
                <a class="nav-link" href="<?= base_url('servicos')?>">Serviços</a>
            </li>
            <li class="nav-item mx-auto">
                <a class="nav-link" href="<?= base_url('contato')?>">Contato</a>
            </li>
        </ul>
    </div>
</nav>

<!-- Banner principal -->
<div class="mb-5 bg-ipci-primary-dark">
    <div class="col-md-12 mx-auto text-center container py-5">
        <div class="my-5 pb-5 pt-1 px-5">
            <h1 class="wow fadeIn my-5 text-white">A empresa</h1>
            <img class="wow fadeIn img-fluid mb-5 w-auto" alt="IPCI" src="<?= base_url('assets/img/logotipo_ipci_w.png')?>" >
        </div>
    </div>
</div>

<!-- Conteúdo -->
<div class="container">
    <div class="row mb-5 wow fadeIn">
        <div class="col-md-6 mx-auto my-auto">
            <h2 class="h2">Quem somos?</h2>
            <p>
            Somos uma pequena empresa de comércio e prestação de serviços na área de Automação Industrial e Eletrônica.
            </p>
        </div>
        <div class="col-md-6 mx-auto my-auto text-center">
            <img class="img-fluid mb-4 w-75 " alt="IPCI" name="IPCI" src="<?= base_url('assets/img/ipci.jpg')?>" >
        </div>
    </div>
</div>

<hr class="mb-5">

<div class="container">
    <div class="row mb-5 wow fadeIn">
        <div class="col-md-6 mx-auto text-center  align-self-center d-none d-md-block">
            <img class="img-fluid w-50 wow fadeIn mb-4 my-5" alt="Instituto Educacional" src="<?= base_url('assets/img/instituto.png')?>">
        </div>
        <div class="col-md-6 mx-auto my-auto">
            <h2 class="h2">História</h2>
            <p class="text-justify" > 
                Uma iniciativa criada pelo Sr. Carlos Gonzales, que foi professor de escolas técnicas no seu país de origem. Sempre focado com o compromisso de trabalhar nessa área de desenvolvimento, seu objetivo é realizar soluções didáticas para alunos e soluções sob medida para empresas.
            </p>
        </div>
        <div class="col-md-6 mx-auto d-sm-block d-md-none text-center">
            <img class="img-fluid mb-5 w-75 p-3 my-5 wow fadeIn mb-4" alt="Instituto Educacional" src="<?= base_url('assets/img/instituto.png')?>">
        </div>
    </div>
</div>

<hr class="mb-5">

<div class="container">
    <div class="row mb-5 wow fadeIn">
        <div class="col-md-4 mx-auto">
            <i class="mb-2 far fa-handshake fa-6x text-ipci-primary"></i>
            <h2 class="h2 text-justify">Missão</h2>
            <p>
                Permitir o acesso dos clientes às nossas soluções em eletrônica, mecatrônica e automação industrial.
            </p>
        </div>
        <div class="col-md-4 mx-auto">
        <i class="mb-2 fas fa-bullseye fa-6x text-ipci-primary"></i>
            <h2 class="h2 text-justify">Visão</h2>
            <p>
                O sonho de todo professor é de realizar ao longo do tempo a evolução no aprendizado, uma luta na qual parece ser invencível, acredita-se que não se ganha esta luta, porém participar dela nos dá o mérito, e isso nos permite aprimorar constantemente.
            </p>
        </div>
        <div class="col-md-4 mx-auto">
        <i class="mb-2 fas fa-thumbtack fa-6x text-ipci-primary"></i>
            <h2 class="h2 text-justify">Valores</h2>
            <p>
                Zelar o respeito e a confiança de nossos parceiros e colaboradores e inovar com responsabilidades.
            </p>
        </div>
    </div>
</div>

<hr class="mb-5">