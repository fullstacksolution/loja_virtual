<div class="container">

    <div class="row mt-5">
        <div class="col-lg-10 col-md-12 mx-auto">~

			<!-- Card -->
            <div class="card">
                <div class="card-body px-lg-5 px-md-5 px-3">

					<!-- row 1 -->
					<div class="row">
						<div class="col">
							
							<div class="row">
								<div class="col mx-auto text-center my-4">
									<h2 class="h3 text-center">Serviços</h2>
									<h5>Confira alguns de nosso serviços abaixo</h5>	
								</div>
							</div>

							<!-- row 2 -->
							<div class="row mb-5 mt-3">
							
								<div class="col-md-6">
									<div class="row">
										<div class="col-2">
											<i class="fas fa-check-circle fa-2x text-primary"></i>
										</div>
										<div class="col-10">
											<h5>Pesquisa</h5>
											<p class="dark-grey-text text-justify">É realizada uma busca atualizada com o que tem de mais inovador no mercado em modulos eletronicos no seu segmento de atuação, fazemos o estudo da adequação e viabilidade tecnica para adaptar o projeto ao seu parque fabril.
											</p>
											<div style="height:15px"></div>
										</div>
									</div>

									<div class="row">
										<div class="col-2">
											<i class="fas fa-check-circle fa-2x text-primary"></i>
										</div>
										<div class="col-10">
											<h5>Desenvolvimento de Protótipos</h5>
											<p class="dark-grey-text text-justify">A prototipagem é essencial no processo de desenvolvimento de um produto. É nesse estagio que se verifica se todas as funcionalidades imaginadas estão sendo atendidas bem como desempenho e segurança funcional. Nessa etapa é possível identificar as falhas e melhorias a serem realizadas tanto no projeto quanto nos conceitos do produto. Transformar atravez de uma processo artesanal, uma ideia em um produto funcional, eficiente e com custo beneficio acessivel é o objetivo desta etapa.
											</p>
											<div style="height:15px"></div>
										</div>
									</div>

									<div class="row">
										<div class="col-2">
											<i class="fas fa-check-circle fa-2x text-primary"></i>
										</div>
										<div class="col-10">
										<h5>Montagem de Placas Eletrônicas</h5>
										<p class="dark-grey-text text-justify">A experiencia de decadas em elaborar Placas de Circuitos Eletronicos, faz com que se desenvolva a eficiencia de operação tanto em funcionamento 
										quanto na elaboração.
										</p>
										<div style="height:15px"></div>
										</div>
									</div>
								</div>
									
									
								<div class="col-md-6 ">
									<div class="row">
										<div class="col-2">
											<i class="fas fa-check-circle fa-2x text-primary"></i>
										</div>
										<div class="col-10">
											<h5>Confidencialidade</h5>
											<p class="dark-grey-text text-justify">Desenvolvemos projetos com contrato de confidencialidade, assim temos o compromisso de garantir o sigilo e proteger suas ideias.
											</p>
											<div style="height:15px"></div>
										</div>
									</div>
									<div class="row">
										<div class="col-2">
												<i class="fas fa-check-circle fa-2x text-primary"></i>
											</div>
											<div class="col-10">
												<h5>Desenvolvimento de Hardware</h5>
												<p class="dark-grey-text text-justify">O desenvolvimento de hardware envolve tecnologias novas com recursos e sistemas inovadores. Desenvolver um projeto com dedicação é o caminho que nos leva a satisfação dos nossos clientes e parceiros.</p>
												<div style="height:15px"></div>
											</div>
										</div>
									
									<div class="row">
										<div class="col-2">
											<i class="fas fa-check-circle fa-2x text-primary"></i>
										</div>
										<div class="col-10">
											<h5>Manuntenção</h5>
											<p class="dark-grey-text text-justify">Mais do que ter a solução ao seu alcance, sabemos que é necessária a manutenção dos seus equipamentos, por isso contamos com nossa experiencia 
											para poder oferecer ou indicar planos de manutenção corretiva, preventiva, preditiva, detectivaonde encaminhamos os nossos relatorios ao
											Depto de Engenharia de Manutenção de cada cliente.
												</p>
											<div style="height:15px"></div>
										</div>
									</div>
								</div>


								<div class="row mx-md-5">
									<div class="col-12 mx-auto text-center">
										<h4>Alguns tipos de Manuntenção</h4>
										<ul class="list-group mt-4">
											<li class="list-group-item text-justify">
												<h5>Manutenção Corretiva</h5>
												<p>É a forma mais óbvia e mais primária de manutenção. Pode sintetizar-se pelo ciclo "quebra-repara", ou seja, o reparo dos equipamentos após a avaria.</p>
											</li>
											<li class="list-group-item text-justify">
												<h5>Manutenção Preventiva</h5>
												<p>Como o próprio nome sugere, consiste em um trabalho de prevenção de defeitos que possam originar a parada ou um baixo rendimento dos equipamentos em operação.</p>
												</li>
											<li class="list-group-item text-justify">
												<h5>Manutencao Preditiva</h5>
												<p>É a atuação realizada com base em modificação de parâmetro de CONDIÇÃO ou DESEMPENHO, cujo acompanhamento obedece a uma sistemática.</p>
											</li>
											<li class="list-group-item text-justify">
												<h5>Manutenção Detectiva</h5>
												<p>É a atuação efetuada em sistemas de proteção buscando detectar FALHAS OCULTAS ou não-perceptíveis ao pessoal de operação e manutenção.</p>
											</li>  
											<li class="list-group-item text-justify">
												<h5>Engenharia de Manutenção</h5>
												<p>É uma nova concepção que constitui a segunda quebra de paradigma na manutenção. Praticar engenharia de manutenção é deixar de ficar consertando continuadamente, para procurar as causas básicas, modificar situações permanentes de mau desempeno, deixar de conviver com problemas crônicos, melhorar padrões e sistemáticas, desenvolver a manutenibilidade, das feedback ao projeto, interferir tecnicamente nas compras.</p>
											</li>
										</ul>
									</div>
								</div>
										
							</div>
							<!-- ;row 2 -->


						</div>
                	</div>
					<!-- ;row 1 -->

					<!-- row 3 -->
					<div class="row mx-auto">
						<div class="col mx-auto text-center">
							<hr class="mb-4">
								<h5>Deseja solicitar um serviço ou um orçamento?</h5>   
								<h5><a class="text-ipci-primary" href="<?= base_url('contato')?>">Entre em contato por aqui</a></h5>
							<hr class="mt-4">
						</div>
					</div>
					<!-- ;row 3 -->

                </div>
            </div>
			<!-- ;Card -->

        </div>
    </div>
</div>








