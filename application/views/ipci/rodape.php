		<!-- Rodapé -->
		<footer class="page-footer wow fadeIn">
			<div class="container text-center text-md-left mt-5">
				<div class="row mt-3 text-dark">

					<!-- Brand da empresa -->
					<div class="col-md-3 col-lg-4 col-xl-3 mb-4">

						<h6 class="text-uppercase font-weight-bold">
							<img class="img-fluid h-50 w-50" src="<?= base_url('assets/img/logotipo_ipci.png') ?>" alt="IPCI">
						</h6>
						<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto">
						<p>
						Somos uma pequena empresa de comércio e prestação de serviços na área de Automação Industrial e Eletrônica.
						</p>
						<p>
							<a class="text-ipci-nocolor" href="https://www.facebook.com/ipcioficial/" target="_blank">
							<i class="fab fa-facebook-square fa-2x mr-2 "></i>
							</a>
						</p>

					</div>

					<!-- 
					<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

						<h6 class="text-uppercase font-weight-bold">xx</h6>
						<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
						<p>
							<a class="text-dark" href="#!">xx</a>
						</p>
						<p>
							<a class="text-dark" href="#!">xx</a>
						</p>
						<p>
							<a class="text-dark" href="#!">xx</a>
						</p>
						<p>
							<a class="text-dark" href="#!">xx</a>
						</p>

					</div> 
					-->

					<!-- Links Ajuda -->
					<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

						<h6 class="text-uppercase font-weight-bold">Ajuda</h6>
						<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('sobre') ?>">Sobre</a>
						</p>
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('servicos') ?>">Serviços</a>
						</p>
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('contato') ?>">Relatar um problema ou sugestão</a>
						</p>
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('termos') ?>">Políticas e termos de uso</a>
						</p>
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('mapa') ?>">Mapa do site</a>
						</p>
						<p>
						<a class="text-ipci-nocolor" href="<?= base_url('contato') ?>">Contato</a>
						</p>

					</div>

					<!-- Informações de Contato -->
					<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

						<h6 class="text-uppercase font-weight-bold">Contato</h6>
						<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
						<p>
							<i class="fas fa-home mr-3"></i>Rua Paculé, 63, Jd. Jovaia, Guarulhos - SP</p>
						<p>
							<i class="fas fa-bell mr-3"></i>Horários de funcionamento: de Segunda a Sexta-feira das 09:00 às 17:00
						</p>
						<p>
							<i class="fas fa-envelope mr-3"></i><a class="text-ipci-nocolor" href="mailto:atendimento@ipci.com.br">atendimento@ipci.com.br</a> 
						</p>
						<p>
							<i class="fas fa-phone mr-3"></i><a href="tel:1141754777" class="text-ipci-nocolor">(11) 4175-4777</a>
						</p>
						<p>
							<i class="fab fa-whatsapp mr-3"></i><a href="https://api.whatsapp.com/send?phone=5511945000300" class="text-ipci-nocolor" target="_blank">(11) 94500-0300</a>
						</p>

					</div>
				</div>
			</div>

			<div class="text-center text-dark py-3">
				<a class="text-dark" href="https://www.ipci.com.br">IPCI</a> © <?= date('Y') ?> | Todos os direitos reservados.
				<br>
				Site desenvolvido por <a class="text-dark" href="https://www.fullstacksolutions.tk" title="Fullstack Solutions"target="_blank" rel="Fullstack Solutions">Fullstack Solutions</a>
			</div>

		</footer>

