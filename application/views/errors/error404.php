<div class="container">
    <div class="row" style="height: 100vh;">
        <div class="col-md-8 mx-auto flex-center flex-column">

            <h1 class="text-hide animated fadeIn mb-4" style="background-image: url('<?= base_url('assets/img/logotipo_ipci.png')?>'); background-size: cover; width: 236px; height: 100px;">IPCI
            </h1>
            <h2 class="h2 animated fadeIn mb-3 text-center">Erro 404, página não encontrada.</h2>
            <h5 class="animated fadeIn mb-3">Se você digitou o URL diretamente, por favor, certifique-se a ortografia está correta. <br> Se você clicou em um link para chegar até aqui, o link está desatualizado. </h5>
            <a class="text-ipci-primary h5 my-2" href="<?= base_url('loja') ?>" title="Voltar à loja">Voltar à loja</a>
            <?= (isset($_SERVER['HTTP_REFERER'])) ? '<a class="text-ipci-primary h5 my-2" onclick="paginaanterior()" title="Página anterior">Página anterior</a>' : '' ?>
        </div>
    </div>
</div>
<script>
    function paginaanterior() {
        window.history.back();
    }
</script>