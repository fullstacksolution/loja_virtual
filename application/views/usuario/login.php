<div class="container">
    <div class="row mt-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <div class="card">
                <div class="card-body px-lg-5 px-md-5 px-3">

                        
                    <div class="py-1">
                        <a class="text-ipci-primary h6" href="<?= base_url('loja') ?>">Voltar à loja</a>
                    </div>

                    <!-- Form -->
                    <form id="login" class="text-center">

                        <div class="row">
                            <div class="col-8 mx-auto">
                                <a class="p-4" title="Página principal" href="<?= base_url('loja') ?>" >
                                    <h2 class="text-hide my-auto">
                                        IPCI
                                        <img class="img-fluid img-hover" src="<?= base_url('assets/img/logotipo_ipci.png')?>" alt="IPCI">
                                    </h2>
                                </a>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <h3>Login</h3>
                            </div>
                        </div>

                        <?php if(isset($_GET['cadastro'])) { ?>
                            <div class="alert card alert-white  alert-dismissible fade show" role="alert">
                                <strong>Cadastro realizado com sucesso!</strong> Agora você pode fazer login.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } else if(isset($_GET['status']) && $_GET['status'] == 'add_avaliacao' && isset($_GET['produto'])) { ?>
                            <div class="alert card alert-danger  alert-dismissible fade show" role="alert">
                                <strong>Você precisa estar logado para avaliar um produto.</strong> Faça login para continuar.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } else if(isset($_GET['status']) && $_GET['status'] == 'add_carrinho' && isset($_GET['produto'])) { ?>
                            <div class="alert card alert-danger  alert-dismissible fade show" role="alert">
                                <strong>Você precisa estar logado para adicionar um produto ao carrinho.</strong> Faça login para continuar.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } else if(isset($_GET['status']) && $_GET['status'] == 'page_carrinho') { ?>
                            <div class="alert card alert-danger  alert-dismissible fade show" role="alert">
                                <strong>Você precisa estar logado para ter acesso ao carrinho.</strong> Faça login para continuar.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>

                        <!-- Email -->
                        <div class="form-row">
                            <div class="col-md-12 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="email" id="email" name="email" class="form-control">
                                    <label for="email">E-mail</label>
                                </div>
                            </div>
                        </div>

                        <!-- Senha -->
                        <div class="form-row">
                            <div class="col-md-12 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="password" id="senha" name="senha" class="form-control">
                                    <label for="senha">Senha</label>
                                </div>
                            </div>
                        </div>

 
<?php // Redirect para páginas de produtos
    if(isset($_GET['status']) && isset($_GET['produto'])){
        if($_GET['status'] == 'add_avaliacao'){ 
?>
                        <input type="hidden" name="redirect" id="redirect" value="add_avaliacao">
<?php } else if($_GET['status'] == 'add_carrinho'){ ?>
                        <input type="hidden" name="redirect" id="redirect" value="add_carrinho">
<?php } ?>
                        <input type="hidden" name="id_produto" id="id_produto" value="<?=$_GET['produto']?>">
<?php } ?>

<?php // Redirect para páginas
    if(isset($_GET['status'])){ 
        if($_GET['status'] == 'page_carrinho'){
?>
                        <input type="hidden" name="redirect" id="redirect" value="page_carrinho">
<?php }} ?>



                                <!-- Lembrar-me -->
                        <div class="form-row">
                            <div class="col-md-6 text-left">
                                <!-- Esqueceu a senha? -->
                                <a class="font-small text-ipci-primary" href="">Esqueceu a senha?</a>
                            </div>
                            <!-- <div class="col-md-6">
                                <input type="checkbox" class="custom-control-input" id="lembrar" name="lembrar">
                                <label class="custom-control-label text-muted" for="lembrar">Lembrar-me</label>
                            </div> -->
                        </div>

                        <!-- Enviar -->
                        <button class="btn btn-primary btn-rounded btn-block my-4 waves-effect" type="submit">Entrar</button>

                        <!-- Cadastrar -->
                        <p>Ainda não possui uma conta?<br>
                            <a class="text-ipci-primary" href="<?= base_url('sign/up')?>">Criar conta</a>
                        </p>

                    </form>
                    <!-- Form -->

                </div>

            </div>
            
            <div class="text-right align-self-center">
                <ul class="list-inline p-2">
                    <li class="list-inline-item">
                        <a class="text-muted" href="<?= base_url('contato')?>">Contato</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="<?= base_url('termos')?>">Políticas e Termos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {

    $('#email').focus()
    
    $('#login').submit(e => {
        e.preventDefault()

        let dados = $('#login').serialize()

        $.ajax({
            type: 'POST',
            url: '<?= base_url('sign/auth')?>',
            data:  dados,
            dataType: 'json',
            success: data => {
                if(!data.email){
                    $('#email').removeClass('#email is-invalid')
                    $('#email').addClass('is-valid')
                    $('#erro_email').remove()
                } else {
                    $('#erro_email').remove()
                    $('#email').addClass('is-invalid')
                    $('#email').after($('<div id="erro_email" class="invalid-feedback d-block">'+data.email+'</div>'))
                    $('#email').focus()
                }
                if(!data.senha){
                    $('#senha').removeClass('#senha is-invalid')
                    $('#senha').addClass('is-valid')
                    $('#erro_senha').remove()
                } else {
                    $('#erro_senha').remove()
                    $('#senha').addClass('is-invalid')
                    $('#senha').after($('<div id="erro_senha" class="invalid-feedback d-block">'+data.senha+'</div>'));
                }
                try{
                    $('#login').find('.is-invalid').first().focus()
                } catch (e){}
                if(data.auth){
                    console.log(data.auth)
                    $(location).attr('href', data.redirect)
                }
            },
            error: erro => {
                console.log('Erros foram encontrados: \n')
                console.log(erro)
            }
        })

    })

})
</script>