        <div class="container">

            <div class="row mt-4">
                <div class="col text-center">
                    <h1>Olá, <?= $this->session->userdata('userlogado')->nome?> <?= $this->session->userdata('userlogado')->sobrenome?></h1>
                    <h4>Aqui você encontrará suas informações pessoais</h4>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-lg-6 mt-4 text-center mx-auto">

                    <!-- Card das informações do usuário -->
                    <div class="card">
                        <div class="card-body p-4">
                
                            <form id="infouser">
                            
                                <div class="row my-3">

                                    <div class="col text-center">
                                        <h4>Informações do Usuário </h4>
                                    </div>

                                </div>

                                <div class="form-row">

                                    <!-- Nome / Razão Social -->
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="text" id="nome" name="nome" maxlength="40" value="<?= $this->session->userdata('userlogado')->nome ?>" class="form-control">
                                            <label for="nome">Nome / Razão Social</label>
                                        </div>
                                    </div>

                                    <!-- Sobrenome -->
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="text" id="sobrenome" name="sobrenome" maxlength="40" value="<?= $this->session->userdata('userlogado')->sobrenome ?>" class="form-control">
                                            <label for="sobrenome">Sobrenome</label>
                                        </div>
                                    </div>
                                    
                                </div>
                                    
                                <div class="form-row">     
                                    <!-- Tel/Cel -->
                                    <div class="form-row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" id="telefone" name="telefone" maxlength="15" value="<?= $this->session->userdata('userlogado')->telefone ?>" class="form-control">
                                                <label for="telefone">Telefone</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mx-auto">
                                            <div class="md-form mt-2">
                                                <input type="text" id="celular" name="celular" maxlength="15" value="<?= $this->session->userdata('userlogado')->celular ?>" class="form-control">
                                                <label for="celular">Celular</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if($this->session->userdata('userlogado')->tipo >= 2) { ?>
                                    <input type="hidden" name="id" id="id1" value="<?= $this->session->userdata('userlogado')->id ?>">
                                <?php } ?>

                                <button class="btn btn-primary" type="submit">Salvar</button>

                            </form>
                        </div>

                    </div>

                    <div class="card mt-4">
                        <div class="card-body p-4">
                            <form id="altsenha">
                                    
                                <div class="row my-3">
                                    <div class="col text-center">
                                        <h4>Alterar senha</h4>
                                    </div>
                                </div>

                                <!-- Senha/Confirmação -->
                                <div class="form-row">
                                    <div class="col-md-12 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="password" id="senha" maxlength="30" name="senha" class="form-control" autocomplete="none" >
                                            <label for="senha">Senha atual</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="password" id="senhanova" maxlength="30" name="senhanova" class="form-control" autocomplete="none" >
                                            <label for="senha">Nova senha</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="password" id="confirmarsenha" maxlength="30" name="confirmarsenha" class="form-control" autocomplete="none" >
                                            <label for="confirmarsenha">Confirmar nova senha</label>
                                        </div>
                                    </div>
                                </div>

                                <?php if($this->session->userdata('userlogado')->tipo >= 2) { ?>
                                    <input type="hidden" name="id" id="id2" value="<?= $this->session->userdata('userlogado')->id ?>">
                                <?php } ?>

                                <button class="btn btn-primary" type="submit">Salvar</button>

                            </form>
                        </div>
                    </div>
                    
                </div>

                <div class="col-lg-6 text-center mx-auto mt-4">

                    <!-- Card dos endereços -->
                    <div class="card">
                        <div class="card-body p-4">
                
                            <form id="infoentrega">
                                
                                <div class="row my-3">
                                    <div class="col text-center">
                                        <h4>Endereços</h4>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-6 mx-auto mb-4">
                                        <label for="tipo">Tipo</label>
                                        <select class="custom-select" name="tipo" id="tipo">
                                            <option id="entrega" value="1">Entrega</option>
                                            <option id="cobranca" value="2">Cobrança</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 mx-auto">
                                        <div class="md-form mt-2 mb-0">
                                            <input type="text" class="form-control" size="15" maxlength="9" id="cep" name="cep" value="<?= (isset($endereco[0])) ? $endereco[0]->cep : '' ?>">
                                            <label for="cep">CEP</label>
                                        </div>
                                        <a class="text-ipci-primary font-small" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu cep</a>
                                    </div>
                                </div>

                                <div class="form-row">

                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="text" class="form-control" id="endereco" name="endereco" placeholder=" " value="<?= (isset($endereco[0])) ? $endereco[0]->endereco : '' ?>">
                                            <label for="endereco">Endereço</label>
                                        </div>
                                    </div>

                                    <div class="col-md-3 mx-auto">
                                        <div class="md-form mt-2">
                                            <input type="number" name="numero" id="numero" min="1" max="100000" maxlength="6" class="form-control" value="<?= (isset($endereco[0])) ? $endereco[0]->numero : '' ?>">
                                            <label for="numero">Número</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3 mx-auto mb-4">
                                        <div class="md-form mt-2 mb-0">
                                            <input type="text" class="form-control" size="15" maxlength="9" id="complemento" name="complemento" value="<?= (isset($endereco[0])) ? $endereco[0]->complemento : ''  ?>">
                                            <label for="complemento">Complemento</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <div class="md-form mt-2">
                                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder=" " value="<?= (isset($endereco[0])) ? $endereco[0]->bairro : '' ?>">
                                            <label for="bairro">Bairro</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="md-form mt-2">
                                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder=" " value="<?= (isset($endereco[0])) ? $endereco[0]->cidade : '' ?>">
                                            <label for="cidade">Cidade</label>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="md-form mt-2">
                                            <input type="text" class="form-control" id="uf" name="uf" placeholder=" " value="<?= (isset($endereco[0])) ? $endereco[0]->uf : '' ?>"donly>
                                            <label for="uf">UF</label>
                                        </div>
                                    </div>
                                </div>

                                <?php if($this->session->userdata('userlogado')->tipo >= 2) { ?>
                                    <input type="hidden" name="id" id="id3" value="<?= $this->session->userdata('userlogado')->id ?>">
                                <?php } ?>

                                <button class="btn btn-primary" type="submit">Salvar</button>

                            </form>
                        </div>
                    </div>
                    
                </div>

            </div>

        </div>
        <script>
            $(document).ready(function () {
                
                $('#infouser').submit(e => {
                    e.preventDefault()

                    let dados = $('#infouser').serialize();
                    
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('perfil/edit/infouser') ?>",
                        data: dados,
                        dataType: "json",
                        success: data => {
                            console.log(data);
                            $.each(data, (index, value) => {
                                if(!value){
                                    $(`#${index}`).addClass('is-valid')
                                    $(`#${index}`).removeClass(`#${index} is-invalid`)
                                    $(`#erro_${index}`).remove()
                                } else {
                                    $(`#erro_${index}`).remove()
                                    $(`#${index}`).addClass('is-invalid')
                                    $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                }
                            })
                            if(data.edit == 1){
                                $('h1').html(`Olá, ${data["dados"].nome} ${data["dados"].sobrenome}`)
                            }
                        },
                        error: erro => {
                            console.log('Erros foram encontrados: \n')
                            console.log(erro)
                        }
                    });

                })

                $('#altsenha').submit(e => {
                    e.preventDefault()

                    let dados = $('#altsenha').serialize();

                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('perfil/edit/altsenha') ?>",
                        data: dados,
                        dataType: "json",
                        success: data => {
                            console.log(data);
                            $.each(data, (index, value) => {
                                if(!value){
                                    $(`#${index}`).addClass('is-valid')
                                    $(`#${index}`).removeClass(`#${index} is-invalid`)
                                    $(`#erro_${index}`).remove()
                                } else {
                                    $(`#erro_${index}`).remove()
                                    $(`#${index}`).addClass('is-invalid')
                                    $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                }
                            })
                            if(data.edit){
                                $('#senha, #senhanova, #confirmarsenha').val(null);
                                $('#senha').parent().before('<div class="alert card alert-white alert-dismissible fade show" role="alert">Senha alterada com sucesso!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
                                )
                            }
                        },
                        error: erro => {
                            console.log('Erros foram encontrados: \n')
                            console.log(erro)
                        }
                    });
                    
                })
                
                $('select#tipo').change(e => {
                    $.ajax({
                        type: "post",
                        url: "<?= base_url('perfil/get_endereco') ?>",
                        data: '<?php if($this->session->userdata('userlogado')->tipo >= 2) { echo 'id='.$this->session->userdata('userlogado')->id; } else { echo null; }?>',
                        dataType: "json",
                        success: data => {
                            console.log(data)
                            
                            let tipo = $('select#tipo').val()
                            if(data[tipo-1] != null) {
                                $('#cep').val(data[tipo-1].cep)
                                $('#endereco').val(data[tipo-1].endereco)
                                $('#numero').val(data[tipo-1].numero)
                                $('#complemento').val(data[tipo-1].complemento)
                                $('#bairro').val(data[tipo-1].bairro)
                                $('#cidade').val(data[tipo-1].cidade)
                                $('#uf').val(data[tipo-1].uf)
                            } else {
                                $('#cep').val('')
                                $('#endereco').val('')
                                $('#numero').val('')
                                $('#complemento').val('')
                                $('#bairro').val('')
                                $('#cidade').val('')
                                $('#uf').val('')
                            }            
                            if($('#cep').val().length == 8) {
                                let cep = $('#cep').val();
                                var resultado = cep.replace(/(\d{5})(\d{1})/, "$1-$2");
                                $('#cep').val(resultado);
                            }
                        },
                        error: erro => {
                            console.log('Erros foram encontrados: \n')
                            console.log(erro)
                        }
                    })
                    
                })

                $('#infoentrega').submit(e => {
                    e.preventDefault()

                    let dados = $('#infoentrega').serialize();
                    
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('perfil/edit/infoentrega') ?>",
                        data: dados,
                        dataType: "json",
                        success: data => {
                            console.log(data);
                            $.each(data, (index, value) => {
                                if(!value){
                                    $(`#${index}`).addClass('is-valid')
                                    $(`#${index}`).removeClass(`#${index} is-invalid`)
                                    $(`#erro_${index}`).remove()
                                } else {
                                    $(`#erro_${index}`).remove()
                                    $(`#${index}`).addClass('is-invalid')
                                    $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                }
                            }) 
                        },
                        error: erro => {
                            console.log('Erros foram encontrados: \n')
                            console.log(erro)
                        }
                    })
                })
            })
        </script>