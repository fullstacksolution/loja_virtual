<div class="container">
    <div class="row mt-5">
        <div class="col-lg-8 col-md-12 mx-auto">
            <div class="card">
                <div class="card-body text-center px-lg-5 px-md-5 px-3">

                    <div class="py-1 text-left">
                        <a class="text-ipci-primary h6 " href="<?= base_url('loja') ?>" title="Voltar à loja">Voltar à loja</a>
                    </div>

                    <!-- Form -->
                    <form id="cadastro">

                        <div class="row">
                            <div class="col-8 mx-auto">
                                <a class="p-4" title="Página principal" href="<?= base_url('loja') ?>" >
                                    <h2 class="text-hide my-auto">
                                        IPCI
                                        <img class="img-fluid img-hover" src="<?= base_url('assets/img/logotipo_ipci.png')?>" alt="IPCI">
                                    </h2>
                                </a>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <h3>Cadastro</h3>
                            </div>
                        </div>

                        <div class="row my-3">

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                            <div class="col-sm-4 col-md-4 text-center">
                            <span class="text-muted">Informações do Usuário</span>
                            </div>

                            <div class="col-sm-4 col-md-4">
                            <hr>
                            </div>

                        </div>
                        <!-- Nome -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="nome" name="nome" maxlength="40" class="form-control">
                                    <label for="nome">Nome / Razão Social</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="sobrenome" name="sobrenome" maxlength="40" class="form-control">
                                    <label for="sobrenome">Sobrenome</label>
                                </div>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="email" id="email" name="email" maxlength="40" class="form-control">
                                    <label for="email">E-mail</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="email" id="confirmaremail" name="confirmaremail" maxlength="40" class="form-control">
                                    <label for="confirmaremail">Confirmar e-mail</label>
                                </div>
                            </div>
                        </div>

                        <!-- CPF/Tel/Cel -->
                        <div class="form-row">
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="cpf" name="cpf" maxlength="14" class="form-control">
                                    <label for="cpf">CPF / CNPJ</label>
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="telefone" name="telefone" maxlength="15" class="form-control">
                                    <label for="telefone">Telefone</label>
                                </div>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" id="celular" name="celular" maxlength="15" class="form-control">
                                    <label for="celular">Celular</label>
                                </div>
                            </div>
                        </div>

                        <!-- Senha/Confirmação -->
                        <div class="form-row">
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="password" id="senha" maxlength="30" name="senha" class="form-control">
                                    <label for="senha">Senha</label>
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="password" id="confirmarsenha" maxlength="30" name="confirmarsenha" class="form-control">
                                    <label for="confirmarsenha">Confirmar senha</label>
                                </div>
                            </div>
                        </div>

                        <div class="row my-3">

                            <div class="col-sm-4 col-md-4">
                                <hr>
                            </div>

                            <div class="col-sm-4 col-md-4 text-center">
                                <span class="text-muted">Informações de Entrega <span class="font-small">(Opcional)</span></span>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <hr>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mx-auto mb-4">
                                <div class="md-form mt-2 mb-0">
                                    <input type="text" class="form-control" size="15" maxlength="9" id="cep" name="cep" >
                                    <label for="cep">CEP</label>
                                </div>
                                <a class="text-ipci-primary font-small" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei meu cep</a>
                            </div>
                        </div>

                        <div class="form-row">

                            <div class="col-md-6 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="endereco" name="endereco" maxlength="40" placeholder=" " readonly>
                                    <label for="endereco">Endereço</label>
                                </div>
                            </div>

                            <div class="col-md-3 mx-auto">
                                <div class="md-form mt-2">
                                    <input type="number" name="numero" id="numero" min="1" max="100000" maxlength="6" class="form-control">
                                    <label for="numero">Número</label>
                                </div>
                            </div>
                            
                            <div class="col-md-3 mx-auto mb-4">
                                <div class="md-form mt-2 mb-0">
                                    <input type="text" class="form-control" size="15" maxlength="15" id="complemento" name="complemento" >
                                    <label for="complemento">Complemento</label>
                                </div>
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="bairro" name="bairro" maxlength="40"  placeholder=" " readonly>
                                    <label for="bairro">Bairro</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="cidade" name="cidade" maxlength="40"  placeholder=" " readonly>
                                    <label for="cidade">Cidade</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="md-form mt-2">
                                    <input type="text" class="form-control" id="uf" name="uf" placeholder=" " readonly>
                                    <label for="uf">UF</label>
                                </div>
                            </div>
                        </div>

                        <!-- Políticas e Termos -->
                        <div class="form-row">
                            <div class="col-md-8 col-6 mx-auto">
                                <div class="form-group custom-control custom-checkbox mt-2">
                                    <input type="checkbox" class="custom-control-input" id="termos" name="termos">
                                    <label class="custom-control-label text-muted" for="termos">Li e concordo com as <a href="<?=base_url('termos')?>" class="text-ipci-primary" title="Políticas e Termos de Uso" target="_blank">Políticas e com os Termos de uso</a> da IPCI.</label>
                                </div>
                            </div>
                        </div>

                        <!-- Enviar -->
                        <button class="btn btn-primary btn-rounded btn-block my-4 waves-effect" type="submit">Cadastrar</button>

                        <!-- Cadastrar -->
                        <p>Já tem uma conta?<br>
                            <a class="text-ipci-primary" href="<?= base_url('sign/in')?>">Fazer login</a>
                        </p>

                    </form>
                    <!-- Form -->

                </div>

            </div>
            
            <div class="text-right align-self-center">
                <ul class="list-inline p-2">
                    <li class="list-inline-item">
                        <a class="text-muted" href="<?= base_url('contato')?>">Contato</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="<?= base_url('termos')?>">Políticas e Termos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {

    $('#nome').focus()

    $('#cadastro').submit(e => {
        e.preventDefault()

        let dados = $('#cadastro').serialize();

        $.ajax({
            type: "POST",
            url: "<?= base_url('sign/cadastro')?>",
            data: dados,
            dataType: "json",
            success: data => {
                $.each(data, (index, value) => {
                    if(!value){
                        $(`#${index}`).addClass('is-valid')
                        $(`#${index}`).removeClass(`#${index} is-invalid`)
                        $(`#erro_${index}`).remove()
                    } else {
                        $(`#erro_${index}`).remove()
                        $(`#${index}`).addClass('is-invalid')
                        $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                    }
                })
                try{
                    $('#cadastro').find('.is-invalid').first().focus()
                } catch (e){}
                if(data.redirect){
                    $(location).attr('href', data.redirect)
                }
            },
            error: erro => {
                console.log('Erros foram encontrados: \n')
                console.log(erro)
            }
        })
    })
})
</script>