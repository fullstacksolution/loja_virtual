<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152853736-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-152853736-1');
        </script>
        
        <!-- Descrições -->
        <meta name="DC.title" content="IPCI | Automação Industrial e Eletrônica">
        <meta name="description" content="A IPCI é uma empresa focada em desenvolvimento de projetos em Automação Industrial e Eletrônica.">
        <meta name="robots" content="">
        <meta name="keywords" content="IPCI, Eletrônica, Automação Industrial, Projetos de automação industrial, loja de eletrônica, informática">

        <meta property="og:description" content="A IPCI é uma empresa focada em desenvolvimento de projetos em Automação Industrial e Eletrônica." />
        <meta property="og:image" content="<?= base_url('assets/img/ipci.jpg') ?>">

        <!-- Location -->
        <meta name="geo.region" content="BR-SP" />
        <meta name="geo.placename" content="Guarulhos" />
        <meta name="geo.position" content="-23.435305;-46.520593" />
        <meta name="ICBM" content="-23.435305, -46.520593" />

        <link rel="shortcut icon" href="<?= base_url('assets/img/ipci.ico') ?>" type="image/x-icon">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="<?= base_url('assets/css/mdb.css') ?>" rel="stylesheet">
        <!-- Selectize -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous">
        <!-- Estilo customizado -->
        <link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        
        <!-- ReCAPTCHA v3 -->
        <?= isset($recaptcha) ? $recaptcha : '' ?>
        <title><?= isset($titulo) ? $titulo : 'IPCI' ?></title>
    </head>
    <body>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0"></script>