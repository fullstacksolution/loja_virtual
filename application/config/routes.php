<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Manutencao';
$route['404_override'] = 'Errors/error404';
$route['404'] = 'Errors/error404';
$route['translate_uri_dashes'] = FALSE;

//  Rotas de login, logout, autenticação e cadastro
$route['sign/in'] = 'auth/in';
$route['sign/out'] = 'auth/out';
$route['sign/up'] = 'auth/up';
$route['sign/auth'] = 'auth/auth';
$route['sign/cadastro'] = 'auth/cadastro';

// Rotas do perfil cadastral dos usuários
    // Rotas para usuários comuns
    $route['perfil/edit/infouser'] = 'perfil/infouser';
    $route['perfil/edit/altsenha'] = 'perfil/altsenha';
    $route['perfil/edit/infoentrega'] = 'perfil/infoentrega';
    // Rotas para administradores
    $route['perfil/edit/altemail'] = 'admin/usuario/altemail';
    $route['perfil/edit/altcpf'] = 'admin/usuario/altcpf';
    $route['perfil/deletar'] = 'admin/usuario/deletar';

// Rotas da loja
$route['categoria'] = 'categoria/listar';
$route['categoria/(:any)'] = 'categoria/listar/$1';
$route['categoria/(:any)/(:any)'] = 'categoria/listar/$1/$2';
$route['categoria/(:any)/(:any)/(:any)'] = 'categoria/listar/$1/$2/$3';
$route['categoria/(:any)/(:any)/(:any)/(:num)'] = 'categoria/listar/$1/$2/$3/$4';

$route['mapa'] = 'categoria/mapa';
$route['busca'] = 'loja/busca';
$route['busca/(:num)'] = 'loja/busca/$1';

$route['produto/(:num)'] = 'produto/index/$1';
$route['produto/(:num)/(:any)'] = 'produto/index/$1/$2';
$route['produto/avaliacao/get'] = 'produto/get_avaliacoes_by_produto';

// Rotas do painel administrativo
$route['admin'] = 'admin/home/index';
$route['admin/usuario/ativos'] = 'admin/usuario/get_usuarios_ativos';
$route['admin/usuario/inativos'] = 'admin/usuario/get_usuarios_inativos';

$route['admin/banner'] = 'admin/banner/index';
$route['admin/banner/editar/(:num)'] = 'admin/banner/editar/$1';
$route['admin/banner/deletar/(:num)'] = 'admin/banner/deletar/$1';

$route['admin/cliente'] = 'admin/usuario/listar_clientes';
$route['admin/cliente/editar/(:num)'] = 'admin/usuario/editar_usuario/$1';
$route['admin/cliente/deletar/(:num)'] = 'admin/usuario/deletar_usuario/$1';
$route['admin/cliente/cadastrar'] = 'admin/usuario/cadastrar_usuario';

$route['admin/administrador'] = 'admin/usuario/listar_administradores';
$route['admin/administrador/editar/(:num)'] = 'admin/usuario/editar_usuario/$1';
$route['admin/administrador/deletar/(:num)'] = 'admin/usuario/deletar_usuario/$1';
$route['admin/administrador/cadastrar'] = 'admin/usuario/cadastrar_usuario';

$route['admin/departamento'] = 'admin/departamento/listar_departamentos';
$route['admin/departamento/editar/(:num)'] = 'admin/departamento/editar_departamento/$1';
$route['admin/departamento/deletar/(:num)'] = 'admin/departamento/deletar_departamento/$1';
$route['admin/departamento/criar'] = 'admin/departamento/criar_departamento';

$route['admin/categoria'] = 'admin/categoria/listar_categorias';
$route['admin/categoria/editar/(:num)'] = 'admin/categoria/editar_categoria/$1';
$route['admin/categoria/deletar/(:num)'] = 'admin/categoria/deletar_categoria/$1';
$route['admin/categoria/criar'] = 'admin/categoria/criar_categoria';

$route['admin/subcategoria'] = 'admin/subcategoria/listar_subcategorias';
$route['admin/subcategoria/editar/(:num)'] = 'admin/subcategoria/editar_subcategoria/$1';
$route['admin/subcategoria/deletar/(:num)'] = 'admin/subcategoria/deletar_subcategoria/$1';
$route['admin/subcategoria/criar'] = 'admin/subcategoria/criar_subcategoria';

$route['admin/produto'] = 'admin/produto/listar_produtos';
$route['admin/produto/editar/(:num)'] = 'admin/produto/editar_produto/$1';
$route['admin/produto/deletar/(:num)'] = 'admin/produto/deletar_produto/$1';
$route['admin/produto/criar'] = 'admin/produto/criar_produto';
$route['admin/produto/avaliacao/listar'] = 'admin/produto/listar_avaliacoes';
$route['admin/produto/avaliacao/listar_pendentes'] = 'admin/produto/listar_avaliacoes_pendentes';
$route['admin/produto/avaliacao/deletar/(:num)'] = 'admin/produto/deletar_avaliacao/$1';
$route['admin/produto/avaliacao/publicar/(:num)'] = 'admin/produto/publicar_avaliacao/$1';

$route['admin/produto/imagem/adicionar'] = 'admin/produto/adicionar_imagem';
$route['admin/produto/imagem/deletar'] = 'admin/produto/deletar_imagem';
$route['admin/produto/imagem/listar'] = 'admin/produto/listar_imagens';