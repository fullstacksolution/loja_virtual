<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoLib extends CI_Object {

    public function criar($nome, $id_subcategoria, $descricao, $fabricante, $marca, $modelo, $cor, $preco_custo, $preco_venda, $preco_promocional, $garantia, $ipi, $sku, $ean_gtin, $mpn_partnumber, $ncm, $serial, $caixa, $peso_kg, $altura_cm, $largura_cm, $profundidade_cm, $endereco_estoque, $quantidade, $unidade, $disponibilidade, $condicao, $codigo, $status, $promocao){
        $dados['nome'] = $nome;
        $dados['id_subcategoria'] = $id_subcategoria;
        $dados['descricao'] = $descricao;
        $dados['fabricante'] = $fabricante;
        $dados['marca'] = $marca;
        $dados['modelo'] = $modelo;
        $dados['cor'] = $cor;
        $dados['preco_custo'] = $preco_custo;
        $dados['preco_venda'] = $preco_venda;
        $dados['preco_promocional'] = $preco_promocional;
        $dados['garantia'] = $garantia;
        $dados['ipi'] = $ipi;
        $dados['sku'] = $sku;
        $dados['ean_gtin'] = $ean_gtin;
        $dados['mpn_partnumber'] = $mpn_partnumber;
        $dados['ncm'] = $ncm;
        $dados['serial'] = $serial;
        $dados['caixa'] = $caixa;
        $dados['peso_kg'] = $peso_kg;
        $dados['altura_cm'] = $altura_cm;
        $dados['largura_cm'] = $largura_cm;
        $dados['profundidade_cm'] = $profundidade_cm;
        $dados['endereco_estoque'] = $endereco_estoque;
        $dados['quantidade'] = $quantidade;
        $dados['unidade'] = $unidade;
        $dados['disponibilidade'] = $disponibilidade;
        $dados['condicao'] = $condicao;
        $dados['codigo'] = $codigo;
        $dados['status'] = $status;
        $dados['promocao'] = $promocao;
        return $this->db->insert('tb_produto', $dados);
    }

    public function editar($nome, $id, $id_subcategoria, $descricao, $fabricante, $marca, $modelo, $cor, $preco_custo, $preco_venda, $preco_promocional = null, $garantia, $ipi, $sku, $ean_gtin, $mpn_partnumber, $ncm, $serial, $caixa, $peso_kg, $altura_cm, $largura_cm, $profundidade_cm, $endereco_estoque, $quantidade, $unidade, $disponibilidade, $condicao, $codigo, $status, $promocao, $destaque){
        $dados['nome'] = $nome;
        $dados['id_subcategoria'] = $id_subcategoria;
        $dados['descricao'] = $descricao;
        $dados['fabricante'] = $fabricante;
        $dados['marca'] = $marca;
        $dados['modelo'] = $modelo;
        $dados['cor'] = $cor;
        $dados['preco_custo'] = $preco_custo;
        $dados['preco_venda'] = $preco_venda;
        $dados['preco_promocional'] = $preco_promocional;
        $dados['garantia'] = $garantia;
        $dados['ipi'] = $ipi;
        $dados['sku'] = $sku;
        $dados['ean_gtin'] = $ean_gtin;
        $dados['mpn_partnumber'] = $mpn_partnumber;
        $dados['ncm'] = $ncm;
        $dados['serial'] = $serial;
        $dados['caixa'] = $caixa;
        $dados['peso_kg'] = $peso_kg;
        $dados['altura_cm'] = $altura_cm;
        $dados['largura_cm'] = $largura_cm;
        $dados['profundidade_cm'] = $profundidade_cm;
        $dados['endereco_estoque'] = $endereco_estoque;
        $dados['quantidade'] = $quantidade;
        $dados['unidade'] = $unidade;
        $dados['disponibilidade'] = $disponibilidade;
        $dados['condicao'] = $condicao;
        $dados['codigo'] = $codigo;
        $dados['status'] = $status;
        if(isset($promocao)) $dados['promocao'] = $promocao;
        if(isset($destaque)) $dados['destaque'] = $destaque;
        $this->db->where('id', $id);
        return $this->db->update('tb_produto', $dados);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('tb_produto');
    }

    public function adicionar_imagem($id_produto, $img){
        $dados['id_produto'] = $id_produto;
        $dados['img'] = $img;
        return $this->db->insert('tb_produto_img', $dados);
    }

    public function deletar_imagem($id_imagem, $id_produto){
        $this->db->where('id', $id_imagem);
        $this->db->where('id_produto', $id_produto);
        return $this->db->delete('tb_produto_img');
    }

    public function deletar_imagens($id_produto){
        $this->db->where('id_produto', $id_produto);
        return $this->db->delete('tb_produto_img');
    }

    public function adicionar_avaliacao($id_produto, $id_usuario, $titulo, $comentario, $avaliacao){
        $dados['id_produto'] = $id_produto;
        $dados['id_usuario'] = $id_usuario;
        $dados['titulo'] = $titulo;
        $dados['comentario'] = $comentario;
        $dados['avaliacao'] = $avaliacao;
        return $this->db->insert('tb_produto_avaliacao', $dados);
    }

    public function deletar_avaliacao($id_avaliacao){
        $this->db->where('id', $id_avaliacao);
        return $this->db->delete('tb_produto_avaliacao');
    }

    public function deletar_avaliacoes($id_produto){
        $this->db->where('id_produto', $id_produto);
        return $this->db->delete('tb_produto_avaliacao');
    }

    public function publicar_avaliacao($id_avaliacao){
        $dados['status'] = 1;
        $this->db->where('id', $id_avaliacao);
        return $this->db->update('tb_produto_avaliacao', $dados);
    }
}
