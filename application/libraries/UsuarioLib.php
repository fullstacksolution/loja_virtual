<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioLib extends CI_Object {

    // Set

    public function criar($nome, $sobrenome = null,$email, $telefone = null, $celular = null, $senha, $cpf, $tipo = 1){
        $dados['nome'] = $nome;
        $dados['sobrenome'] = $sobrenome;
        $dados['email'] = $email;
        $dados['telefone'] = $telefone;
        $dados['celular'] = $celular;
        $dados['senha'] = sha1($senha);
        $dados['cpf'] = $cpf;
        $dados['tipo'] = $tipo;
        return $this->db->insert('tb_usuario', $dados);
    }

    public function editar($nome, $sobrenome, $telefone, $celular, $id){
        $dados['nome'] = $nome;
        $dados['sobrenome'] = $sobrenome;
        $dados['telefone'] = $telefone;
        $dados['celular'] = $celular;
        $this->db->where('id', $id);
        return $this->db->update('tb_usuario', $dados);
    }

    public function editar_senha($senhanova, $id){
        $dados['senha'] = sha1($senhanova);
        $this->db->where('id', $id);
        return $this->db->update('tb_usuario', $dados);
    }

    public function editar_email($email, $id){
        $dados['email'] = $email;
        $this->db->where('id', $id);
        return $this->db->update('tb_usuario', $dados);
    }

    public function editar_cpf($cpf, $id){
        $dados['cpf'] = $cpf;
        $this->db->where('id', $id);
        return $this->db->update('tb_usuario', $dados);
    }

    public function deletar($id) {
        $this->db->where('tipo <=', 2);
        $this->db->where('id', $id);
        return $this->db->delete('tb_usuario');
    }

    public function autenticar($id, $status = 0){
        $dados['status'] = $status;
        $this->db->where('id', $id);
        return $this->db->update('tb_usuario', $dados);
    }

    // Get

    public function get_usuarios_by_status($status){
        $this->db->select('count(usuario.id) as usuarios_'.($status ? 'ativos' : 'inativos'));
        $this->db->from('tb_usuario usuario');
        $this->db->where('usuario.status', $status);
        return $this->db->get()->row_object();
    }

    public function get_usuario_id($email){
        $this->db->select('id');
        $this->db->where('email', $email);
        return $this->db->get('tb_usuario')->row_object();
    }

    public function get_usuario_senha($id){
        $this->db->select('senha');
        $this->db->where('id', $id);
        return $this->db->get('tb_usuario')->row_object();
    }

    public function get_usuario_email($id){
        $this->db->select('email');
        $this->db->where('id', $id);
        return $this->db->get('tb_usuario')->row_object();
    }
    
    public function get_usuario_cpf($id){
        $this->db->select('cpf');
        $this->db->where('id', $id);
        return $this->db->get('tb_usuario')->row_object();
    }    
    public function get_usuario_tipo($id){
        $this->db->select('tipo');
        $this->db->where('id', $id);
        return $this->db->get('tb_usuario')->row_object();
    }    
    
    public function get_usuario_by_email($email){
        $this->db->where('email', $email);
        return $this->db->get('tb_usuario')->row_object();
    }
    
    public function get_usuario_by_id($id){
        $this->db->where('id', $id);
        return $this->db->get('tb_usuario')->row_object();
    }

    public function get_clientes(){
        $this->db->select('id, nome, sobrenome, email, cpf, telefone, celular, data_registro, tipo');
        $this->db->where('tipo', 1);
        return $this->db->get('tb_usuario')->result();
    }

    public function get_administradores(){
        $this->db->select('id, nome, sobrenome, email, cpf, telefone, celular, data_registro, tipo');
        $this->db->where('tipo >=', 2);
        return $this->db->get('tb_usuario')->result();
    }


}
