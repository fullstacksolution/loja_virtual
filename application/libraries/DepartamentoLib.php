<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DepartamentoLib extends CI_Object {

    public function criar($nome){
        $dados['nome'] = $nome;
        return $this->db->insert('tb_departamento', $dados);
    }

    public function editar($nome, $id){
        $dados['nome'] = $nome;
        $this->db->where('id', $id);
        return $this->db->update('tb_departamento', $dados);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('tb_departamento');
    }

}
