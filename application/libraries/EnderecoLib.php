<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EnderecoLib extends CI_Object {

    public function criar($cep, $endereco, $numero, $complemento = null, $bairro, $cidade, $uf, $id_usuario, $tipo){
        $dados['cep'] = $cep;
        $dados['endereco'] = $endereco;
        $dados['numero'] = $numero;
        $dados['complemento'] = $complemento;
        $dados['bairro'] = $bairro;
        $dados['cidade'] = $cidade;
        $dados['uf'] = $uf;
        $dados['tipo'] = $tipo;
        $dados['id_usuario'] = $id_usuario;
        return $this->db->insert('tb_endereco', $dados);
    }

    public function editar($cep, $endereco, $numero, $complemento = null, $bairro, $cidade, $uf, $id_usuario, $tipo){
        $dados['cep'] = $cep;
        $dados['endereco'] = $endereco;
        $dados['numero'] = $numero;
        $dados['complemento'] = $complemento;
        $dados['bairro'] = $bairro;
        $dados['cidade'] = $cidade;
        $dados['uf'] = $uf;
        $dados['tipo'] = $tipo;
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('tipo', $tipo);
        return $this->db->update('tb_endereco', $dados);
        
    }

    public function deletar($id_usuario){
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->delete('tb_endereco');
    }

    public function verifica($id, $tipo){
        $this->db->select('id');
        $this->db->where('id_usuario', $id);
        $this->db->where('tipo', $tipo);
        return $this->db->get('tb_endereco')->result();
    }

}
