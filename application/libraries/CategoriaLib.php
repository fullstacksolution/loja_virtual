<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CategoriaLib extends CI_Object {

    public function criar($nome, $id_departamento){
        $dados['nome'] = $nome;
        $dados['id_departamento'] = $id_departamento;
        return $this->db->insert('tb_categoria', $dados);
    }

    public function editar($nome, $id_departamento, $id){
        $dados['nome'] = $nome;
        $dados['id_departamento'] = $id_departamento;
        $this->db->where('id', $id);
        return $this->db->update('tb_categoria', $dados);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('tb_categoria');
    }

}
