<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubcategoriaLib extends CI_Object {

    public function criar($nome, $id_categoria){
        $dados['nome'] = $nome;
        $dados['id_categoria'] = $id_categoria;
        return $this->db->insert('tb_subcategoria', $dados);
    }

    public function editar($nome, $id_categoria, $id){
        $dados['nome'] = $nome;
        $dados['id_categoria'] = $id_categoria;
        $this->db->where('id', $id);
        return $this->db->update('tb_subcategoria', $dados);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('tb_subcategoria');
    }

}
