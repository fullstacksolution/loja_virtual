<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BannerLib extends CI_Object {
    
    public function adicionar($nome, $link, $status, $img){
        $dados['nome'] = $nome;
        $dados['link'] = $link;
        $dados['status'] = $status;
        $dados['img'] = $img;
        return $this->db->insert('tb_banner', $dados);
    }

    public function editar($id, $nome, $link, $status){
        $dados['nome'] = $nome;
        $dados['link'] = $link;
        $dados['status'] = $status;
        $this->db->where('id', $id);
        return $this->db->update('tb_banner', $dados);
    }
    
    public function deletar($id){
        return $this->db->delete('tb_banner', "id = $id");
    }

    public function get_lista_banners(){
        return $this->db->get('tb_banner')->result();
    }

    public function get_banner_by_id($id){
        $this->db->where('id', $id);
        return $this->db->get('tb_banner')->row_object();
    }

    public function get_banners(){
        $this->db->where('status', 1);
        return $this->db->get('tb_banner')->result();
    }

}
