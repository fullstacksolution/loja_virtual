<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CarrinhoLib extends CI_Object {

    public function adicionar($id_produto, $id_usuario, $id_session, $quantidade){
        $dados['id_produto'] = $id_produto;
        $dados['id_usuario'] = $id_usuario;
        $dados['id_session'] = $id_session;
        $dados['quantidade'] = $quantidade;
        return $this->db->insert('tb_carrinho', $dados);
    }

    public function deletar($id_produto, $id_usuario){
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_produto', $id_produto);
        return $this->db->delete('tb_carrinho');
    }

    public function deletar_produtos_carrinho($id_produto){
        $this->db->where('id_produto', $id_produto);
        return $this->db->delete('tb_carrinho');
    }

    public function atualizar_quantidade($id_produto, $id_usuario, $qtd_atualizada){
        $dados['quantidade'] = $qtd_atualizada;
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_produto', $id_produto);
        return $this->db->update('tb_carrinho', $dados);
    }

    public function verificar_adicionado($id_produto, $id_usuario){
        $this->db->select('count(cart.id) as existe, cart.quantidade');
        $this->db->from('tb_carrinho cart');
        $this->db->where('cart.id_produto', $id_produto);
        $this->db->where('cart.id_usuario', $id_usuario);
        return $this->db->get()->row_object();
    }

    public function get_carrinho($id_usuario){
        $this->db->select('prod.id as id_produto, md5(prod.id) as id_produto_md5, prod.nome, prod.quantidade as qtd_produtos, prod.preco_promocional, prod.preco_venda, prod.promocao, cart.quantidade, img.img');
        $this->db->from('tb_carrinho cart');
        $this->db->join('tb_produto prod', 'prod.id = cart.id_produto', 'left');
        $this->db->join('tb_produto_img img', 'img.id_produto = cart.id_produto', 'left');
        $this->db->join('tb_usuario usuario', 'usuario.id = cart.id_usuario', 'left');
        $this->db->where('usuario.id', $id_usuario);
        $this->db->group_by('cart.id');
        return $this->db->get()->result();
    }
    
    public function get_qtd_produtos($id_usuario){
        $this->db->select('count(id) as qtd_produtos');
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->get('tb_carrinho')->row_object();
    }

}
