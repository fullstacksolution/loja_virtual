$(document).ready(function () {

    let id_produto = $(location).attr('pathname').split("/").splice(2, 1).join("/");    

    $('button#btn_avaliacao').click(function (e) { 
        e.preventDefault();
        $.ajax({
            type: "post",
            url: base_url+"/produto/adicionar_avaliacao",
            data: "request=avaliacao&id_produto="+id_produto,
            dataType: "json",
            success: dados => {
                if(dados.sucesso){
                    $('#row_form_avaliacao').html('')
                    $('#row_avaliacao').after(dados.html)
                    $('button#cancelar_avaliacao').click(function (e){
                        e.preventDefault()
                        $('#row_form_avaliacao').html('')
                    })

                    // Efeito de alteração das estrelas ao modificar o range
                    $('#avaliacao').change(function (e){
                        let range = parseInt($(this).val())
                        switch (range) {
                            case 0: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-ipci-primary')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').addClass('text-muted')                                      
                                break; 
                            case 1: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-ipci-primary')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-muted')
                                $('#star_1').addClass('text-ipci-primary')
                                $('#star_2, #star_3, #star_4, #star_5').addClass('text-muted')
                                break; 
                            case 2: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-ipci-primary')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-muted')
                                $('#star_1, #star_2').addClass('text-ipci-primary')
                                $('#star_3, #star_4, #star_5').addClass('text-muted')
                                break; 
                            case 3: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-ipci-primary')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-muted')
                                $('#star_1, #star_2, #star_3').addClass('text-ipci-primary')
                                $('#star_4, #star_5').addClass('text-muted')
                                break; 
                            case 4: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-ipci-primary')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-muted')
                                $('#star_1, #star_2, #star_3, #star_4').addClass('text-ipci-primary')
                                $('#star_5').addClass('text-muted')
                                break; 
                            case 5: 
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').removeClass('text-muted')
                                $('#star_1, #star_2, #star_3, #star_4, #star_5').addClass('text-ipci-primary')
                                break;
                        }
                    })

                    $('form#add_avaliacao').submit((e)=>{
                        e.preventDefault()
                        let dados_avaliacao = $('form#add_avaliacao').serialize()
                        $.ajax({
                            type: "post",
                            url: base_url+"/produto/adicionar_avaliacao",
                            data: dados_avaliacao,
                            dataType: "json",
                            success: dados2 => {
                                console.log(dados2)
                                if(dados2.erro){
                                    $.each(dados2, (index, value) => {
                                        if(!value){
                                            $(`#${index}`).addClass('is-valid')
                                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                                            $(`#erro_${index}`).remove()
                                        } else {
                                            $(`#erro_${index}`).remove()
                                            $(`#${index}`).addClass('is-invalid')
                                            $(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                                        }
                                        try {
                                            $('form#add_avaliacao').find('.is-invalid').first().focus()
                                        } catch (e) {}
                                    }) 
                                }
                                $('#row_form_avaliacao').html(dados2.html)
                            },
                            error: erro => {
                                $(location).attr('href', dados.redirect)                                       
                            }
                        })
                    })

                } else {
                    $(location).attr('href', dados.redirect)
                }
                
            },
            error: erro => {
                console.log('Erros:')                            
                console.log(erro)                            
            }
        });
    });

    $('button#carregar_avaliacoes').click(function(e){
        e.preventDefault()

        $.ajax({
            type: "post",
            url: base_url+"/produto/avaliacao/get",
            data: 'request=getaval&id_produto='+id_produto,
            dataType: "json",
            success: dados3 => {
                
                $('#lista_avaliacoes').html('')
                jQuery.fn.reverse = function() {
                    return this.pushStack(this.get().reverse(), arguments);
                }; 
                $(dados3).reverse().each( function (index, value) {
                    
                    $('#lista_avaliacoes').prepend('<div class="row" id="'+index+'">')
                    $('#lista_avaliacoes div.row#'+index).prepend('<div class="col-md-8">')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-8').prepend('<p class="mb-2 text-muted">'+value.data+'</p>')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-8').append('<h5 class="font-weight-bold">'+value.titulo+'</h5>')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-8').append('<p>'+value.comentario+'</p>')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-8').append('<blockquote class="blockquote-footer">'+value.nome_usuario+' '+value.sobrenome_usuario+'</blockquote>')
                    $('#lista_avaliacoes div.row#'+index).append('</div>')
                    $('#lista_avaliacoes div.row#'+index).append('<div class="col-md-4">')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-4').prepend('<div class="row">')
                    $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row').prepend('<div class="col text-center">')

                    if(value.avaliacao == 0) { 

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')

                    } else if(value.avaliacao < 2) {

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').append('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')

                    } else if(value.avaliacao < 3) {

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').append('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')

                    } else if(value.avaliacao < 4) {

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').append('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')

                    } else if(value.avaliacao < 5) {

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').append('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star grey-text"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')

                    } else if(value.avaliacao == 5) {

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').append('<ul class="list-group-horizontal mx-0 px-0">')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col ul').prepend('<li class="list-inline-item mb-0"><i class="fas fa-star text-ipci-primary"></i></li>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row div.col').prepend('</ul>')
                    
                    }

                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4 div.row').append('</div>')
                        $('#lista_avaliacoes div.row#'+index+' div.col-md-4').append('</div>')
                        $('#lista_avaliacoes div.row#'+index).append('</div>')
                        $('#lista_avaliacoes div.row#'+index).after('<hr>')
                })
            },
            error: erro => {
                console.log(erro)                                                               
            }
        })

    })


})