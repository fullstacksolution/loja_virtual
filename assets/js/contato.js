$(document).ready(function () {

    $('select').selectize({
        sortField: 'text'
    });

    $('form#contato').submit(e => {
        e.preventDefault()
        let contato = $('form#contato').serialize()
        $('form#contato button:submit').removeClass('btn-primary').addClass('btn-dark').text('Enviando... Aguarde uns instantes.')
        $('form#contato button:submit').attr('disabled', '');
        $.ajax({
            type: "post",
            url: base_url+'/contato/enviar_mensagem',
            data: "request=mail&"+contato,
            dataType: "json",
            success: dados => {
                $('form#contato button:submit').removeClass('btn-dark').addClass('btn-primary').text('Enviar mensagem')
                $('form#contato button:submit').removeAttr('disabled');
                console.log(dados)
                if(dados.erro){
                    if(dados['g-recaptcha']){
                        $('#erro_recaptcha').remove();
                        $('#g-recaptcha').after('<p id="erro_recaptcha" class="text-danger">'+dados['g-recaptcha']+'</p>')
                    } else{
                        $('#erro_recaptcha').remove();
                    }
                    $.each(dados, (index, value) => {
                        if(!value){
                            $(`#${index}`).addClass('is-valid')
                            try{
                                $(`select#${index}, input.horario#${index}`).parent().find('div.selectize-input').addClass('form-control is-valid')                                
                                $(`select#${index}, input.horario#${index}`).parent().find('div.selectize-input').removeClass('is-invalid')
                            } catch(e){}
                            $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                            $(`#erro_${index}`).remove()

                        } else if(value.length > 3) {
                            $(`#erro_${index}`).remove()
                            $(`#${index}`).addClass('is-invalid')
                            $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                            try {
                                $(`select#${index}, input.horario#${index}`).parent().find('div.selectize-input').addClass('form-control is-invalid') 
                                $(`select#${index}, input.horario#${index}`).parent().find('div.selectize-input').removeClass('is-valid')
                            }catch(e){}
                            $(`select#${index}, input.horario#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            $(`div.md-form`).find(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                            
                            try{
                                $('form#contato').find('.is-invalid').first().focus()
                                $('div#alert').remove();
                                $('form#contato button:submit').before($('<div id="alert" class="alert card alert-danger fade show"><strong>Erros foram encontrados.</strong></div>'))
                                setTimeout(()=>{
                                    $("div#alert").alert('close',()=>{
                                        $('div#alert').remove(); 
                                    })
                                },4000)
                            } catch (e){}
                        }
                    })
                }
                if(dados.retorno){
                    $('div.card').before(dados.retorno['aviso'])
                }
            },
            error: erros => {
                console.log(erros)                        
            }
        });

    })

});