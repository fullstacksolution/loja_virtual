$(document).ready(function () {

    
// This will work for dynamically created element
    $('body').on('keyup', '#cep', function(e){
        // Permitir apenas digitação de números
        var valor = $("#cep").val().replace(/[^0-9]+/g,'');

        // Permitir apenas digitação de números
        
        if(typeof($("#endereco")) !== "undefined") {
            var campo_cep = parseInt(valor.length);

            if( campo_cep < 8 && campo_cep > 0) {
                $('#endereco').val('Digite um CEP válido');
                $('#bairro').val('--');
                $('#cidade').val('--');
                $('#uf').val('--');
            } else if( campo_cep == 0) {
                $('#endereco').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#uf').val('');
            } else {

            let url = 'https://viacep.com.br/ws/'+valor+'/json/unicode/';

            let xmlHttp = new XMLHttpRequest();
            xmlHttp.open('GET', url);

            xmlHttp.onreadystatechange = () => {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    let dadosJSONText = xmlHttp.responseText;
                    let dadosJSONObj = JSON.parse(dadosJSONText);

                    if ( dadosJSONObj.ibge == undefined ) {

                        $('#endereco').val('Digite um CEP válido');
                        $('#bairro').val('--');
                        $('#cidade').val('--');
                        $('#uf').val('--');

                    } else {

                    $('#endereco').val(dadosJSONObj.logradouro);
                    $('#bairro').val(dadosJSONObj.bairro);
                    $('#cidade').val(dadosJSONObj.localidade);
                    $('#uf').val(dadosJSONObj.uf);

                    }
                }
            }

            xmlHttp.send()

            }

            // Quando estiver no campo #cep e apertar tab
            $(document).on('keydown', '#cep', function(tab) {
                if(tab.which == 9) {
                    /* tira o foco do #cep */
                    $('#cep').blur();
                    /* previne focar no proximo */
                    event.preventDefault();
                    /* foca no input selecionado */
                    $('#numero').focus();
                } 
            });
            // Quando estiver no campo #complemento e apertar tab
            $(document).on('keydown', '#complemento', function(tab) {
                if(tab.which == 9) {
                    /* tira o foco do ultimo input */
                    $('#complemento').blur();
                    /* previne focar no proximo */
                    event.preventDefault();
                    /* foca no input selecionado */
                    $('#termos').focus();
                } 
            });
        }
        
        // Apenas acrescentar o traço automaticamente
        let cep = $('#cep').val();
        var resultado = cep.replace(/(\d{5})(\d{1})/, "$1-$2");
        $('#cep').val(resultado);
        
    });

    if($('#cep').val()){
        if($('#cep').val().length == 8) {
            let cep = $('#cep').val();
            var resultado = cep.replace(/(\d{5})(\d{1})/, "$1-$2");
            $('#cep').val(resultado);
        }
    }
    
});
