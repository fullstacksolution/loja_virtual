// Transições
$('a#redirect_avaliacao').click(function() {
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top - 75
    }, 1000);
    return false;
});

$(document).ready(function () {

    // Auto-resize do textarea
    jQuery.each(jQuery('textarea[data-autoresize]'), function() {
        var offset = this.offsetHeight - this.clientHeight;
       
        var resizeTextarea = function(el) {
          jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        };
        
        resizeTextarea(this)
        jQuery(this).removeAttr('data-autoresize');
      });
        
    // Adicinar ao carrinho
    $('form#add_carrinho').submit((e)=>{
        e.preventDefault()
        let dados = $('form#add_carrinho').serialize();
        $.ajax({
            type: "post",
            url: base_url+"/carrinho/adicionar",
            data: dados,
            dataType: "json",
            success: dados => {
                if(dados.carrinho){
                    $('#btn_add_carrinho').before('<a href="'+base_url+'/carrinho" class="btn btn-outline-primary font-weight-bold btn-block btn-lg">No carrinho <i class="fas fa-angle-double-right"></i></a>')
                    $('#navbar_qtd_produtos').text(dados.qtd_produtos)
                    $('form#add_carrinho div.row').first().remove()
                    $('#btn_add_carrinho').remove()
                }
                if(dados.erro){
                    if(dados.tipo == 'quantidade'){
                        $('#erro_qtd').remove();
                        $('form#add_carrinho #quantidade').parent().append('<p id="erro_qtd" class="text-danger">'+dados.erro+'</p>')
                    } else{
                        $('#erro_qtd').remove();
                    }
                }
                // Se existir redirect, faça
                if(dados.redirect){
                    $(location).attr('href', dados.redirect);
                }
            },
            error: erros => {
                console.log(erros)                            
            }
        })
    })
    
})
