$(document).ready(function () {
    
    $('body').on('click', '.thumbs img', function(){
        var cover = $('.cover img');
        var thumb = $(this).attr('src');

        if(cover.attr('src') !== thumb){
            cover.fadeTo(350, '0', function(){
                cover.attr('src', thumb);
                cover.fadeTo(350, '1');
            });

            $('.thumbs img').removeClass('active-img');
            $(this).addClass('active-img');

        }
    });
});