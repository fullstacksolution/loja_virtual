$(document).ready(function () {

	// Se existir navbar -> animação de ocultar
	if(document.getElementById("navbar")) {

		var prevScrollpos = window.pageYOffset;
		window.onscroll = function() {
			var currentScrollPos = window.pageYOffset;
			if (prevScrollpos > currentScrollPos) {
				document.getElementById("navbar").style.top = "0";
			} else {
				if($("#nav").is(":visible")){
					
				} else {
					document.getElementById("navbar").style.top = "-125px";
				}
						
			}
			prevScrollpos = currentScrollPos;
		}

		// Funcionalidade do botão de busca
		let btn_busca = $('button#btn_busca');
		let input_busca = btn_busca.parent().parent().parent().find('input#busca')

		function buscar(){
			let busca = ((input_busca.val()).trim()).replace(/[^a-zA-ZÀ-ú0-9 ]/g, '');

			if(!busca){
				input_busca.focus()
				input_busca.val('')
				input_busca.attr('placeholder', 'Busca inválida!')
			} else {
				$('#modal_busca').modal('hide')
				let dados = 'request=busca&busca='+busca
				$(location).attr('href', base_url+'/busca?'+dados)
			}
		}

		btn_busca.click( function (e) {
			e.preventDefault()
			buscar()
		})

		$('input#busca').on('keydown', function(e) {
			if (e.which == 13) {
				e.preventDefault();
				buscar()
			}
		});

		$('nav.navbar input#show_busca, #show_busca_btn, #label_show_busca, #div_show_busca').click(e=>{
			e.preventDefault()
			$(this).blur()
			
			{
				$('#modal_busca').modal('show')
				setTimeout(function(){
					input_busca.focus()
				},500)
			}
		})
	}
	
});
Number.prototype.toFixedNoRounding = function(n) {
    const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + n + "})?", "g")
    const a = this.toString().match(reg)[0];
    const dot = a.indexOf(".");
    if (dot === -1) { // integer, insert decimal dot and pad up zeros
        return a + "." + "0".repeat(n);
    }
    const b = n - (a.length - dot) + 1;
    return b > 0 ? (a + "0".repeat(b)) : a;
 }
 const base_url = window.location.origin;
 