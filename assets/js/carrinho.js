
        $(document).ready(function () {
            function formataPreco(preco){
                preco2 = parseFloat( preco.replace('R$ ', '') )
                return preco2
            }
            function gerarTotal(){
                let subtotal = formataPreco($('#subtotal').text())

                // Se o preço subtotal for maior que 200, o frete é grátis
                if(subtotal > 200){

                    $('#frete').html('Gratuito')
                    $('#total').html('R$ '+subtotal.toFixedNoRounding(2))
                    
                // Se for menor que 200
                } else {
                    // Caso o frete esteja como Gratuito, trocar para Calcular
                    if( $('#frete').text() == 'Gratuito' ) $('#frete').text('Calcular')

                    // Caso esteja como Calcular, definir o frete como 0 por default
                    if( $('#frete').text() == 'Calcular' ){
                        frete = 0

                    // Se não, verificar os inputs de seleção de tipo de entrega criados na função que chama a esta
                    } else {
                        if( $('input#inputsedex').is(':checked') ){
                            let sedex = $('input#inputsedex').parent().find('label').first().html()
                            sedex = sedex.replace('Sedex: ', '')
                            frete = formataPreco( sedex )
                            
                        } else if( $('input#inputpac').is(':checked') ){
                            let pac = $('input#inputpac').parent().find('label').first().html()
                            pac = pac.replace('PAC: ', '')
                            frete = formataPreco( pac ) 

                        }
                    }

                    // Calcula o preço total
                    let total = (subtotal + frete).toFixedNoRounding(2)
                    
                    // Imprime na tela o preço total
                    $('#total').text('R$ '+total)
                }
            }

            function get_carrinho_by_usuario(){
                $.ajax({
                    type: "post",
                    url: base_url+"/carrinho/get_carrinho",
                    data: "request=get_carrinho",
                    dataType: "json",
                    success: dados => {
                        $('#lista_carrinho').html('')
                        let preco_total = 0;
                        if(dados.qtd_produtos > 0){
                            $(dados.carrinho).each( function (index, value) {
                                $('#lista_carrinho').append('<div class="media mb-3" id="produto'+index+'">')
                                $('#lista_carrinho div.media#produto'+index).append('<div class="row media-body">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('<div class="view col-md-3 col-sm-4 text-sm-left mx-auto text-center">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.view').append('<img class="img-fluid mr-3 p-2" src="'+base_url+'/uploads/produtos/'+value.id_produto_md5+'/'+value.img+'" alt="'+value.nome+'">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.view').append('<a href="'+base_url+'/produto/'+value.id_produto+'"><div class="mask rgba-white-slight"></div></a>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('</div>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('<div class="nome-produto col-md-3 col-sm-4 text-sm-left text-center align-self-center">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.nome-produto').append('<h6 class="mt-3 mt-sm-0 font-weight-bold">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.nome-produto h6').append('<a class="text-ipci-nocolor" href="'+base_url+'/produto/'+value.id_produto+'">'+value.nome+'</a>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.nome-produto').append('</h6>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('</div>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('<div class="preco-produto col-md-2 col-sm-4 text-sm-left text-center align-self-center">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.preco-produto').append('<h6 class="my-1 my-sm-0 font-weight-bold">R$ '+ (((value.promocao == "1") ? value.preco_promocional : value.preco_venda) * value.quantidade).toFixedNoRounding(2) +'</h6>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('</div>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('<div class="qtd-produto col-md-3 col-sm-4 text-sm-left text-center float-right mx-auto align-self-center">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto').append('<div class="text-center def-number-input number-input safari_only my-0">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input').append('<button onclick="this.parentNode.querySelector(\'input[type=number]\').stepDown()" class="minus px-3"></button>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input').append('<input type="number" class="font-weight-bold" min="1" max="'+value.qtd_produtos+'" name="quantidade'+value.id_produto+'" id="quantidade'+value.id_produto+'" value="'+value.quantidade+'">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input').append('<button onclick="this.parentNode.querySelector(\'input[type=number]\').stepUp()" class="plus px-3"></button>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input').append('<a title="Atualizar quantidade" id="'+value.id_produto+'" class="text-ipci-nocolor float-right align-self-center">')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input a').append('<i class="fas fa-sync-alt"></i>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto div.number-input').append('</a>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body div.qtd-produto').append('</div>')
                                $('#lista_carrinho div.media#produto'+index+' div.media-body').append('</div>')
                                $('#lista_carrinho div.media#produto'+index).append('</div>')
                                $('#lista_carrinho div.media#produto'+index).append('<div class="remover-produto row">')
                                $('#lista_carrinho div.media#produto'+index+' div.remover-produto').append('<div class="col">')
                                $('#lista_carrinho div.media#produto'+index+' div.remover-produto div.col').append('<a title="Remover produto do carrinho" id="'+value.id_produto+'" class="text-ipci-nocolor-secondary float-right">')
                                $('#lista_carrinho div.media#produto'+index+' div.remover-produto div.col a').append('<i class="fas fa-times"></i>')
                                $('#lista_carrinho div.media#produto'+index+' div.remover-produto div.col').append('</a>')
                                $('#lista_carrinho div.media#produto'+index+' div.remover-produto').append('</div>')
                                $('#lista_carrinho div.media#produto'+index).append('</div>')
                                $('#lista_carrinho').append('</div>')
                                $('#lista_carrinho div.media#produto'+index).after( ((dados.qtd_produtos > 0 && dados.qtd_produtos != index+1) ? '<hr class="my-2">': '') )
                                preco_total += ((value.promocao == "1") ? value.preco_promocional : value.preco_venda) * value.quantidade
                            })
            
                            $('div.qtd-produto div.number-input a').click(function (e){
                                e.preventDefault()
                                atualizar_quantidade(e.currentTarget.id, $(e.currentTarget).parent().find('input').val(), $(e.currentTarget).parent().find('input').attr('max'))
                            })

                            $('div.remover-produto div.col a').click(function (e) {
                                e.preventDefault()
                                deletar_carrinho(e.currentTarget.id);
                            })

                        } else {
                            $('#lista_carrinho').append('<div class="row my-5"><div class="col-md-6 mx-auto text-center"><p>Você não possui produtos ainda.</p></div></div>')
                        }
                        $('#subtotal').text('R$ '+preco_total.toFixedNoRounding(2))
                        gerarTotal()

                        $('#lista_carrinho').append('<hr class="ipci-border mb-4">')
                    },
                    error: erros => {
                        console.log(erros)
                    }
                })
            }

            function deletar_carrinho(id_produto){
                $.ajax({
                    type: "post",
                    url: base_url+"/carrinho/deletar",
                    data: "request=del_carrinho&id_produto="+id_produto,
                    dataType: "json",
                    success: dados => {
                        get_carrinho_by_usuario();
                    },
                    error: erros => {
                        console.log(erros)                        
                    }
                });
            }

            function atualizar_quantidade(id_produto, qtd_atualizada, qtd_produtos){
                if(parseInt(qtd_atualizada) > parseInt(qtd_produtos)){
                    alert('A quantidade a ser atualizada excedeu a quantidade do produto no estoque.')
                } else if(parseInt(qtd_atualizada) <= 0){
                    alert('A quantidade a ser atualizada deve ser no mínimo 1.')
                } else {
                    $.ajax({
                        type: "post",
                        url: base_url+"/carrinho/atualizar_quantidade",
                        data: "request=atl_qtd&id_produto="+id_produto+"&qtd_atualizada="+qtd_atualizada,
                        dataType: "json",
                        success: dados => {
                            console.log(dados)
                            get_carrinho_by_usuario();
                            
                        },
                        error: erros => {
                            console.log(erros)                        
                        }
                    });
                }
            }

            get_carrinho_by_usuario();

            function verificaCep(cep){
                let url = 'https://viacep.com.br/ws/'+cep+'/json/unicode/';
                
                let xmlHttp = new XMLHttpRequest();
                xmlHttp.open('GET', url);

                xmlHttp.onreadystatechange = () => {
                    if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        let dadosJSONText = xmlHttp.responseText;
                        let dadosJSONObj = JSON.parse(dadosJSONText);

                        if ( dadosJSONObj.ibge == undefined ) {
                            $('div#xmlresponse').attr('value', 'false')
                            
                        } else {
                            $('div#xmlresponse').attr('value', 'true')
                            
                        }
                    }
                }

                xmlHttp.send()

            }

            function calcularFrete(cep, input_cep, campo_frete){
                
                let msg_erro = 'O CEP está inválido.'

                // Verificações do campo
                if(cep.length == 8){
                    
                    verificaCep(cep)

                    let subtotal = formataPreco($('#subtotal').text())

                    // Se o preço subtotal for maior que 200, o frete é grátis
                    if(subtotal > 200){
                        $('div#erro_cep').remove()
                        input_cep.removeAttr('disabled')
                        input_cep.removeClass('text-danger')
                        input_cep.parent().parent().append('<div id="erro_cep" class="font-small text-primary">O frete é gratuito para compras acima de R$ 200,00.</div>')
                        return false;

                    }

                    setTimeout(function(){
                        let verificado = $('div#xmlresponse').attr('value')
                    
                        if( verificado == 'true' ){
                        
                            $('div#erro_cep').remove()
                            input_cep.removeClass('text-danger')
                            
                            $.ajax({
                                type: "post",
                                url: base_url+'/carrinho/frete',
                                data: 'request=frete&cep='+cep,
                                dataType: "json",
                                success: dados => {
                                    console.log(dados);
                                    
                                    if(dados.sedex['Erro'] == "0" && dados.pac['Erro'] == "0"){
                                        campo_frete.html('')
                                        campo_frete.prepend('<div class="custom-control custom-radio" id="divsedex">')
                                        campo_frete.find('div#divsedex').prepend('<input type="radio" name="freteescolha" id="inputsedex" value="sedex" class="custom-control-input"><label class="custom-control-label" for="inputsedex">Sedex: R$ ' + (parseFloat( (dados.sedex['Valor']).replace(',', '.') )).toFixedNoRounding(2) + '</label></div>')
                                        
                                        campo_frete.prepend('<div class="custom-control custom-radio" id="divpac">')
                                        campo_frete.find('div#divpac').prepend('<input type="radio" name="freteescolha" id="inputpac" value="pac" class="custom-control-input"><label class="custom-control-label" for="inputpac">PAC: R$ ' + (parseFloat( (dados.pac['Valor']).replace(',', '.') )).toFixedNoRounding(2) + '</label></div>')
                                        
                                    } else {   
                                        $('div#erro_cep').remove()
                                        input_cep.removeAttr('disabled')
                                        $('div#wait_cep').remove()
                                        input_cep.addClass('text-danger')
                                        input_cep.parent().parent().append('<div id="erro_cep" class="font-small text-danger">'+msg_erro+'</div>')
                                    }
                                    
                                    input_cep.removeAttr('disabled')
                                    $('div#wait_cep').remove()
                                    
                                    $('input#inputpac, input#inputsedex').click(e=>{
                                        setTimeout(()=>{
                                            gerarTotal()
                                        }, 500)
                                    })
                                },
                                error: erros => {
                                    console.log(erros)
                                }
                            });

                        } else {                            
                            $('div#erro_cep').remove()
                            input_cep.addClass('text-danger')
                            input_cep.parent().parent().append('<div id="erro_cep" class="font-small text-danger">'+msg_erro+'</div>')
                            input_cep.removeAttr('disabled')
                            $('div#wait_cep').remove()
                        }

                    },500)
                    
                } else {
                    $('div#erro_cep').remove()
                    input_cep.removeAttr('disabled')
                    $('div#wait_cep').remove()
                    input_cep.addClass('text-danger')
                    input_cep.parent().parent().append('<div id="erro_cep" class="font-small text-danger">'+msg_erro+'</div>')
                }
            }

            $('input#cep').mask('00000-000');

            function chamarCalcularFrete(e){
                e.preventDefault()
                if( $('div.media#produto0').length ){
                    $('div#erro_cep').remove()
                    $('input#cep').after('<div id="wait_cep" class="font-small text-muted">Consultando CEP...</div>')
                    let campo_frete = $('#frete')
                    let input_cep = $('input#cep')
                    let cep = ($('input#cep').val()).replace('-', '')
                    calcularFrete(cep, input_cep, campo_frete)
                } else {
                    $('#frete').html('Calcular')
                    $('#total').html('R$ 0.00')
                    $('input#cep').parent().parent().find('label').text('Sem produtos')
                    $('div#erro_cep').remove()
                    $('div#wait_cep').remove()
                    $('button#btn_cep').remove()
                    $('input#cep').attr('disabled', 'disabled')
                }
            }

            // Ao clicar no botão 'Ok'
            $('button#btn_cep').click( e => { chamarCalcularFrete(e) } )
            
            // A cada tecla pressionada no input CEP
            $('input#cep').on('keyup', e => {
                e.preventDefault()

                // Adicionar o atributo disabled para realizar a verificação de contagem
                $('input#cep').attr('disabled', 'disabled')

                // Verifica o tamanho dos dados inseridos no input, se for = 9 é válido e ele chama a função para calcular
                if( ($('input#cep').val()).length == 9 ) {
                    chamarCalcularFrete(e)
                
                // Se não, o usuário ainda não terminou de escrever, logo, remove o atributo disabled e foca novamente no input para ele continuar digitando
                } else {
                    $('input#cep').removeAttr('disabled')
                    $('div#wait_cep').remove()
                    $('input#cep').focus()

                }
            })

        });