$(document).ready(function () {

    // Máscara de CPF e CNPJ (necessita do plugin jQuery Mask)
    $('body').on('keydown', '#cpf, #cpfnovo', function (e) {
        try {
            var digit = e.key.replace(/\D/g, '');
        } catch {
            console.log('Sem valor suficiente.');
            
        }

        var value = $(this).val().replace(/\D/g, '');

        var size = value.concat(digit).length;

        $(this).mask((size <= 11) ? '000.000.000-00' : '00.000.000/0000-00');
    });

});