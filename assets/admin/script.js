const base_url = window.location.origin;

$(document).ready(function () {
		
    // Botão/página painel
    $('a#painel').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar departamentos
    $('a#listardepartamentos').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/departamento",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/departamento') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/departamento");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página criar departamento
    $('a#criardepartamento').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/departamento/criar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/departamento/criar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/departamento/criar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar categorias
    $('a#listarcategorias').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/categoria",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/categoria') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/categoria");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página criar categoria
    $('a#criarcategoria').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/categoria/criar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/categoria/criar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/categoria/criar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    
    // Botão/página listar subcategorias
    $('a#listarsubcategorias').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/subcategoria",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/subcategoria') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/subcategoria");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página criar subcategoria
    $('a#criarsubcategoria').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/subcategoria/criar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/subcategoria/criar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/subcategoria/criar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    
    // Botão/página listar produtos
    $('a#listarprodutos').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/produto",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/produto') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/produto");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página criar produto
    $('a#criarproduto').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/produto/criar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/produto/criar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/produto/criar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar avaliações publicadas dos produtos
    $('a#listaravaliacoes').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/produto/avaliacao/listar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/produto/avaliacao/listar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/produto/avaliacao/listar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar avaliações publicadas dos produtos
    $('a#listaravaliacoespendentes').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/produto/avaliacao/listar_pendentes",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/produto/avaliacao/listar_pendentes') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/produto/avaliacao/listar_pendentes");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar clientes
    $('a#listarclientes').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/cliente",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/cliente') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/cliente");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página cadastrar cliente
    $('a#cadastrarcliente').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/cliente/cadastrar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/cliente/cadastrar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/cliente/cadastrar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
                $('#telefone').maskbrphone();

                $("#celular").mask("(99) 99999-9999").focusout(function (event) {  
                    var target, phone, element;  
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);  
                    element.unmask();  
                    if(phone.length) {  
                        element.mask("(99) 99999-9999");  
                    }
                });
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar administradores
    $('a#listaradministradores').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/administrador",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/administrador') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/administrador");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página cadastrar admin
    $('a#cadastraradministrador').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/administrador/cadastrar",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/administrador/cadastrar') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/administrador/cadastrar");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
                $('#telefone').maskbrphone();
                $("#celular").mask("(99) 99999-9999").focusout(function (event) {  
                    var target, phone, element;  
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);  
                    element.unmask();  
                    if(phone.length) {  
                        element.mask("(99) 99999-9999");  
                    }
                });
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })
    // Botão/página listar administradores
    $('a#banner').click((e) => {
        e.preventDefault()
        $.ajax({
            type: "post",
            url: base_url+"/admin/banner",
            data: "request=admin",
            dataType: "json",
            success: data => {
                if($(location).attr('pathname') != '/admin/banner') {
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                    window.history.pushState('string', data.titulo, "/admin/banner");
                } else { 
                    $(document).attr('title', data.titulo)
                    $('#pagina').html(data.html);
                }
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })

    // Quando voltar para página anterior
    $(window).bind('popstate', ()=>{ 
        $.ajax({
            type: "post",
            url: base_url+'/'+$(location).attr('pathname'),
            data: "request=admin",
            dataType: "json",
            success: data => {
                $(document).attr('title', data.titulo)
                $('#pagina').html(data.html);
                $('#telefone').maskbrphone();
                $("#celular").mask("(99) 99999-9999").focusout(function (event) {  
                    var target, phone, element;  
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);  
                    element.unmask();  
                    if(phone.length) {  
                        element.mask("(99) 99999-9999");  
                    }
                });
            },
            error: erro => {
                console.log('Erros foram encontrados:');
                console.log(erro);
                
            }
        });
    })

    
    
})