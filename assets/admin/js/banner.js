$(document).ready(function () {

    function atualizar_lista(){
        $('table#banner').DataTable( {
            "columnDefs": [                                    
                    { "orderable": false, "targets": 0},
                    { "orderable": false, "targets": 2},
                    { "orderable": false, "targets": 3},
                    { "orderable": false, "targets": 4},
                ],
            "language": {
                "lengthMenu": "_MENU_ registros por página",
                "zeroRecords": "Nenhum Registro Encontrado",
                "info": "Total de _MAX_ banner(s), página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro Disponível",
                "infoFiltered": "",
                "search": "Busca:",
                "pagingType": "full_numbers",
                "paginate": {
                    "next": "Próximo",
                    "previous": "Anterior"
                }
            },
            "displayLength": 10
            
        })
        $('table').css('width', 'auto')
    }

    function lista_banners(){
        $('table#banner tbody').html('')
        $.ajax({
            type: "post",
            url: base_url+'/admin/banner/get_lista_banners',
            data: "request=admin",
            dataType: "json",
            success: dados => {
                if(dados.banners[0]){
                    $('tbody tr.odd').remove()
                    $.each(dados.banners, (index, banner) => {
                        $('table#banner tbody').append('<tr id="'+index+'">')
                        $('table#banner tbody tr#'+index).append('<th>'+banner.id+'</th>')
                        $('table#banner tbody tr#'+index).append('<th scope="row"><img class="img-fluid" src="'+base_url+'/uploads/banner/'+banner.img+'" alt="'+banner.nome+'"></th>')
                        $('table#banner tbody tr#'+index).append('<th>'+banner.nome+'</th>')
                        $('table#banner tbody tr#'+index).append('<th><a href="'+banner.link+'" target="_blank" class="text-ipci-nocolor">'+banner.link+'</a></th>')
                        $('table#banner tbody tr#'+index).append('<td>'+ ((banner.status == '1') ? 'Publicado' : 'Não publicado') +'</td>')
                        $('table#banner tbody tr#'+index).append('<td><a class="a-hover p-2" title="Editar banner" id="editarbanner'+banner.id+'" href="'+base_url+'/admin/banner/editar/'+banner.id+'"><i class="fas fa-pen text-warning mx-2"></i></a><a class="a-hover p-2" title="Deletar banner" id="deletarbanner'+banner.id+'" href="'+base_url+'/admin/banner/deletar/'+banner.id+'"><i class="fas fa-times-circle text-danger mx-2"></i></a></td>')
                        $('table#banner tbody').append('</tr>')         

                        $('a#deletarbanner'+banner.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `${base_url}/admin/banner/deletar/${banner.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/banner/deletar/'+banner.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/banner/deletar/"+banner.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                        $('a#editarbanner'+banner.id).click( e=> {
                            e.preventDefault()
                            $.ajax({
                                type: "post",
                                url: `${base_url}/admin/banner/editar/${banner.id}`,
                                data: "request=admin",
                                dataType: "json",
                                success: data => {
                                    if($(location).attr('pathname') != '/admin/banner/editar/'+banner.id) {
                                        $(document).attr('title', data.titulo)
                                        $('#pagina').html(data.html);
                                        window.history.pushState('string', data.titulo, "/admin/banner/editar/"+banner.id);
                                    }                                    
                                },
                                error: erro => {
                                    console.log('Erros foram encontrados:');
                                    console.log(erro);
                                }
                            });
                        })
                    })
                }
                atualizar_lista()
            },
            error: erros => {
                console.log(erros)                
            }
        });
    }

    lista_banners()

    $('form#adicionar_banner').submit((e)=>{
        e.preventDefault()
        var formdata = new FormData();
        formdata.append('nome', $('#nome').val())
        formdata.append('link', $('#link').val())
        formdata.append('file', $('#imagem').prop('files')[0])
        formdata.append('request', $('#request').val())
        formdata.append('status', $('#status').val())

        $.ajax({
            type: 'post',
            url: base_url+'/admin/banner/adicionar',
            data: formdata, 
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                if(data.sucesso){
                    $('#cabecalho').remove()
                    $('#col_tabela').remove()
                    $('div#row_criar').before(data.html)
                    lista_banners()
                    $('div#alert').remove();
                    $.each(data, (index, value) => {
                        if(!value){
                            $(`#${index}`).addClass('is-valid')
                            $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                            $(`#erro_${index}`).remove()
                        } else if(value.length > 3) {
                            $(`#erro_${index}`).remove()
                            $(`#${index}`).addClass('is-invalid')
                            $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                            $(`div.md-form`).find(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                        }
                    })
                    $('form#adicionar_banner button:submit').before($('<div id="alert" class="alert card alert-success fade show mt-4"><strong>'+data.sucesso+'</strong></div>'))
                    setTimeout(()=>{
                        $("div#alert").alert('close',()=>{
                            $('div#alert').remove(); 
                        })
                    },8000)
                }
                if(data.erro){
                    $.each(data, (index, value) => {
                        if(!value){
                            $(`#${index}`).addClass('is-valid')
                            $(`input:checkbox#${index}`).parent().find('label').removeClass('text-danger')
                            $(`#${index}`).removeClass(`#${index} is-invalid`)
                            $(`#erro_${index}`).remove()
                        } else if(value.length > 3) {
                            $(`#erro_${index}`).remove()
                            $(`#${index}`).addClass('is-invalid')
                            $(`input:checkbox#${index}`).parent().find('label').addClass('text-danger')
                            $(`div.md-form`).find(`#${index}`).parent().append($(`<div id="erro_${index}" class="invalid-feedback d-block">${value}</div>`))
                        }
                    })
                    if(data.tipo == 'imagem') $('#preview').parent().addClass('border-danger')
                    else $('#preview').parent().removeClass('border-danger')
                    $('div#alert').remove();
                    $('form#adicionar_banner button:submit').before($('<div id="alert" class="alert card alert-danger fade show mt-4"><strong>'+data.erro+'</strong></div>'))
                    setTimeout(()=>{
                        $("div#alert").alert('close',()=>{
                            $('div#alert').remove(); 
                        })
                    },8000)
                }
            },
            error: erro => {
                console.log('Erros foram encontrados: \n')
                console.log(erro)
            }
        });
    })
    
    $('select').selectize({
        sortField: 'text'
    });

    $("#status").on('change', function() {
        if ($(this).is(':checked')) {
            $(this).attr('value', 1);
        } else {
           $(this).attr('value', 0);
        }
    })

    $('form#adicionar_banner button:reset').click(e=>{
        $('#imagem').parent().find('label').html('')
    })

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('#imagem').change(function(){
        if ($(this)[0].files && $(this)[0].files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result)
            }
            reader.readAsDataURL($(this)[0].files[0]);
        }
    })

    $('button:reset').click(() => {
        $('#preview').attr('src', '../uploads/sembanner.jpg')
    })

    $('#preview').click(() => { $('#imagem').click() })

        
});